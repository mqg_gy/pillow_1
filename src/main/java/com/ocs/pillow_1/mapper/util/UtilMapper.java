package com.ocs.pillow_1.mapper.util;

import java.util.Map;

/**
 * 所有工具的方法接口类
 */
public interface UtilMapper {
    /**
     * 更新token的使用时间
     *
     * @param param
     */
    void updateUserTime(Map param);
}

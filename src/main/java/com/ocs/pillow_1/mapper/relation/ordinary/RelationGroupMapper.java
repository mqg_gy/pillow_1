package com.ocs.pillow_1.mapper.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.RelationGroup;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
public interface RelationGroupMapper {

    /**
     * 创建用户分组
     *
     * @param relationGroup
     */
    void save(RelationGroup relationGroup);

    /**
     * 删除分组
     *
     * @param groupId
     */
    void delete(String groupId);

    /**
     * 根据用户id查找分组
     *
     * @param userId
     * @return
     */
    List<RelationGroup> findByUserId(String userId);

    /**
     * 根据id查找分组信息
     *
     * @param groupId
     * @return
     */
    RelationGroup findById(String groupId);

}

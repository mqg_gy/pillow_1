package com.ocs.pillow_1.mapper.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.RelationRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 关系请求 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface RelationRequestMapper {
    /**
     * 创建请求
     *
     * @param relationRequest
     */
    void save(RelationRequest relationRequest);

    /**
     * 查找请求记录
     *
     * @param param
     * @return
     */
    RelationRequest findByFandU(Map param);

    /**
     * 更新请求
     *
     * @param relationRequest
     */
    void updateRequest(RelationRequest relationRequest);

    /**
     * 查找用户中的请求列表
     *
     * @param userId
     * @return
     */
    List<RelationRequest> findByUserId(@Param("userId") String userId);

    /**
     * 修改回应状态
     *
     * @param param
     */
    void updateAgree(Map param);

    /**
     * 根据Id查找
     *
     * @param requestId
     * @return
     */
    RelationRequest findById(@Param("requestId") String requestId);

    /**
     * 删除请求记录
     *
     * @param requestId
     */
    void delete(@Param("requestId") String requestId);

    /**
     * 删除message中的信息
     *
     * @param param
     */
    void deleteMessage(Map param);

    /**
     * 查找别人发给我的请求中的未读消息
     *
     * @param userId
     * @return
     */
    Long findNotReadCount(@Param("userId") String userId);

    /**
     * 修改已读状态
     *
     * @param requestId
     */
    void updateReadState(@Param("requestId") String requestId);
}

package com.ocs.pillow_1.mapper.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.entity.relation.ordinary.RelationResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-21
 */
public interface RelationResponseMapper {

    /**
     * 保存回应消息
     *
     * @param relationResponse
     */
    void save(RelationResponse relationResponse);

    /**
     * 删除回应消息
     *
     * @param responseId
     */
    void delete(@Param("responseId") String responseId);

    /**
     * 创建回应消息
     *
     * @param
     */
    Relation create(@Param("friendId") String friendId, String token);

    /**
     * 查找指定用户的所有回应消息
     *
     * @param userId
     * @return
     */
    List<RelationResponse> findByUserId(@Param("userId") String userId);

    /**
     * 查找响应中的未读消息
     *
     * @param userId
     * @return
     */
    Long findNotReadCount(@Param("userId") String userId);

    /**
     * 修改已读状态
     *
     * @param responseId
     */
    void updateReadState(@Param("responseId") String responseId);
}

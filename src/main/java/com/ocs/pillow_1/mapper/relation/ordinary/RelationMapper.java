package com.ocs.pillow_1.mapper.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 关系 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface RelationMapper {
    /**
     * 创建关系
     *
     * @param relation
     */
    void save(Relation relation);

    /**
     * 查找具体好友记录
     *
     * @param relationId
     * @return
     */
    Relation findById(String relationId);

    /**
     * 查找用户好友
     * 用户的好友列表调用结果，授权修改等接口已经获得了该条记录
     *
     * @param userId
     * @return
     */
    List<Relation> findByUserId(String userId);

    /**
     * 更新关系记录
     *
     * @param param
     */
    void update(Map param);

    /**
     * 我关心列表
     *
     * @param userId
     * @return
     */
    List<Relation> ICare(String userId);

    /**
     * 关系记录
     *
     * @param param
     * @return
     */
    Relation findByFandU(Map param);

    /**
     * 删除好友
     *
     * @param friendId
     * @param userId
     */
    void delete(@Param("friendId") String friendId, @Param("userId") String userId);

    /**
     * 删除好友按Id
     *
     */
    void deleteById(@Param("relationId") String relationId);

    /**
     * 给好友添加分组
     *
     * @param param
     */
    void addGroup(Map param);

    /**
     * 分组查找指定用户的好友
     *
     * @param userId
     * @return
     */
    List<Relation> findGroup(String userId);

    List<Relation> findAll();

    void addMeToAffection(Relation relation);

    List<Relation> findByFrirndId(String friendId);

    List<Relation> ICare2(String userId);

    /**
     * 好友权限
     *
     * @param friendId
     * @return
     */
    List<Relation> findFriendPermission(String friendId);
}

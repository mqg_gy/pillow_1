package com.ocs.pillow_1.mapper.article.review;


import com.ocs.pillow_1.entity.article.review.ArticleReview;

import java.util.List;

/**
 * <p>
 * 评论的点评 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleReviewMapper {
    /**
     * 新建文章评论
     */
    void save(ArticleReview articlereview);

    /**
     * 根据Id查找该文章评论
     */
    ArticleReview findById(String reviewId);

    /**
     * 累计点赞量
     */
    void updateThumbCount(String reviewId, long thumbCount);

    /**
     * 累计评论量
     */
    void updateReplyCount(String reviewId, long replyCount);

    /**
     * 删除评论
     */
    void delete(String reviewId);

    /**
     * 查询指定评论下的点评
     */
    List<ArticleReview> reviewList(String commentId);
}

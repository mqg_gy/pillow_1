package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleReader;

import java.util.List;

/**
 * <p>
 * 文章阅读记录 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleReaderMapper {
    /**
     * 保存阅读记录
     */
    void save(ArticleReader articleReader);

    /**
     * 通过文章Id和用户Id查询阅读记录
     */
    long findByAandU(String articleId, String userId);

    /**
     * 查询指定文章的阅读记录
     */
    List<ArticleReader> readerList(String articleId);
}

package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleType;

import java.util.List;

/**
 * <p>
 * 文章类型接口 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleTypeMapper {
    /**
     * 根据code查找栏目类型：code
     */
    ArticleType findByCode(Integer code);

    /**
     * 查询所有的栏目类型
     */
    List<ArticleType> findAll();

    /**
     * 修改栏目名称
     */
    void updateName(String articleTypeId, String typeName);
}

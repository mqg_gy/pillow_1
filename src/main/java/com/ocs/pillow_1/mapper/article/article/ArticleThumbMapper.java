package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleThumb;

/**
 * <p>
 * 文章点赞 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleThumbMapper {
    /**
     * 添加点赞记录
     */
    void save(ArticleThumb articleThumb);

    /**
     * 通过文章Id和用户Id查询点赞记录
     */
    long findByAandU(String articleId, String userId);

    /**
     * 取消点赞记录
     */
    void delete(String articleThumbId);

    /**
     * 查找点赞记录
     */
    ArticleThumb findById(String articleThumbId);
}

package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.Article;

import java.util.List;

/**
 * <p>
 * 文章接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleMapper {
    /**
     * 查询文章详情
     */
    Article findById(String articleId);

    /**
     * 根据文章类别Id查询
     */
    List<Article> findByTypeId(String articleTypeId);

    /**
     * 累计阅读量
     */
    void updateReadCount(String articleId, long readCount);

    /**
     * 累计点赞量
     */
    void updateThumbCount(String articleId, long thumbCount);

    /**
     * 累计回复量
     */
    void updateReplyCount(String articleId, long replyCount);

    /**
     * 创建文章
     *
     * @param article
     */
    void save(Article article);
}

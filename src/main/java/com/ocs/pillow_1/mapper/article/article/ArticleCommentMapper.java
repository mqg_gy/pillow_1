package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleComment;

import java.util.List;

/**
 * <p>
 * 文章评论接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleCommentMapper {
    /**
     * 保存评论内容
     */
    void save(ArticleComment articleComment);

    /**
     * 根据ID查找
     */
    ArticleComment findById(String commentId);

    /**
     * 累计点赞量
     */
    void updateThumbCount(String commentId, long thumbCount);

    /**
     * 累计回复量
     */
    void updateReplyCount(String commentId, long replyCount);

    /**
     * 删除评论
     */
    void delete(String commentId);

    /**
     * 查询一篇文章的评论
     */
    List<ArticleComment> commentList(String articleId);
}

package com.ocs.pillow_1.mapper.article.review;


import com.ocs.pillow_1.entity.article.review.ArticleReviewthumb;

/**
 * <p>
 * 点评的点赞 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleReviewthumbMapper {
    /**
     * 添加点赞记录
     */
    void save(ArticleReviewthumb reviewthumb);

    /**
     * 通过文章Id和用户Id查询点赞记录
     */
    long findByRandU(String reviewId, String userId);

    /**
     * 通过Id查找
     */
    ArticleReviewthumb findById(String reviewThumbId);

    /**
     * 通过Id删除
     */
    void delete(String reviewThumbId);

}

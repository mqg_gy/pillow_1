package com.ocs.pillow_1.mapper.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleCommentthumb;

/**
 * <p>
 * 文章评论的点赞 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleCommentthumbMapper {
    /**
     * 添加点赞记录
     */
    void save(ArticleCommentthumb articleCommentthumb);

    /**
     * 通过文章Id和用户Id查询点赞记录
     */
    long findByCandU(String commentId, String userId);

    /**
     * 通过Id查找
     */
    ArticleCommentthumb findById(String commentThumbId);

    /**
     * 通过Id删除
     */
    void delete(String commentThumbId);
}

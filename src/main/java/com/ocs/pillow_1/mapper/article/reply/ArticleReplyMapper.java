package com.ocs.pillow_1.mapper.article.reply;


import com.ocs.pillow_1.entity.article.reply.ArticleReply;

import java.util.List;

/**
 * <p>
 * 回复 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleReplyMapper {
    /**
     * 根据ID查询
     */
    ArticleReply findById(String replyId);

    /**
     * 保存评论点评回复
     */
    void save(ArticleReply articlereply);

    /**
     * 累计点赞量
     */
    void updateThumbCount(String replyId, long thumbCount);

    /**
     * 累计回复量
     */
    void updateReplyCount(String replyId, long replyCount);

    /**
     * 删除评论
     */
    void delete(String replyId);

    /**
     * 查询指定reviewId的评论
     */
    List<ArticleReply> replyList(String reviewId);
}

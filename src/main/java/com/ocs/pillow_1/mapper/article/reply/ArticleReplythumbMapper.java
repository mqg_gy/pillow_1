package com.ocs.pillow_1.mapper.article.reply;


import com.ocs.pillow_1.entity.article.reply.ArticleReplythumb;

/**
 * <p>
 * 回复点赞 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface ArticleReplythumbMapper {
    /**
     * 添加点赞记录
     */
    void save(ArticleReplythumb replythumb);

    /**
     * 通过文章Id和用户Id查询点赞记录
     */
    long findByRandU(String replyId, String userId);

    /**
     * 通过Id查找
     */
    ArticleReplythumb findById(String replyThumbId);

    /**
     * 通过Id删除
     */
    void delete(String replyThumbId);

}

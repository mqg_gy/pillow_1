package com.ocs.pillow_1.mapper.user.system;


import com.ocs.pillow_1.entity.user.system.SysVouchers;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public interface SysVouchersMapper {
    /**
     * 创建优惠卷
     *
     * @param sysVouchers
     */
    void save(SysVouchers sysVouchers);

    /**
     * 修改优惠卷的信息
     *
     * @param param
     */
    void update(Map param);

    /**
     * 更新开始时间
     *
     * @param param
     */
    void updateStartTime(Map param);

    /**
     * 更新结束时间
     *
     * @param param
     */
    void updateEndTime(Map param);

    /**
     * 删除优惠卷
     *
     * @param voucherId
     */
    void delete(String voucherId);

    /**
     * 查找所有优惠卷，并按照时间的顺序
     *
     * @return
     */
    List<SysVouchers> findAll();

    /**
     * 查找指定优惠卷
     *
     * @param voucherId
     * @return
     */
    SysVouchers findById(String voucherId);

}

package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserAutonym;

import java.util.Map;

/**
 * <p>
 * 用户实名认证接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public interface UserAutonymMapper {
    /**
     * 创建用户的实名认证信息
     *
     * @param userAutonym
     */
    void save(UserAutonym userAutonym);

    /**
     * 查找验证信息
     *
     * @param userId
     * @return
     */
    UserAutonym findById(String userId);

    /**
     * 审核不通过，删除该审核信息
     *
     * @param userId
     */
    void delete(String userId);

    /**
     * 更新实名认证的状态
     *
     * @param param
     */
    void updateState(Map param);
}

package com.ocs.pillow_1.mapper.user.system;

import com.ocs.pillow_1.entity.user.system.SysUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-06-04
 */
public interface SysUserMapper {

    /**
     * 根据输入的工号和密码查询
     *
     * @param param
     * @return
     */
    SysUser findByCodeAndPass(Map param);

    List<SysUser> findAll();

    void save(SysUser manager);
}

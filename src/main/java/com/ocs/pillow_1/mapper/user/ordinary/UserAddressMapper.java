package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserAddress;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户购物车地址接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public interface UserAddressMapper {
    /**
     * 创建收货地址
     *
     * @param userAddress
     */
    void save(UserAddress userAddress);

    /**
     * 查找某个用户的所有地址
     *
     * @param userId
     * @return
     */
    List<UserAddress> findByUserId(String userId);

    /**
     * 修改地址的默认状态
     *
     * @param param
     */
    void updateDefault(Map param);

    /**
     * 查找指定地址详情
     *
     * @param addressId
     * @return
     */
    UserAddress findById(String addressId);

    /**
     * 修改地址
     *
     * @param userAddress
     */
    void updateAddress(UserAddress userAddress);

    /**
     * 删除收货地址
     *
     * @param addressId
     */
    void delete(String addressId);
}

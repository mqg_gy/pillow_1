package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDraw;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
public interface UserDrawMapper {
    /**
     * 创建中奖记录
     *
     * @param userDraw
     */
    void save(UserDraw userDraw);

    /**
     * 查找指定用户的所有中奖记录
     *
     * @param userId
     * @return
     */
    List<UserDraw> findByUserId(String userId);

    /**
     * 创建中奖记录详情
     *
     * @param drawId
     * @return
     */
    UserDraw findById(String drawId);

}

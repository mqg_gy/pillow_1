package com.ocs.pillow_1.mapper.user.system;


import com.ocs.pillow_1.entity.user.system.SysRole;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-06-25
 */
public interface SysRoleMapper {

    List<SysRole> findAll();
}

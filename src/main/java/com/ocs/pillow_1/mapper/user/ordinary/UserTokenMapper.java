package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserToken;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 用户的token接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public interface UserTokenMapper {
    /**
     * 根据登录的账号查找用户的id
     *
     * @param username
     * @param password
     * @return
     */
    User findToLogin(@Param("username") String username, @Param("password") String password);

    /**
     * 确认用户是否已经存在token
     *
     * @param userId
     * @param username
     * @return
     */
    UserToken findByIdAndName(String userId, String username);

    /**
     * 刚注册完还没有登陆过的用户,即第一次登录
     *
     * @param userToken
     */
    void save(UserToken userToken);

    /**
     * 根据userId查找用户token是否已经存在
     *
     * @param userId
     * @return
     */
    UserToken findById(String userId);

    /**
     * 已经登陆过了，需要修改token
     *
     * @param userToken
     */
    void reLogin(UserToken userToken);

    /**
     * 根据每一次前端传回的token，获得需要的userId等信息
     * 登录之后，每一次操作都调用这个方法
     *
     * @param token
     * @return
     */
    UserToken findByToken(String token);

    /**
     * 当用户修改了用户名的时候，token中也需要修改
     *
     * @param userId
     * @param username
     */
    void updateUsername(String userId, String username);

}

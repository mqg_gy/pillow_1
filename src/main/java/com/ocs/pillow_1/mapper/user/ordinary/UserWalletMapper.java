package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserWallet;

import java.util.Map;

/**
 * <p>
 * 用户钱包接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public interface UserWalletMapper {
    /**
     * 注册时，创建用户的钱包
     *
     * @param param
     */
    void save(Map param);

    /**
     * 根据Id查找用户钱包
     *
     * @param userId
     * @return
     */
    UserWallet findById(String userId);

    /**
     * 修改钱包的余额
     *
     * @param param
     */
    void updateBalance(Map param);

    /**
     * 修改钱包的积分
     *
     * @param param
     */
    void updateIntegral(Map param);

    /**
     * 设置与修改支付密码
     *
     * @param param
     */
    void updatePayNumber(Map param);

}

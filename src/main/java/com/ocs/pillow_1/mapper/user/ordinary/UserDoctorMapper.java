package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserAutonym;
import com.ocs.pillow_1.entity.user.ordinary.UserDoctor;

import java.util.Map;

/**
 * <p>
 * 医生认证 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public interface UserDoctorMapper {
    /**
     * 创建医生的认证信息
     *
     * @param userDoctor
     */
    void save(UserDoctor userDoctor);

    /**
     * 查找验证信息
     *
     * @param userId
     * @return
     */
    UserDoctor findById(String userId);

    /**
     * 审核不通过，删除该审核信息
     *
     * @param userId
     */
    void delete(String userId);

    /**
     * 更新实名认证的状态
     *
     * @param param
     */
    void updateState(Map param);

}

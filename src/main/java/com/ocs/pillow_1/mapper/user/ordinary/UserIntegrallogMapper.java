package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserIntegrallog;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分记录表 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-28
 */
public interface UserIntegrallogMapper {
    /**
     * 创建积分记录表
     *
     * @param userIntegrallog
     */
    void save(UserIntegrallog userIntegrallog);

    /**
     * 查找某个用户的所有积分记录
     *
     * @param userId
     * @return
     */
    List<UserIntegrallog> findByUserId(String userId);

    /**
     * 查找某个记录的详情
     *
     * @param logId
     * @return
     */
    UserIntegrallog findById(String logId);

    /**
     * 查找奖励积分表
     *
     * @param userId
     * @return
     */
    List<UserIntegrallog> findAwarding(String userId);

}

package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserVouchers;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public interface UserVouchersMapper {
    /**
     * 添加用户的优惠券
     *
     * @param userVouchers
     */
    void save(UserVouchers userVouchers);

    /**
     * 删除优惠卷
     *
     * @param id
     */
    void delete(String id);

    /**
     * 修改优惠卷的状态
     *
     * @param param
     */
    void updateState(Map param);

    /**
     * 查找用户所有的优惠券
     *
     * @param userId
     * @return
     */
    List<UserVouchers> findByUserId(String userId);

}

package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;

import java.util.Map;

/**
 * <p>
 * 用户详情接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public interface UserDetailMapper {
    /**
     * 创建用户详情，参数通过创建用户时，同时传入
     *
     * @param param
     */
    void save(Map param);

    /**
     * 根据用户id查找该用户详情
     *
     * @param userId
     * @return
     */
    UserDetail findById(String userId);

    /**
     * 修改用户的手机系统
     *
     * @param param
     */
    void updateSystemType(Map param);

    /**
     * 用户详情的修改
     *
     * @param param
     */
    void updateDetail(Map param);

    /**
     * 更新用户的实名状态
     *
     * @param param
     */
    void updateAutonym(Map param);

    /**
     * 更新医生的实名状态
     *
     * @param param
     */
    void updateDoctor(Map param);

    /**
     * 查找用户详细资料
     *
     * @param userId
     * @return
     */
    UserDetail findInfo(String userId);

    UserDetail findByOne(String wakeupTime);


    //    User findByNick(String nickname);
    void saveOpenIdByUsername(Map param);
}

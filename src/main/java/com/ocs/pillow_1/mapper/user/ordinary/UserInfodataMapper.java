package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserInfodata;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-06-11
 */
public interface UserInfodataMapper {

    /**
     * 保存
     *
     * @param userInfodata
     */
    void save(UserInfodata userInfodata);

    Map selectUserById(String openId);

}

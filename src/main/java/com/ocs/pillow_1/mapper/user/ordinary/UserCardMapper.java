package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserCard;

import java.util.List;

/**
 * <p>
 * 用户银行卡 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public interface UserCardMapper {
    /**
     * 绑定银行卡
     *
     * @param userCard
     */
    void save(UserCard userCard);

    /**
     * 查找指定用户的银行卡
     *
     * @param userId
     * @return
     */
    List<UserCard> findByUserId(String userId);

    /**
     * 解绑银行卡
     *
     * @param cardId
     */
    void delete(String cardId);
}

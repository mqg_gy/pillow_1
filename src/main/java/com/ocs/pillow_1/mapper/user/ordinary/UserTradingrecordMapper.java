package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserTradingrecord;

/**
 * <p>
 * 用户交易记录接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public interface UserTradingrecordMapper {
    /**
     * 创建交易记录
     *
     * @param userTradingrecord
     */
    void save(UserTradingrecord userTradingrecord);
}

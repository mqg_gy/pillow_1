package com.ocs.pillow_1.mapper.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 普通用户接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-04
 */
public interface UserMapper {
    /**
     * 注册
     *
     * @param user
     */
    void save(User user);

    /**
     * 修改昵称或者用户名
     *
     * @param param
     */
    void updateNickandName(Map param);

    /**
     * 根据Id查找用户
     *
     * @param userId
     * @return
     */
    User findById(String userId);

    /**
     * 根据用户名查找用户
     *
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 修改密码
     *
     * @param user
     */
    void updatePassword(User user);

    List<User> findAll();

    List<User> findByUsernameForWeb(User user);

    /**
     * 用户名密码注册
     *
     * @param user
     */
    void saveOne(User user);

    User findByOpenId(String openId);
}

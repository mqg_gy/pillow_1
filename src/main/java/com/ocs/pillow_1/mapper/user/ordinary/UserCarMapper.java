package com.ocs.pillow_1.mapper.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserCar;
import com.ocs.pillow_1.entity.user.ordinary.UserCard;

import java.util.List;

/**
 * <p>
 * g购物车接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public interface UserCarMapper {
    /**
     * 向购物车中添加商品
     *
     * @param userCar
     */
    void save(UserCar userCar);

    /**
     * 查找用户的购物车
     *
     * @param userId
     * @return
     */
    List<UserCar> findByUserId(String userId);

    /**
     * 校对添加进入购物车的商品
     *
     * @param userCar
     * @return
     */
    UserCar checkSave(UserCar userCar);

    /**
     * 更新商品数量
     *
     * @param userCar
     */
    void updateCount(UserCar userCar);

    /**
     * 删除购物车的物品
     *
     * @param shopId
     */
    void delete(String shopId);

    /**
     * 通过id查找
     *
     * @param shopId
     * @return
     */
    UserCar findById(String shopId);
}

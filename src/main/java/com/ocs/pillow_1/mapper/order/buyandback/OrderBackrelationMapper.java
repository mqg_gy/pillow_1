package com.ocs.pillow_1.mapper.order.buyandback;


import com.ocs.pillow_1.entity.order.buyandback.OrderBackrelation;

import javax.annotation.Resource;

/**
 * <p>
 * 返还用户关系 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public interface OrderBackrelationMapper {
    /**
     * 创建关系
     *
     * @param orderBackrelation
     */
    void save(OrderBackrelation orderBackrelation);


}

package com.ocs.pillow_1.mapper.order.distribute;


import com.ocs.pillow_1.entity.order.distribute.OrderDistribute;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分销订单 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderDistributeMapper {
    /**
     * 创建订单
     *
     * @param orderDistribute
     */
    void save(OrderDistribute orderDistribute);

    /**
     * 查找订单
     *
     * @param distributeOrderId
     * @return
     */
    OrderDistribute findById(String distributeOrderId);

    /**
     * 查找同一个物流中的订单
     *
     * @param orderId
     * @return
     */
    List<OrderDistribute> findByOrderId(String orderId);

    /**
     * 修改订单状态
     *
     * @param param
     */
    void updateState(Map param);
}

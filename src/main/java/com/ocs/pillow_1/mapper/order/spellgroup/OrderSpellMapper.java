package com.ocs.pillow_1.mapper.order.spellgroup;


import com.ocs.pillow_1.entity.order.spellgroup.OrderSpell;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 拼团订单 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderSpellMapper {
    /**
     * 创建订单
     *
     * @param orderSpell
     */
    void save(OrderSpell orderSpell);

    /**
     * 查找订单
     *
     * @param orderId
     * @return
     */
    OrderSpell findById(String orderId);

    /**
     * 查找一个团的订单
     *
     * @param groupId
     * @return
     */
    List<OrderSpell> findByGroupId(String groupId);

    /**
     * 修改订单状态
     *
     * @param param
     */
    void updateState(Map param);
}

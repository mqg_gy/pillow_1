package com.ocs.pillow_1.mapper.order.exchangebyintegral;


import com.ocs.pillow_1.entity.order.exchangebyintegral.OrderIntegralbase;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderIntegralbaseMapper {
    /**
     * 创建基本
     *
     * @param orderIntegralbase
     */
    void save(OrderIntegralbase orderIntegralbase);

    /**
     * 修改额外的差价
     *
     * @param param
     */
    void updateDistance(Map param);

    /**
     * 修改所需的积分
     *
     * @param param
     */
    void updateIntegral(Map param);

    /**
     * 删除
     *
     * @param productId
     */
    void delete(String productId);

    /**
     * 查找
     *
     * @param productId
     * @return
     */
    OrderIntegralbase findById(String productId);

}

package com.ocs.pillow_1.mapper.order.privatecustom;


import com.ocs.pillow_1.entity.order.privatecustom.OrderCustombase;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 私人定制商品列表实体（提前写下的） 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderCustombaseMapper {
    /**
     * 创建项
     *
     * @param orderCustombase
     */
    void save(OrderCustombase orderCustombase);

    /**
     * 查找同一商品
     *
     * @param productId
     * @return
     */
    List<OrderCustombase> findByProductId(String productId);

    /**
     * 查找指定项
     *
     * @param customId
     * @return
     */
    OrderCustombase findById(String customId);

    /**
     * 更新差价
     *
     * @param param
     */
    void updateDistancePrice(Map param);

    /**
     * 修改类目
     *
     * @param param
     */
    void updateNoteItem(Map param);

    /**
     * 根据定制类目、商品id查找
     *
     * @param noteItem
     * @param productId
     * @return
     */
    OrderCustombase getCustomId(String noteItem, String productId);
}

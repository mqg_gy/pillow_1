package com.ocs.pillow_1.mapper.order.buyandback;


import com.ocs.pillow_1.entity.order.buyandback.OrderBack;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 先购后返订单 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public interface OrderBackMapper {
    /**
     * 创建订单
     *
     * @param orderBack
     */
    void save(OrderBack orderBack);

    /**
     * 查找订单
     *
     * @param backOrderId
     * @return
     */
    OrderBack findById(String backOrderId);

    /**
     * 修改订单状态
     *
     * @param param
     */
    void updateState(Map param);

    /**
     * 查找同一个物流订单
     *
     * @param orderId
     * @return
     */
    List<OrderBack> findByOrderId(String orderId);

}

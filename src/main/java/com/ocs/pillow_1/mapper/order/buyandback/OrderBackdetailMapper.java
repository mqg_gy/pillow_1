package com.ocs.pillow_1.mapper.order.buyandback;


import com.ocs.pillow_1.entity.order.buyandback.OrderBackdetail;

/**
 * <p>
 * 返还详情 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public interface OrderBackdetailMapper {
    /**
     * 订单建立之后，创建详情表
     *
     * @param orderBackdetail
     */
    void save(OrderBackdetail orderBackdetail);

}

package com.ocs.pillow_1.mapper.order.privatecustom;


import com.ocs.pillow_1.entity.order.privatecustom.OrderCustom;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 私人定制订单 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderCustomMapper {
    /**
     * 创建订单
     *
     * @param orderCustom
     */
    void save(OrderCustom orderCustom);

    /**
     * 查找同一个物流订单
     *
     * @param orderId
     * @return
     */
    List<OrderCustom> findByOrderId(String orderId);

    /**
     * 查找一个订单
     *
     * @param customOrderId
     * @return
     */
    OrderCustom findById(String customOrderId);

    /**
     * 修改订单状态
     *
     * @param param
     */
    void updateState(Map param);
}

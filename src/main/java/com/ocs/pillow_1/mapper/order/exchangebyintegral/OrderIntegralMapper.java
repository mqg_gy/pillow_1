package com.ocs.pillow_1.mapper.order.exchangebyintegral;


import com.ocs.pillow_1.entity.order.exchangebyintegral.OrderIntegral;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderIntegralMapper {
    /**
     * 创建订单
     *
     * @param orderIntegral
     */
    void save(OrderIntegral orderIntegral);

    /**
     * 查找同一个物流订单
     *
     * @param orderId
     * @return
     */
    List<OrderIntegral> findByOrderId(String orderId);

    /**
     * 查找一个订单
     *
     * @param integralOrderId
     * @return
     */
    OrderIntegral findById(String integralOrderId);

    /**
     * 修改订单状态
     *
     * @param param
     */
    void updateState(Map param);
}

package com.ocs.pillow_1.mapper.order.spellgroup;


import com.ocs.pillow_1.entity.order.spellgroup.OrderSpellbase;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 已经存在的团表 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface OrderSpellbaseMapper {
    /**
     * 开团接口
     *
     * @param orderSpellbase
     */
    void save(OrderSpellbase orderSpellbase);

    /**
     * 查找团
     *
     * @param groupId
     * @return
     */
    OrderSpellbase findById(String groupId);

    /**
     * 查找某个商品的团
     *
     * @param productId
     * @return
     */
    List<OrderSpellbase> findSpelling(String productId);

    /**
     * 拼团人数增加
     *
     * @param param
     */
    void addCount(Map param);

    /**
     * 团成立
     *
     * @param param
     */
    void endGroup(Map param);
}

package com.ocs.pillow_1.mapper.system.version;


import com.ocs.pillow_1.entity.system.version.Version;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-16
 */
public interface VersionMapper {
    void save(Version version);

    List<Version> findAll();

}

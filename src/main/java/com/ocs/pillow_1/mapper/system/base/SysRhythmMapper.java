package com.ocs.pillow_1.mapper.system.base;


import com.ocs.pillow_1.entity.system.base.SysRhythm;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
public interface SysRhythmMapper {
    /**
     * 查找睡眠的起止时间
     *
     * @param age
     * @return
     */
    SysRhythm findByAge(Long age);
}

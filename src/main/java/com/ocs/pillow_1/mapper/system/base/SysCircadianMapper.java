package com.ocs.pillow_1.mapper.system.base;


import com.ocs.pillow_1.entity.system.base.SysCircadian;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
public interface SysCircadianMapper {

    SysCircadian findByTimeDu(Double timeDu);

}

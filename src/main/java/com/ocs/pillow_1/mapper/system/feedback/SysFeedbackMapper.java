package com.ocs.pillow_1.mapper.system.feedback;


import com.ocs.pillow_1.entity.system.feedback.SysFeedback;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
public interface SysFeedbackMapper {

    void save(SysFeedback sysFeedback);

    List<SysFeedback> findAll();

    SysFeedback findById(Integer id);

    void saveResult(SysFeedback feedback);
}

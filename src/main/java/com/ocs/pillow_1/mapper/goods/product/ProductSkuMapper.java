package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSku;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库存 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface ProductSkuMapper {
    /**
     * 创建库存
     *
     * @param productSku
     */
    void save(ProductSku productSku);

    /**
     * 根据Id查找
     *
     * @param skuId
     * @return
     */
    ProductSku findById(String skuId);

    /**
     * 根据商品Id查找
     *
     * @param productId
     * @return
     */
    List<ProductSku> findByProductId(String productId);

    /**
     * 查找所有库存
     *
     * @return
     */
    List<ProductSku> findAll();

    /**
     * 更新库存单位
     *
     * @param param
     */
    void update(Map param);

    /**
     * 删除库存记录
     *
     * @param skuId
     */
    void delete(String skuId);

    /**
     * 获得库存的id
     *
     * @param productId
     * @param speValues
     * @return
     */
    ProductSku getSkuId(String productId, String speValues);

    /**
     * 更新库存数量
     *
     * @param param
     */
    void updateInventory(Map param);

}

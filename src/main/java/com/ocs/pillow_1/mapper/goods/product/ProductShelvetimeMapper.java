package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductShelvetime;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
public interface ProductShelvetimeMapper {
    /**
     * 创建上下架记录
     *
     * @param productShelvetime
     */
    void save(ProductShelvetime productShelvetime);

    /**
     * 设置下架
     *
     * @param param
     */
    void update(Map param);

}

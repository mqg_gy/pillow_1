package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductImages;

import java.util.Map;

/**
 * <p>
 * 商品图片 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface ProductImagesMapper {
    /**
     * 创建商品图实体
     *
     * @param productImages
     */
    void save(ProductImages productImages);

    /**
     * 删除商品图实体
     *
     * @param productId
     */
    void delete(String productId);

    /**
     * 根据id查找商品图
     *
     * @param productId
     * @return
     */
    ProductImages findById(String productId);

    /**
     * 修改商品图
     *
     * @param param
     */
    void update(Map param);
}

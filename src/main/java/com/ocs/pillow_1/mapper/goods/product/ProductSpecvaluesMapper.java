package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSpecvalues;

import java.util.List;

/**
 * <p>
 * 规格值 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface ProductSpecvaluesMapper {
    /**
     * 创建规格值
     *
     * @param productSpecvalues
     */
    void save(ProductSpecvalues productSpecvalues);

    /**
     * 查找
     *
     * @param specValueId
     * @return
     */
    ProductSpecvalues findById(long specValueId);

    /**
     * 更新
     *
     * @param productSpecvalues
     */
    void update(ProductSpecvalues productSpecvalues);

    /**
     * 删除
     *
     * @param specValueId
     */
    void delete(long specValueId);

    /**
     * 根据规格项查找
     *
     * @param specId
     * @return
     */
    List<ProductSpecvalues> findBySpecId(long specId);

    /**
     * 规格项级联删除
     *
     * @param specId
     */
    void cascadeDelete(long specId);

}

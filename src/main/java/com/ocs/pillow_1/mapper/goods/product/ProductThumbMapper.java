package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductThumb;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public interface ProductThumbMapper {
    /**
     * 创建点赞记录
     *
     * @param productThumb
     */
    void save(ProductThumb productThumb);

    /**
     * 根据指定的peoductId和userId查找点赞记录
     *
     * @param param
     * @return
     */
    ProductThumb findByProAndUser(Map param);

    /**
     * 查找商品的所有点赞记录
     *
     * @param productId
     * @return
     */
    List<ProductThumb> findByProductId(String productId);

    /**
     * 删除点赞记录
     *
     * @param productThumbId
     */
    void delete(String productThumbId);

}

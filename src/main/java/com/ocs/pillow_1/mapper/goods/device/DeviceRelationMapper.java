package com.ocs.pillow_1.mapper.goods.device;


import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备用户关系 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface DeviceRelationMapper {
    /**
     * 用户激活设备之后，创建
     *
     * @param deviceRelation
     */
    void save(DeviceRelation deviceRelation);

    /**
     * 查找设备与用户关系
     *
     * @param deviceId
     * @param userId
     * @return
     */
    DeviceRelation findByDevAndUser(@Param("deviceId") String deviceId, @Param("userId") String userId);

    /**
     * 更新设备的绑定状态
     *
     * @param param
     */
    void updateState(Map param);

    /**
     * 查找指定设备且已经被绑定
     *
     * @param deviceId
     * @return
     */
    DeviceRelation findByDevAndSate(@Param("deviceId") String deviceId);

    /**
     * 查找指定用户是否有设备
     *
     * @param userId
     * @return
     */
    DeviceRelation findIsExist(@Param("userId") String userId);

    /**
     * 根据用户Id查找
     *
     * @param userId
     * @return
     */
    List<DeviceRelation> findByUserId(@Param("userId") String userId);

    List<DeviceRelation> findAll();

    DeviceRelation findById(@Param("relationId") String relationId);

}

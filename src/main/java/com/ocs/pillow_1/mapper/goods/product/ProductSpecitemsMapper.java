package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSpecitems;

/**
 * <p>
 * 规格项 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface ProductSpecitemsMapper {
    /**
     * 创建规格项
     *
     * @param productSpecitems
     */
    void save(ProductSpecitems productSpecitems);

    /**
     * 查找
     *
     * @param specId
     * @return
     */
    ProductSpecitems findById(long specId);

    /**
     * 更新
     *
     * @param productSpecitems
     */
    void update(ProductSpecitems productSpecitems);

    /**
     * 删除
     *
     * @param specId
     */
    void delete(long specId);
}

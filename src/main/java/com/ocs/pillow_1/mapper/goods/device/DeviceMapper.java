package com.ocs.pillow_1.mapper.goods.device;


import com.ocs.pillow_1.entity.goods.device.Device;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public interface DeviceMapper {
    /**
     * 创建设备
     *
     * @param device
     */
    void save(Device device);

    /**
     * 修改销售状态
     *
     * @param param
     */
    void updateSaled(Map param);

    /**
     * 修改激活状态
     *
     * @param param
     */
    void updateActivation(Map param);

    /**
     * 设备能返回设备串号，据此查找设备Id
     *
     * @param sn
     * @return
     */
    Device findBySn(@Param("sn") String sn);

    List<Device> findAll();

    void changeQR(Map map);

    Device findById(@Param("deviceId") String deviceId);
}

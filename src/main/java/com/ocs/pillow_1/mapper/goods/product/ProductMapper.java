package com.ocs.pillow_1.mapper.goods.product;


import com.ocs.pillow_1.entity.goods.product.Product;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品 接口
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public interface ProductMapper {
    /**
     * 创建商品
     *
     * @param product
     */
    void save(Product product);

    /**
     * 修改名字、原价、价格、规格项、描述、拼团底价
     *
     * @param param
     */
    void updateBaseItem(Map param);

    /**
     * 修改是否上架、先购后返、私人定制、分销、积分购买、拼团
     *
     * @param param
     */
    void updateIs(Map param);

    /**
     * 根据Id查找
     *
     * @param productId
     * @return
     */
    Product findById(String productId);

    /**
     * 查询同一类型的商品。先购后返、私人定制、分销、积分购买、拼团
     *
     * @param param
     * @return
     */
    List<Product> findByIs(Map param);

    /**
     * 删除产品
     *
     * @param productId
     */
    void delete(String productId);

}

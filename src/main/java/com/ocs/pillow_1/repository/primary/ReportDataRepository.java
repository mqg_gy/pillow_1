package com.ocs.pillow_1.repository.primary;

import com.ocs.pillow_1.entity.ReportData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created by nbfujx on 2017-12-08.
 */
public interface ReportDataRepository extends MongoRepository<ReportData, Long> {

    @Query(value = "{reportName:?0}", fields = "{'id':0}")
    ReportData findByReportName(String s);

    @Query(value = "{did:?0,date:?1}")
    ReportData findByReportDidAndDate(String did, String date);

    @Query(value = "{relationId:?0,date:?1}")
    ReportData findByReportRelationIdAndDate(String relationId, String date);

}

package com.ocs.pillow_1.repository.primary;

import com.ocs.pillow_1.entity.DeviceUserRelation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by nbfujx on 2017-12-08.
 */
public interface DeviceUserRelationRepository extends MongoRepository<DeviceUserRelation, Long> {

    /*@Transactional
    @Query(value = "update DeviceUserRelation d set d.status=?3 where d.deviceId=?1 and d.userId=?2")
    void update(String deviceId,String userId,Integer status);*/

    /*@Query("select d from DeviceUserRelation d where d.deviceId=?1 and d.userId=?2")
    List<DeviceUserRelation> findByDeviceIdAndUserId(String deviceId,String userId);*/

    @Transactional
    @Query(value = "{deviceId:?0,userId:?1}")
    DeviceUserRelation findByDeviceIdAndUserId(String deviceId, String userId);

    @Transactional
    @Query(value = "{userId:?0}")
    List<DeviceUserRelation> findByUserId(String userId);

}

package com.ocs.pillow_1.web;

import com.ocs.pillow_1.entity.user.system.SysRole;
import com.ocs.pillow_1.entity.user.system.SysUser;
import com.ocs.pillow_1.service.user.system.SysManagerService;
import com.ocs.pillow_1.service.user.system.SysRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/managerWeb")
public class ManagerWeb {
    @Resource
    SysManagerService managerService=null;
    @Resource
    SysRoleService sysRoleService=null;

    @RequestMapping("/list")
    public ModelAndView findAll(){
        ModelAndView mav=new ModelAndView("index");
        List<SysUser> sysUsers=managerService.findAll();
        mav.addObject("sysUsers",sysUsers);
        mav.addObject("mes","common/system/manager/listManager.html");
        return mav;
    }

    @RequestMapping("/addManager")
    public ModelAndView addManager(){
        ModelAndView mav=new ModelAndView("index");
        List<SysRole> roles=sysRoleService.findAll();
        mav.addObject("roles",roles);
        mav.addObject("mes","common/system/manager/addManager.html");
        return mav;
    }

    @RequestMapping("/saveManager")
    public ModelAndView saveManager(String userCode,String username,String password,String roleLevel){
        SysUser manager=new SysUser();
        manager.setUserCode(userCode);
        manager.setUsername(username);
        manager.setPassword(password);
        manager.setRoleLevel(Long.valueOf(roleLevel));
        String result=managerService.save(manager);
        if (result.equals("200")){
            return findAll();
        }else {
            return addManager();
        }
    }
}

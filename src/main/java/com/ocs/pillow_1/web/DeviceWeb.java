package com.ocs.pillow_1.web;

import com.ocs.pillow_1.config.FileBean;
import com.ocs.pillow_1.entity.goods.device.Device;
import com.ocs.pillow_1.service.goods.device.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.jws.WebParam;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/deviceWeb")
public class DeviceWeb {
    @Resource
    DeviceService deviceService=null;


    /**
     * 设备列表
     * @return
     */
    @RequestMapping("/list")
    public ModelAndView findAll(){
        ModelAndView mav=new ModelAndView("index");
        List<Device> devices=deviceService.findAll();
        mav.addObject("devices",devices);
        mav.addObject("mes","common/system/device/listDevice.html");
        return mav;
    }

    /**
     * 修改二维码
     * @param sn
     * @return
     */
    @RequestMapping("/changeQR")
    public ModelAndView changQR(String sn){
        if ("".equals(sn)){
            return findAll();
        }
        ModelAndView mav=new ModelAndView("index");
        Device device=deviceService.findBySn(sn);
        if (device!=null) {
            deviceService.changeQR(device.getDeviceId(), sn);
        }
        mav.addObject("devices",deviceService.findBySn(sn));
        mav.addObject("mes","common/system/device/listDevice.html");
        return mav;
    }

    /**
     * 添加设备
     * @return
     */
    @RequestMapping("/add")
    public ModelAndView addDevice(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/device/addDevice.html");
        return mav;
    }

    /**
     * 保存设备
     * @param sn
     * @param deliveryTime
     * @return
     */
    @RequestMapping("/save")
    public ModelAndView saveDevice(String sn,String deliveryTime,String sim,String type,String source,String model,String insurance){
        String result=deviceService.create(sn,deliveryTime,sim,type,source,model,insurance);
        if (!result.equals("200")){
            ModelAndView mav=new ModelAndView("index");
            mav.addObject("result","文件上传失败！");
            mav.addObject("mes","common/system/device/addDevice.html");
            return mav;
        }else {
            return findAll();
        }
    }
}

package com.ocs.pillow_1.web;

import com.ocs.pillow_1.entity.system.version.Version;
import com.ocs.pillow_1.service.system.version.VersionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/versionWeb")
public class VersionWeb {
    @Resource
    VersionService versionService=null;

    @RequestMapping("/add")
    public ModelAndView test(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/version/addVersion.html");
        return mav;
    }

    @RequestMapping("/save")
    public ModelAndView create(String versionId, MultipartFile upload){
        String result=versionService.create(versionId,upload);
        if (!result.equals("200")){
            ModelAndView mav=new ModelAndView("index");
            mav.addObject("result","文件上传失败！");
            mav.addObject("mes","common/system/version/addVersion.html");
            return mav;
        }else {
            return findAll();
        }
    }

    /**
     * 设备版本列表
     * @return
     */
    @RequestMapping("/list")
    private ModelAndView findAll() {
        ModelAndView mav=new ModelAndView("index");
        List<Version> versions=versionService.findAll();
        mav.addObject("versions",versions);
        mav.addObject("mes","common/system/version/listVersion.html");
        return mav;
    }
}

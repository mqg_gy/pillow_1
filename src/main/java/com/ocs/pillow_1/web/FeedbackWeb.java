package com.ocs.pillow_1.web;

import com.ocs.pillow_1.entity.system.feedback.SysFeedback;
import com.ocs.pillow_1.service.system.feedback.SysFeedbackService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/feedbackWeb")
public class FeedbackWeb {
    @Resource
    SysFeedbackService feedbackService=null;

    @RequestMapping("/list")
    public ModelAndView findAll(){
        ModelAndView mav=new ModelAndView("index");
        List<SysFeedback> feedbacks=feedbackService.findAll();
        mav.addObject("feedbacks",feedbacks);
        mav.addObject("mes","common/system/feedback/listFeedback.html");
        return mav;
    }

    @RequestMapping("/feedDeal")
    public ModelAndView feedDeal(String id,String result){
        SysFeedback feedback=feedbackService.findById(Integer.parseInt(id));
        feedback.setResult(result);
        if (feedback!=null){
            feedbackService.saveResult(feedback);
        }
        return findAll();
    }

    @RequestMapping("/noOperate")
    public ModelAndView findNoOperate(){
        ModelAndView mav=new ModelAndView("index");
        List<SysFeedback> list=feedbackService.findAll();
        List<SysFeedback> feedbacks=new ArrayList<>();
        for (SysFeedback f:list) {
            if (f.getState()==0){
                feedbacks.add(f);
            }
        }
        mav.addObject("feedbacks",feedbacks);
        mav.addObject("mes","common/system/feedback/listFeedback.html");
        return mav;
    }

    @RequestMapping("/alOperate")
    public ModelAndView findAlOperate(){
        ModelAndView mav=new ModelAndView("index");
        List<SysFeedback> list=feedbackService.findAll();
        List<SysFeedback> feedbacks=new ArrayList<>();
        for (SysFeedback f:list) {
            if (f.getState()==1){
                feedbacks.add(f);
            }
        }
        mav.addObject("feedbacks",feedbacks);
        mav.addObject("mes","common/system/feedback/listFeedback.html");
        return mav;
    }
}

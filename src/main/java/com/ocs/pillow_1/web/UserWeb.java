package com.ocs.pillow_1.web;

import com.ocs.pillow_1.entity.ReportData;
import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.entity.user.UserWapper;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.repository.primary.ReportDataRepository;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.relation.ordinary.RelationGroupService;
import com.ocs.pillow_1.service.relation.ordinary.RelationService;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import com.ocs.pillow_1.service.util.UpDataService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/userWeb")
public class UserWeb {
    @Resource
    UserService userService=null;
    @Resource
    UserDetailService userDetailService=null;
    @Resource
    RelationService relationService=null;
    @Resource
    RelationGroupService relationGroupService=null;
    @Resource
    DeviceRelationService deviceRelationService=null;

    /**
     * 用户列表
     * @return
     */
    @RequestMapping("/list")
    public ModelAndView findAll(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes", "common/system/user/listUser.html");
        List<User> users=userService.findAll();
        if (users!=null){
            List<UserWapper> userWappers=new ArrayList<>();
            for (User u:users) {
                UserWapper userWapper=new UserWapper();
                userWapper.setUserId(u.getUserId());
                userWapper.setUser(u);
                UserDetail userDetail= userDetailService.findById(u.getUserId());
                if(userDetail!=null){
                    userWapper.setUserDetail(userDetail);
                }else{
                    userWapper.setUserDetail(new UserDetail());
                }
                userWappers.add(userWapper);
            }
            mav.addObject("userWappers",userWappers);
        }
        return mav;
    }

    /**
     * 查找指定用户
     * @param username
     * @return
     */
    @RequestMapping("/user")
    public ModelAndView findUser(String username){
        if ("".equals(username)){
            return findAll();
        }
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUser.html");
        User user=new User();
        user.setUsername(username);
        List<User> users=userService.findByUsernameForWeb(user);

        if (users!=null){
            List<UserWapper> userWappers=new ArrayList<>();
            for (User u:users) {
                UserWapper userWapper=new UserWapper();
                userWapper.setUserId(u.getUserId());
                userWapper.setUser(u);
                userWapper.setUserDetail(userDetailService.findById(u.getUserId()));
                userWappers.add(userWapper);
            }
            mav.addObject("userWappers",userWappers);
        }
        return mav;
    }

    /**
     * 用户好友关系表
     * @return
     */
    @RequestMapping("/relation")
    public ModelAndView userRelation(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserRelation.html");
        List<Relation> relations=relationService.findAll();
        if (relations!=null) {
            for (Relation r : relations) {
                r.setUser(userService.findById(r.getUserId()));
                r.setFriend(userDetailService.findById(r.getFriendId()));
                if (r.getGroupId().equals("0")) {
                    r.setGroupName("我的好友");
                } else {
                    r.setGroupName(relationGroupService.findById(r.getGroupId()).getGroupName());
                }
            }
        }
        mav.addObject("relations",relations);
        return mav;
    }

    /**
     * 查找指定用户的好友列表
     * @param username
     * @return
     */
    @RequestMapping("/userRelation")
    public ModelAndView findUserRelation(String username){
        if ("".equals(username)){
            return userRelation();
        }
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserRelation.html");
        List<Relation> relations=relationService.findByUserId(userService.findByUsername(username).getUserId());
        if (relations!=null) {
            for (Relation r : relations) {
                r.setUser(userService.findById(r.getUserId()));
                r.setFriend(userDetailService.findById(r.getFriendId()));
                if (r.getGroupId().equals("0")) {
                    r.setGroupName("我的好友");
                } else {
                    r.setGroupName(relationGroupService.findById(r.getGroupId()).getGroupName());
                }
            }
        }
        mav.addObject("relations",relations);
        return mav;
    }

    /**
     * 用户设备列表
     * @return
     */
    @RequestMapping("/device")
    public ModelAndView userDevice(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserDevice.html");
        List<DeviceRelation> deviceRelations=deviceRelationService.findAll();
        if (deviceRelations!=null) {
            for (DeviceRelation d : deviceRelations) {
                d.setUser(userService.findById(d.getUserId()));
            }
        }
        mav.addObject("deviceRelations",deviceRelations);
        return mav;
    }

    /**
     * 查找指定用户的设备关系表
     * @param username
     * @return
     */
    @RequestMapping("/userDevice")
    public ModelAndView findUserDevice(String username){
        if ("".equals(username)){
            return userDevice();
        }
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserDevice.html");
        User user=userService.findByUsername(username);
        List<DeviceRelation> deviceRelations=new ArrayList<>();
        if (user!=null){
            deviceRelations=deviceRelationService.findByUserId(user.getUserId());
        }else {
            deviceRelations=null;
        }
        if (deviceRelations!=null) {
            for (DeviceRelation d : deviceRelations) {
                d.setUser(userService.findById(d.getUserId()));
            }
        }
        mav.addObject("deviceRelations",deviceRelations);
        return mav;
    }

    /**
     * 已绑定的用户设备
     */
    @RequestMapping("/isBinding")
    public ModelAndView isBinding(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserDevice.html");
        List<DeviceRelation> deviceRelations1=deviceRelationService.findAll();
        List<DeviceRelation> deviceRelations=new ArrayList<>();
        if (deviceRelations1!=null){
            for (DeviceRelation d:deviceRelations1) {
                if (d.getState()==0){
                    d.setUser(userService.findById(d.getUserId()));
                    deviceRelations.add(d);
                }
            }
        }
        mav.addObject("deviceRelations",deviceRelations);
        return mav;
    }

    /**
     * 未被绑定的用户设备
     */
    @RequestMapping("/noBinding")
    public ModelAndView noBinding(){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserDevice.html");
        List<DeviceRelation> deviceRelations1=deviceRelationService.findAll();
        List<DeviceRelation> deviceRelations=new ArrayList<>();
        if (deviceRelations1!=null){
            for (DeviceRelation d:deviceRelations1) {
                if (d.getState()==1){
                    d.setUser(userService.findById(d.getUserId()));
                    deviceRelations.add(d);
                }
            }
        }
        mav.addObject("deviceRelations",deviceRelations);
        return mav;
    }

    /**
     * 修改指定设备的绑定状态
     */
    @Resource
    UpDataService upDataService=null;

    /**
     * 修改设备的绑定状态
     * @param relationId
     * @return
     */
    @RequestMapping("/deviceState")
    public ModelAndView changeDeviceState(String relationId){
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/listUserDevice.html");
        DeviceRelation deviceRelation=deviceRelationService.findById(relationId);  //根据设备id查找设备信息
        if (deviceRelation.getState()==0){
            deviceRelationService.updateState(deviceRelation.getDeviceId(),deviceRelation.getUserId(),1);
            upDataService.update(deviceRelation.getDeviceId(),deviceRelation.getUserId(),0);
        }else if (deviceRelationService.findByDevAndSate(deviceRelation.getDeviceId())==null){//先判断是否有其他人已经绑定了该设备
            deviceRelationService.updateState(deviceRelation.getDeviceId(),deviceRelation.getUserId(),0);
            upDataService.update(deviceRelation.getDeviceId(),deviceRelation.getUserId(),1);
        }
        DeviceRelation deviceRelations=deviceRelationService.findById(relationId); //查找用户信息
        if (deviceRelations!=null) {
            deviceRelations.setUser(userService.findById(deviceRelations.getUserId()));
        }
        mav.addObject("deviceRelations",deviceRelations);
        return mav;
    }

    /**
     * 查询设备报告
     * @param relationId
     * @param date
     * @return
     */
    @Resource
    ReportDataRepository reportDataRepository=null;
    @RequestMapping("/deviceReport")
    public ModelAndView deviceReport(String relationId,String date) throws ParseException {
        ModelAndView mav=new ModelAndView("index");
        mav.addObject("mes","common/system/user/deviceReports.html");
        mav.addObject("relationId",relationId);
        if (date==null||"".equals(date))
        {
            Calendar calendar=Calendar.getInstance();
            calendar.add(Calendar.DATE,-1);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFormat.format(calendar.getTime());
        }
        ReportData reportData=reportDataRepository.findByReportRelationIdAndDate(relationId,date);
        mav.addObject("reportData",reportData);
        mav.addObject("date",date);
        return mav;
    }

    /**
     * 注册页面
     * @return
     */
    @RequestMapping("/register")
    public ModelAndView register() {
        return new ModelAndView("register");
    }

    /**
     * 用户协议
     * @return
     */
    @RequestMapping("/userAgreement")
    public ModelAndView userAgreement() {
        return new ModelAndView("userAgreement");
    }
}

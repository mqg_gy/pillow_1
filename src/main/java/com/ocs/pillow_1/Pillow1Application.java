package com.ocs.pillow_1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.ocs.pillow_1.mapper")
public class Pillow1Application {

    public static void main(String[] args) {
        SpringApplication.run(Pillow1Application.class, args);
    }

}

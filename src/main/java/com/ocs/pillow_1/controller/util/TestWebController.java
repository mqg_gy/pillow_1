package com.ocs.pillow_1.controller.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ocs.pillow_1.entity.TestUser;
import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.mapper.goods.device.DeviceRelationMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.jws.soap.SOAPBinding;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test")
public class TestWebController {

    @RequestMapping("/qr")
    @ResponseBody
    public String qr(String sn) {
        Integer width = 300;
        Integer height = 300;
        String format = "png";

        HashMap hints = new HashMap();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);

        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(sn, BarcodeFormat.QR_CODE, width, height, hints);
            Path file = new File("C:/upload1/img.png").toPath();
            MatrixToImageWriter.writeToPath(bitMatrix, format, file);
            return "200";
        } catch (WriterException e) {
            e.printStackTrace();
            return "201";
        } catch (IOException e) {
            e.printStackTrace();
            return "201";
        }
    }

    /**
     * 后端往前端传数据
     *
     * @return
     */
    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("testLabel", "显示空间");
        return mav;
    }

    /**
     * 后端从前端拿数据
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/create")
    public ModelAndView create(String username, String password) {
        ModelAndView mav = new ModelAndView("index");
        System.out.println("username______" + username);
        System.out.println("password______" + password);
        return mav;
    }

    /**
     * 点击按钮之后，跳转另一个页面，并发送数据，以及遍历数组数据
     *
     * @return
     */
    @RequestMapping("/findAll")
    public ModelAndView findAll() {
        List<TestUser> testUsers = new ArrayList<>();
        testUsers.add(new TestUser("王婕", "123"));
        testUsers.add(new TestUser("wang", "444"));
        testUsers.add(new TestUser("jie", "333"));
        testUsers.add(new TestUser("比目", "222"));
        testUsers.add(new TestUser("佛手", "111"));
        ModelAndView mav = new ModelAndView("first");
        mav.addObject("username", "王婕");
        mav.addObject("password", "111111");
        mav.addObject("testUsers", testUsers);
        return mav;
    }

    @ResponseBody
    @RequestMapping("/toString")
    public String toString() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("key1", "value1");
        map.put("key2", "value2");

        String data = new String();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            data += entry.getKey() + ":" + entry.getValue() + ";";
        }

        return data.substring(0, data.length() - 1);
    }
}

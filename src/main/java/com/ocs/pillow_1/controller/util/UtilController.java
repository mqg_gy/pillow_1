package com.ocs.pillow_1.controller.util;

import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/util")
public class UtilController {
    @Resource
    UtilService utilService = null;

    /**
     * 根据token获得userId，并更新token的使用时间
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/loginReturn")
    public String loginReturn(String token) {
        return utilService.loginReturn(token);
    }

    /**
     * 加密
     *
     * @param userId
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping("/addmd")
    public String string2MD5(String userId, String password) {
        return utilService.string2MD5(userId, password);
    }

}

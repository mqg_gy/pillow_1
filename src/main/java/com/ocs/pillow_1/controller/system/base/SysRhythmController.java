package com.ocs.pillow_1.controller.system.base;


import com.ocs.pillow_1.entity.system.base.SysRhythm;
import com.ocs.pillow_1.service.system.base.SysCircadianService;
import com.ocs.pillow_1.service.system.base.SysRhythmService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
@Controller
@RequestMapping("/sysRhythm")
public class SysRhythmController {
    @Resource
    SysRhythmService sysRhythmService = null;
    @Resource
    SysCircadianService sysCircadianService = null;

    /**
     * 查找睡眠的起止时间，以及时间的节律钟内容
     *
     * @param age
     * @return
     */
    @ResponseBody
    @RequestMapping("/findTime")
    public Object findTime(Long age, String sex) {
        Map param = new HashMap();
        String code;
        String message;
        SysRhythm data = sysRhythmService.findTime(age);
        if (data != null) {
            param.put("startTime", data.getSleepTime());
            code = "200";
            message = "成功";
            if (sex.equals("男")) {
                param.put("endTime", data.getManEndTime());
            } else if (sex.equals("女")) {
                param.put("endTime", data.getWomenEndTime());
            } else {
                code = "204";
                message = "性别错误！";
                param.put("code", code);
                param.put("message", message);
                return param;
            }
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        param.put("content", sysCircadianService.findByTimeDu());
        return param;
    }
}


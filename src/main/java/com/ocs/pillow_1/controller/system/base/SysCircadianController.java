package com.ocs.pillow_1.controller.system.base;


import com.ocs.pillow_1.entity.system.base.SysCircadian;
import com.ocs.pillow_1.service.system.base.SysCircadianService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.sql.Time;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
@Controller
@RequestMapping("/sysCircadian")
public class SysCircadianController {
    @Resource
    SysCircadianService sysCircadianService = null;

    /**
     * 根据此时的时间查找对应信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByTimeDu")
    public Object findByTimeDu() {
        String code;
        String message;
        Map param = new HashMap();
        String data = sysCircadianService.findByTimeDu();
        if (data == "201") {
            code = "201";
            message = "失败";
        } else {
            code = "200";
            message = "成功";
            param.put("daya", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


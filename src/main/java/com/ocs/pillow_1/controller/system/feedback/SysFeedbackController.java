package com.ocs.pillow_1.controller.system.feedback;


import com.ocs.pillow_1.service.system.feedback.SysFeedbackService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
@Controller
@RequestMapping("/sysFeedback")
public class SysFeedbackController {
    @Resource
    SysFeedbackService sysFeedbackService = null;

    /**
     * 用户创建反馈
     *
     * @param content
     * @param contact
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object create(String content, String contact) {
        String code;
        String message;
        Map param = new HashMap();
        String data = sysFeedbackService.create(content, contact);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


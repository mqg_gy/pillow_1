package com.ocs.pillow_1.controller.system.version;


import com.ocs.pillow_1.entity.system.version.Version;
import com.ocs.pillow_1.service.system.version.VersionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-16
 */
@Controller
@RequestMapping("/version")
public class VersionController {
    @Resource
    VersionService versionService = null;

    @RequestMapping("/findNew")
    @ResponseBody
    public Object findNew() {
        Map param = new HashMap();
        String code;
        String message;
        Version data = versionService.findNew();
        if (data != null) {
            code = "200";
            message = "成功";
            File file = new File(data.getApk());
            param.put("file", file);
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


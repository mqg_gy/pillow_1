package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserAddress;
import com.ocs.pillow_1.service.user.ordinary.UserAddressService;
import com.ocs.pillow_1.service.util.UtilService;
import jdk.nashorn.internal.ir.IfNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * UserAddressController前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Controller
@RequestMapping("/userAddress")
public class UserAddressController {
    @Resource
    UserAddressService userAddressService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建用户的收货地址
     *
     * @param province
     * @param city
     * @param area
     * @param street
     * @param address
     * @param consignee
     * @param consigneePhone
     * @param isDefault
     * @param type
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String province, String city, String area, String street, String address, String consignee, String consigneePhone, Integer isDefault, String type, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "201";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        if (userAddressService.create(province, city, area, street, address, consignee, consigneePhone, isDefault, type, userId) == "201") {
            code = "201";
            message = "创建失败";
        } else {
            code = "200";
            message = "创建成功";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找一个用户的所有地址
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "201";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<UserAddress> data = userAddressService.findByUserId(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 修改默认地址，默认地址唯一
     *
     * @param addressId
     * @param isDefault
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateDefault")
    public Object updateDefault(String addressId, int isDefault, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "201";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userAddressService.updateDefault(addressId, isDefault, userId);
        if (data == "201") {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "修改成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 修改收货地址
     *
     * @param addressId
     * @param province
     * @param city
     * @param area
     * @param street
     * @param address
     * @param consignee
     * @param consigneePhone
     * @param isDefault
     * @param type
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateAddress")
    public Object updateAddress(String addressId, String province, String city, String area, String street, String address, String consignee, String consigneePhone, Integer isDefault, String type, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "201";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userAddressService.updateAddress(addressId, province, city, area, street, address, consignee, consigneePhone, isDefault, type, userId);
        if (data == "201") {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "修改成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除用户地址
     *
     * @param token
     * @param addressId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String token, String addressId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "201";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userAddressService.delete(addressId);
        if (data == "201") {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "删除成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


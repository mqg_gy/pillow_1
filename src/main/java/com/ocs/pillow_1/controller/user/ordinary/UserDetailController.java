package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户详情前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Controller
@RequestMapping("/userDetail")
public class UserDetailController {
    @Resource
    UserDetailService userDetailService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 用户详细资料修改接口
     *
     * @param token
     * @param birthday
     * @param file
     * @param sex
     * @param phone
     * @param industry
     * @param unit
     * @param position
     * @param address
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateDetil")
    public Object updateDetail(@RequestParam("token") String token, String birthday, MultipartFile file, String sex, String phone, String industry, String unit, String position, String address, String signature, Double weight, String wakeupTime, String idealWakeupTime) {
        String userId;
        Map param = new HashMap();
        String code;
        String message;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userDetailService.updateDetail(userId, birthday, file, sex, phone, industry, unit, position, address, signature, weight, wakeupTime, idealWakeupTime);
        if (data == "201") {
            code = "201";
            message = "修改失败";
        } else {
            code = "200";
            message = "修改成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找用户详情，用于显示（头像方法还未调用）
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String token) {
        String userId;
        String message;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在！";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        if (userDetailService.findById(userId) == null) {
            code = "201";
            message = "该用户不存在";
        } else {
            code = "200";
            message = "查找成功";
            UserDetail data = userDetailService.findById(userId);
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 根据用户名保存openId
     *
     * @param username
     * @param openId
     * @return
     */
    @ResponseBody
    @RequestMapping("saveOpenIdByUsername")
    public Object saveOpenIdByUsername(@RequestParam("username") String username, @RequestParam("openId") String openId) {
        Map param = new HashMap();
        String code;
        String message;
        String data = userDetailService.saveOpenIdByUsername(username, openId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


}


package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserIntegrallog;
import com.ocs.pillow_1.service.user.ordinary.UserIntegrallogService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-28
 */
@Controller
@RequestMapping("/userIntegrallog")
public class UserIntegrallogController {
    @Resource
    UserIntegrallogService userIntegrallogService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建积分记录
     *
     * @param token
     * @param amount
     * @param orderId
     * @param message
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, Double amount, String orderId, String message) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = userIntegrallogService.create(userId, amount, orderId, message);
        if (data == "200") {
            code = "200";
            mes = "创建成功";
        } else {
            code = "201";
            mes = "创建失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找指定用户的所有积分记录
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<UserIntegrallog> data = userIntegrallogService.findByUserId(userId);
        if (data != null) {
            code = "200";
            mes = "创建成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "创建失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找指定记录的详情
     *
     * @param logId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String logId) {
        String mes;
        String code;
        Map param = new HashMap();
        UserIntegrallog data = userIntegrallogService.findById(logId);
        if (data != null) {
            code = "200";
            mes = "创建成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "创建失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找奖励积分表
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findAwarding")
    public Object findAwarding(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<UserIntegrallog> data = userIntegrallogService.findAwarding(userId);
        if (data != null) {
            code = "200";
            mes = "创建成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "创建失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

}


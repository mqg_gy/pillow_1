package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserAutonym;
import com.ocs.pillow_1.service.user.ordinary.UserAutonymService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户实名认证前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Controller
@RequestMapping("/userAutonym")
public class UserAutonymController {
    @Resource
    UserAutonymService userAutonymService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建实名认证信息，认证状态默认是0：审核中
     *
     * @param token
     * @param positivePhoto
     * @param backPhoto
     * @param name
     * @param national
     * @param birthday
     * @param address
     * @param idNumber
     * @param deadline
     * @param organ
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String positivePhoto, String backPhoto, String name, String national, Date birthday, String address, String idNumber, Date deadline, String organ) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userAutonymService.create(userId, positivePhoto, backPhoto, name, national, birthday, address, idNumber, deadline, organ);
        if (data == "201") {
            code = "201";
            message = "创建失败";
        } else {
            code = "200";
            message = "创建成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找实名认证信息
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public UserAutonym findById(String token) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return null;
        }
        return userAutonymService.findById(userId);
    }

    /**
     * 修改用户的审核状态
     *
     * @param token
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String token, int state) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return userAutonymService.updateState(userId, state);
    }

}


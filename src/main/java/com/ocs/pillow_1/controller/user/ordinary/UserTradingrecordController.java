package com.ocs.pillow_1.controller.user.ordinary;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 用户交易记录前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Controller
@RequestMapping("/userTradingrecord")
public class UserTradingrecordController {

}


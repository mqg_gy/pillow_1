package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.service.user.ordinary.UserInfodataService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-06-11
 */
@Controller
@RequestMapping("/userInfodata")
public class UserInfodataController {
    @Resource
    UserInfodataService userInfodataService = null;

    /**
     * 保存用户的微信信息
     *
     * @param openId
     * @param nickname
     * @param userImg
     * @param sex
     * @param unionId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String openId, String nickname, String userImg, String sex, String unionId) {
        String code;
        String message;
        Map param = new HashMap();
        String data = userInfodataService.create(openId, nickname, userImg, sex, unionId);
        if (data.equals("200")) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


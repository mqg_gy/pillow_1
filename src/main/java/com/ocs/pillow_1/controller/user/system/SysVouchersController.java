package com.ocs.pillow_1.controller.user.system;


import com.ocs.pillow_1.service.user.system.SysVouchersService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Controller
@RequestMapping("/sysVouchers")
public class SysVouchersController {
    @Resource
    SysVouchersService sysVouchersService = null;

    /**
     * 添加系统的优惠卷
     *
     * @param faceValue
     * @param range
     * @param products
     * @param startTime
     * @param endTime
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String faceValue, String range, String products, Date startTime, Date endTime) {
        return sysVouchersService.create(faceValue, range, products, startTime, endTime);
    }

    /**
     * 修改优惠卷的面值
     *
     * @param voucherId
     * @param faceValue
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateFaceValue")
    public String updateFaceValue(String voucherId, String faceValue) {
        return sysVouchersService.update(voucherId, faceValue, null, null);
    }

    /**
     * 修改优惠卷的适用范围
     *
     * @param voucherId
     * @param range
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateRange")
    public String updateRange(String voucherId, String range) {
        return sysVouchersService.update(voucherId, null, range, null);
    }

    /**
     * 修改优惠卷的适用商品
     *
     * @param voucherId
     * @param products
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateProducts")
    public String updateProducts(String voucherId, String products) {
        return sysVouchersService.update(voucherId, null, null, products);
    }

    /**
     * 更新开始时间
     *
     * @param voucherId
     * @param startTime
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateStartTime")
    public String updateStartTime(String voucherId, Date startTime) {
        return sysVouchersService.updateStartTime(voucherId, startTime);
    }

    /**
     * 更新结束时间
     *
     * @param voucherId
     * @param endTime
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateEndTime")
    public String updateEndTime(String voucherId, Date endTime) {
        return sysVouchersService.updateEndTime(voucherId, endTime);
    }

    /**
     * 删除优惠卷
     *
     * @param voucherId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String voucherId) {
        return sysVouchersService.delete(voucherId);
    }

}


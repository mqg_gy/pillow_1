package com.ocs.pillow_1.controller.user.system;


import com.ocs.pillow_1.entity.user.system.SysUser;
import com.ocs.pillow_1.service.user.system.SysManagerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-06-04
 */
@Controller
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    SysManagerService sysManagerService = null;

    /**
     * 管理员登录接口
     *
     * @param usercode
     * @param password
     * @return
     */
    @RequestMapping("/login")
    public ModelAndView checkLogin(String usercode, String password) {
        ModelAndView mav = new ModelAndView();
        SysUser sysUser = sysManagerService.checkLogin(usercode, password);
        if (sysUser != null) {
            mav.addObject("username", sysUser.getUsername());
            mav.setViewName("index");
        } else {
            mav.addObject("result", "false");
            mav.setViewName("login");
        }
        return mav;
    }
}


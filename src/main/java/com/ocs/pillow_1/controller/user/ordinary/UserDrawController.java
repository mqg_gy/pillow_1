package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDraw;
import com.ocs.pillow_1.service.user.ordinary.UserDrawService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/userDraw")
public class UserDrawController {
    @Resource
    UserDrawService userDrawService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建中奖记录
     *
     * @param token
     * @param drawMessage
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String drawMessage) {
        String userId;
        String message;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在！";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userDrawService.create(userId, drawMessage);
        if (data == "200") {
            code = "200";
            message = "创建成功";
        } else {
            code = "201";
            message = "创建失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找指定用户的所有中奖记录
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String message;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在！";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<UserDraw> data = userDrawService.findByUserId(userId);
        if (data != null) {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "查找失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找中奖记录详情
     *
     * @param drawId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String drawId) {
        String message;
        String code;
        Map param = new HashMap();
        UserDraw data = userDrawService.findById(drawId);
        if (data != null) {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "查找失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


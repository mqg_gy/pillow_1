package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDoctor;
import com.ocs.pillow_1.service.user.ordinary.UserDoctorService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Controller
@RequestMapping("/userDoctor")
public class UserDoctorController {
    @Resource
    UserDoctorService userDoctorService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建医生实名认证信息，认证状态默认是0：审核中
     *
     * @param token
     * @param licPositivePhoto
     * @param licBackPhoto
     * @param workPhoto
     * @param introduce
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String licPositivePhoto, String licBackPhoto, String workPhoto, String introduce) {
        String userId;
        String message;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在！";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userDoctorService.create(userId, licPositivePhoto, licBackPhoto, workPhoto, introduce);
        if (data == "200") {
            code = "200";
            message = "创建成功";
        } else {
            code = "201";
            message = "创建失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找实名认证信息
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public UserDoctor findById(String token) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return null;
        }
        return userDoctorService.findById(userId);
    }

    /**
     * 修改用户的审核状态
     *
     * @param token
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String token, int state) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return userDoctorService.updateState(userId, state);
    }

}


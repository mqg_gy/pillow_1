package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserWallet;
import com.ocs.pillow_1.service.user.ordinary.UserTradingrecordService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户钱包前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Controller
@RequestMapping("/userWallet")
public class UserWalletController {
    @Resource
    UserWalletService userWalletService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserTradingrecordService userTradingrecordService = null;

    /**
     * 创建用户钱包
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String save(String userId) {
        return userWalletService.create(userId);
    }

    /**
     * 查找用户钱包
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        UserWallet data = userWalletService.findById(userId);
        if (data != null) {
            code = "200";
            mes = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "查找失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 用户余额充值
     *
     * @param token
     * @param amount
     * @param method
     * @return
     */
    @ResponseBody
    @RequestMapping("/addBalance")
    public Object addBalance(String token, Double amount, String method) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        try {
            userWalletService.addBalance(userId, amount);
            userTradingrecordService.create(+amount, method, "6", userId, null);
            code = "200";
            mes = "添加成功";
        } catch (Exception e) {
            code = "201";
            mes = "添加失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 用户余额提现
     *
     * @param token
     * @param amount
     * @return
     */
    @ResponseBody
    @RequestMapping("/reduceBalance")
    public Object reduceBalance(String token, Double amount) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        try {
            userWalletService.reduceBalance(userId, amount);
            userTradingrecordService.create(-amount, "钱包", "7", userId, null);
            code = "200";
            mes = "添加成功";
        } catch (Exception e) {
            code = "201";
            mes = "添加失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 用户购物后返还积分接口
     *
     * @param token
     * @param amount
     * @return
     */
    @ResponseBody
    @RequestMapping("/addIntegral")
    public Object addIntegral(String token, Double amount) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = userWalletService.addIntegral(userId, amount);
        if (data == "200") {
            code = "200";
            mes = "添加成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "添加失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 用户在积分商城购物消耗积分接口
     *
     * @param token
     * @param amount
     * @return
     */
    @ResponseBody
    @RequestMapping("/reduceIntegral")
    public Object reduceIntegral(String token, Double amount) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = userWalletService.reduceIntegral(userId, amount);
        if (data == "200") {
            code = "200";
            mes = "减少成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "减少失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 设置或
     *
     * @param token
     * @param payNumber
     * @return
     */
    @ResponseBody
    @RequestMapping("/updatePayNumber")
    public Object updatePayNumber(String token, String payNumber) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = userWalletService.updatePayNumber(userId, payNumber);
        if (data == "200") {
            code = "200";
            mes = "成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 密码校对
     *
     * @param token
     * @param payNumber
     * @return
     */
    @ResponseBody
    @RequestMapping("/chenkPay")
    public Object chenkPay(String token, String payNumber) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = userWalletService.chenkPay(userId, payNumber);
        if (data == "200") {
            code = "200";
            mes = "密码正确";
        } else if (data == "204") {
            code = "204";
            mes = "密码未设置";
        } else {
            code = "201";
            mes = "密码错误";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;

        /*UserWallet userWallet=userWalletService.findById(userId);
        if (userWallet.getPayNumber()==null){
            return "密码未设置";
        }
        if (payNumber.equals(userWallet.getPayNumber())){
            return "密码正确";
        }else{
            return "密码错误";
        }*/
    }
}


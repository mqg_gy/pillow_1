package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserVouchers;
import com.ocs.pillow_1.service.user.ordinary.UserVouchersService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Controller
@RequestMapping("/userVouchers")
public class UserVouchersController {
    @Resource
    UserVouchersService userVouchersService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 添加用户的优惠卷
     *
     * @param voucherId
     * @param token
     * @param source
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String voucherId, String token, String source) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return userVouchersService.create(voucherId, userId, source);
    }

    /**
     * 删除优惠卷
     *
     * @param id
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String id) {
        String mes;
        String code;
        Map param = new HashMap();
        String data = userVouchersService.delete(id);
        if (data == "200") {
            code = "200";
            mes = "删除成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "删除失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找用户所有的优惠券
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<UserVouchers> data = userVouchersService.findByUserId(userId);
        if (data != null) {
            code = "200";
            mes = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "查找失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 未使用的优惠卷
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/validVouchers")
    public Object validVouchers(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<UserVouchers> data = userVouchersService.validVouchers(userId);
        if (data != null) {
            code = "200";
            mes = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "查找失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 已使用的优惠卷
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/usedVouchers")
    public Object usedVouchers(String token) {
        String userId;
        String mes;
        String code;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "该token不存在！";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<UserVouchers> data = userVouchersService.usedVouchers(userId);
        if (data != null) {
            code = "200";
            mes = "查找成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "查找失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }
}


package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.entity.user.ordinary.UserToken;
import com.ocs.pillow_1.mapper.user.ordinary.UserDetailMapper;
import com.ocs.pillow_1.mapper.user.ordinary.UserMapper;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import com.ocs.pillow_1.service.user.ordinary.UserTokenService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户token前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Controller
@RequestMapping("/userToken")
public class UserTokenController {
    @Resource
    UserTokenService userTokenService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserService userService = null;
    @Resource
    UserDetailMapper userDetailMapper = null;

    /**
     * 判断用户的token是否已经存在
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping("/login")
    public Object login(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("systemType") Integer systemType) {
        String code;
        String message;
        Map param = new HashMap();
        UserToken data = userTokenService.login(username, password, systemType);
        User user = userService.findByUsername(username);
        if (user == null) {
            code = "202";
            message = "未注册";
        } else {
            String userId = user.getUserId();
            if (data == null) {
                code = "201";
                message = "登录失败";
            } else {
                code = "200";
                message = "登录成功";
                param.put("data", data);
                if (userDetailMapper.findInfo(userId) == null) {
                    String info = "0";
                    param.put("info", info);
                } else {
                    String info = "1";
                    param.put("info", info);
                }
            }
        }
        param.put("code", code);
        param.put("message", message);
        param.put("user", user);
        return param;
    }

    /**
     * 根据每一次前端传回的token，获得需要的userId等信息
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByToken")
    public UserToken findByToken(String token) {
        return userTokenService.findByToken(token);
    }

    @ResponseBody
    @RequestMapping("/wechatLogin")
    public Object wechatLogin(@RequestParam("openId") String openId, @RequestParam("systemType") Integer systemType) {
        String code;
        String message;
        Map param = new HashMap();
        UserToken data = userTokenService.wechatLogin(openId, systemType);
        User user = userService.findByOpenId(openId);
        if (data == null || "null".equals(data.getUserId())) {
            code = "202";
            message = "未注册";
        } else {
            String userId = data.getUserId();
            if (data == null) {
                code = "201";
                message = "登录失败";
            } else {
                code = "200";
                message = "登录成功";
                param.put("data", data);
                if (userDetailMapper.findInfo(userId) == null) {
                    String info = "0";
                    param.put("info", info);
                } else {
                    String info = "1";
                    param.put("info", info);
                }
            }
        }
        param.put("code", code);
        param.put("message", message);
        param.put("user", user);
        return param;
    }
}

package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserCard;
import com.ocs.pillow_1.mapper.user.ordinary.UserCardMapper;
import com.ocs.pillow_1.service.user.ordinary.UserCardService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户银行卡前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Controller
@RequestMapping("/userCard")
public class UserCardController {
    @Resource
    UserCardService userCardService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 绑定银行卡
     *
     * @param token
     * @param type
     * @param number
     * @param name
     * @param phone
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String type, String number, String name, String phone) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCardService.create(userId, type, number, name, phone);
        if (data == "201") {
            code = "201";
            message = "创建失败";
        } else if (data == "204") {
            code = "204";
            message = "未完成实名认证";
        } else {
            code = "200";
            message = "创建成功";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找用户的银行卡
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<UserCard> data = userCardService.findByUserId(userId);
        if (data == null) {
            code = "201";
            message = "查找失败";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 银行卡解绑接口
     *
     * @param token
     * @param cardId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String token, String cardId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCardService.delete(cardId);
        if (data == "201") {
            code = "201";
            message = "删除失败";
        } else {
            code = "200";
            message = "删除成功";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


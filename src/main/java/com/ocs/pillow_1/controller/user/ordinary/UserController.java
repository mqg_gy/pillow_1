package com.ocs.pillow_1.controller.user.ordinary;


import com.alibaba.fastjson.JSON;
import com.ocs.pillow_1.entity.relation.ordinary.RelationGroup;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserToken;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.relation.ordinary.RelationGroupService;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import com.ocs.pillow_1.service.user.ordinary.UserTokenService;
import com.ocs.pillow_1.service.util.UtilService;


import org.omg.CORBA.INTERNAL;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-04
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Resource
    UserService userService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserTokenService userTokenService = null;
    @Resource
    DeviceRelationService deviceRelationService = null;

    /**
     * 用户注册第一步
     *
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object Save(@RequestParam("username") String username, @RequestParam("password") String password) {
        Map param = new HashMap();
        String code = "200";
        Object message = userService.createOne(username, password);
        Object data = null;
        if (message == "201") {
            code = "201";
            message = "注册失败";
        } else if (message == "202") {
            code = "202";
            message = "该用户已经存在";
        } else {
            data = message;
            message = "注册成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 用户注册第二步 资料填写
     *
     * @param username
     * @return
     */
    @ResponseBody
    @RequestMapping("/saveinfo")
    public Object Saveinfo(@RequestParam("username") String username, @RequestParam("birthday") String birthday, @RequestParam("sex") String sex, @RequestParam("weight") Double weight, @RequestParam("wakeupTime") String wakeupTime, @RequestParam("idealWakeupTime") String idealWakeupTime, String loadWay, @RequestParam("openId") String openId) throws ParseException {
        Map param = new HashMap();
        String code = "200";
        Object message = userService.create(username, birthday, sex, weight, wakeupTime, idealWakeupTime, loadWay, openId);
        Object data = null;
        if (message == "201") {
            code = "201";
            message = "资料填写失败";
        } else {
            data = message;
            message = "资料填写成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 修改昵称或者用户名
     *
     * @param token
     * @param username
     * @param nickname
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateNickname")
    public Object updateNickandName(@RequestParam("token") String token, String username, String nickname) {
        System.out.println("nickname" + nickname);
        String userId;
        Map param = new HashMap();
        String message;
        String code = "200";
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        if (userService.updateNickandName(token, username, nickname) == "201") {
            code = "201";
            message = "修改失败";
        } else {
            code = "200";
            message = "修改成功";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 根据Id查找用户
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String userId) {
        Map param = new HashMap();
        String code;
        String message;
        if (userService.findById(userId) == null) {
            code = "201";
            message = "该用户不存在";
        } else {
            code = "200";
            message = "查找成功";
            User data = userService.findById(userId);
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 密码找回或修改
     *
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping("/updatePassword")
    public Object updatePassword(@RequestParam("username") String username, @RequestParam("password") String password) {
        String code;
        String message;
        Map param = new HashMap();
        String data = userService.updatePassword(username, password);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 根据用户名查找用户
     *
     * @param username
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUsername")
    public Object findByUsername(String username) {
        String code;
        String message;
        Map param = new HashMap();
        User data = userService.findByUsername(username);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 注册页面
     *
     * @return
     */
    @RequestMapping("/register")
    public ModelAndView register() {
        return new ModelAndView("register");
    }


    /**
     * 注册短信发送接口
     *
     * @param phone
     * @return
     */
    @RequestMapping("/sendSms")
    @ResponseBody
    public Object getCheck(String phone) throws IOException {
        Map param = new HashMap();
        String code;
        String message;
        String data = null;
        User existUser = userService.findByUsername(phone);
        if (existUser != null) {
            code = "201";
            message = "该手机号已经被注册";
        } else {
            code = "200";
            message = "可以注册";
            data = userService.sendNote(phone);
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 发送短信
     *
     * @param phone
     * @return
     */
    @ResponseBody
    @RequestMapping("/sendNote")
    public Object sendNote(String phone) throws IOException {
        Map param = new HashMap();
        String code;
        String message;
        String data = userService.sendNote(phone);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * openId获取用户名密码
     *
     * @param openId
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("findUserByOpenId")
    public Object findUserByOpenId(String openId) throws IOException {
        Map param = new HashMap();
        String code;
        String message;
        String data = userService.findUserInfoByOpenId(openId);
        if ("null".equals(data)) {
            code = "201";
            message = "失败";
        } else {
            code = "200";
            message = "成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 用户注册第一步
     *
     * @param username
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping("/userRegister")
    public Object userRegister(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("sn") String sn) {
        Map param = new HashMap();
        String code = "200";
        Object message = userService.createOne(username, password);
        Object data = null;
        if (message == "201") {
            code = "201";
            message = "注册失败";
        } else if (message == "202") {
            code = "202";
            message = "该用户已经存在";
        } else {
            String token = userTokenService.getUserToken(username).getToken();
            String userId = utilService.loginReturn(token);
            try {
                code = deviceRelationService.create(sn, userId);
                switch (code) {
                    case "200":
                        message = "成功";
                        break;
                    case "204":
                        message = "该设备已经被绑定！";
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                code = "201";
                message = "注册成功,设备绑定失败请重新绑定";
                param.put("code", code);
                param.put("message", message);
                return param;
            }
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


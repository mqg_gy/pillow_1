package com.ocs.pillow_1.controller.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserCar;
import com.ocs.pillow_1.service.user.ordinary.UserCarService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/userCar")
public class UserCarController {
    @Resource
    UserCarService userCarService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 向购物车中添加商品
     *
     * @param token
     * @param productId
     * @param type       商品类型。1：先购后返；2：拼团；3：私人定制；4：分销；5：积分
     * @param count
     * @param specValues
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String productId, Integer type, Integer count, String specValues) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCarService.create(userId, productId, type, count, specValues);
        if (data == "201") {
            code = "201";
            message = "创建失败";
        } else {
            code = "200";
            message = "创建成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找指定用户的购物车
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<UserCar> data = userCarService.findByUserId(userId);
        if (data == null) {
            code = "201";
            message = "查找失败";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除购物车中的物品
     *
     * @param token
     * @param shopId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String token, String shopId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCarService.delete(shopId);
        if (data == "201") {
            code = "201";
            message = "查找失败";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 点击+按钮，商品数量+1
     *
     * @param token
     * @param shopId
     * @return
     */
    @ResponseBody
    @RequestMapping("/upCount")
    public Object upCount(String token, String shopId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCarService.upCount(shopId);
        if (data == "201") {
            code = "201";
            message = "添加失败";
        } else {
            code = "200";
            message = "添加成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 点击-按钮，商品数量-1
     *
     * @param token
     * @param shopId
     * @return
     */
    @ResponseBody
    @RequestMapping("/downCount")
    public Object downCount(String token, String shopId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = userCarService.downCount(shopId);
        if (data == "201") {
            code = "201";
            message = "减少失败";
        } else {
            code = "200";
            message = "减少成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


package com.ocs.pillow_1.controller.goods.device;


import com.ocs.pillow_1.entity.goods.device.Device;
import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.goods.device.DeviceService;
import com.ocs.pillow_1.service.relation.ordinary.RelationService;
import com.ocs.pillow_1.service.util.UpDataService;
import com.ocs.pillow_1.service.util.UtilService;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/deviceRelation")
public class DeviceRelationController {
    @Resource
    DeviceRelationService deviceRelationService=null;
    @Resource
    UtilService utilService=null;
    @Resource
    UpDataService upDataService=null;
    @Resource
    RelationService relationService=null;
    @Resource
    DeviceService deviceService=null;

    /**
     * 用户激活设备之后，创建
     * 重新绑定设备的接口
     * @param sn
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String sn,String token){
        String userId;
        String code;
        String message=null;
        Map param=new HashMap();
        if (utilService.loginReturn(token)!=null){
            userId = utilService.loginReturn(token);
        }else {
            code="203";
            message="token不存在";
            param.put("code",code);
            param.put("message",message);
            return param;
        }
        try {
            String data = deviceRelationService.create(sn, userId);
            code = data;
            switch (data) {
                case "200":
                    message = "成功";
                    break;
                case "204":
                    message = "该设备已经被绑定！";
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
            code="201";
            message = "失败";
        }
        param.put("code",code);
        param.put("message",message);
        return param;
    }

    /**
     * 解绑设备接口
     * @param deviceId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public Object updateState(String deviceId,String token){
        String userId;
        String code;
        String message=null;
        Map param=new HashMap();
        if (utilService.loginReturn(token)!=null){
            userId = utilService.loginReturn(token);
        }else {
            code="203";
            message="token不存在";
            param.put("code",code);
            param.put("message",message);
            return param;
        }

        try {
            String data=deviceRelationService.updateState(deviceId,userId,1);
            if (data == "200") {
                code="200";
                message="成功";
            }else {
                code="201";
                message="设备已经被绑定";
            }
        }catch (Exception e){
            e.printStackTrace();
            code="201";
            message="执行错误";
        }
        param.put("code",code);
        param.put("message",message);
        return param;
    }

    /**
     * 查找指定用户是否有设备
     * @param userId
     * @return
     */
    @RequestMapping("/findByUserId")
    @ResponseBody
    public Object findByUserId(String userId){
        String code;
        String message=null;
        Map param=new HashMap();
        List<DeviceRelation> deviceRelations=deviceRelationService.findByUserId(userId);
        DeviceRelation deviceRelation=new DeviceRelation();
        deviceRelation=null;
        for (DeviceRelation d:deviceRelations) {
            if (d.getState()==0){
                deviceRelation=d;
            }
        }
        if (deviceRelation==null){
            code="201";
            message="无设备";
        }else {
            code="200";
            message="有设备";
            param.put("deviceRelation",deviceRelation);
            Device device=deviceService.findById(deviceRelation.getDeviceId());
            param.put("sn",device.getSn());
        }
        param.put("code",code);
        param.put("message",message);
        return param;
    }

}


package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductThumb;
import com.ocs.pillow_1.service.goods.product.ProductThumbService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.rmi.MarshalledObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Controller
@RequestMapping("/productThumb")
public class ProductThumbController {
    @Resource
    ProductThumbService productThumbService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 查找商品的所有点赞记录
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/thumbcount")
    public long findByProductId(String productId) {
        long count = productThumbService.findByProductId(productId).size();
        return count;

    }

    /**
     * 创建点赞记录
     *
     * @param productId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String productId, String token) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return productThumbService.create(productId, userId);
    }

    /**
     * 根据指定的peoductId和userId查找点赞记录
     *
     * @param productId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByProAndUser")
    public Object findByProAndUser(String productId, String token) {
        Map map = new HashMap();
        int code = 200;
        String message;
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            message = "token不存在";
            code = 201;
            map.put("code", code);
            map.put("message", message);
            return map;
        }
        ProductThumb data = productThumbService.findByProAndUser(productId, userId);
        map.put("code", code);
        map.put("data", data);
        return map;
    }

    /**
     * 删除点赞记录
     *
     * @param productThumbId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String productThumbId) {
        return productThumbService.delete(productThumbId);
    }

}


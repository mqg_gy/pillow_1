package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.service.goods.product.ProductShelvetimeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/productShelvetime")
public class ProductShelvetimeController {
    @Resource
    ProductShelvetimeService productShelvetimeService = null;

    /**
     * 商品上架接口
     *
     * @param productId
     * @param upUserId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String productId, String upUserId) {
        return productShelvetimeService.create(productId, upUserId);
    }

    /**
     * 商品下架接口
     *
     * @param productId
     * @param downUserId
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public String update(String productId, String downUserId) {
        return productShelvetimeService.update(productId, downUserId);
    }

}


package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSpecvalues;
import com.ocs.pillow_1.service.goods.product.ProductSpecvaluesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 规格值前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/productSpecvalues")
public class ProductSpecvaluesController {
    @Resource
    ProductSpecvaluesService productSpecvaluesService = null;

    /**
     * 创建规格值
     *
     * @param valueName
     * @param specId
     * @return
     */
    @ResponseBody
    @RequestMapping("/create")
    public String create(String valueName, long specId) {
        return productSpecvaluesService.create(valueName, specId);
    }

    /**
     * 根据规格项Id查找
     *
     * @param specValueId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public ProductSpecvalues findById(long specValueId) {
        return productSpecvaluesService.findById(specValueId);
    }

    /**
     * 更新
     *
     * @param specValueId
     * @param valueName
     */
    @ResponseBody
    @RequestMapping("/update")
    public String update(long specValueId, String valueName) {
        return productSpecvaluesService.update(specValueId, valueName);
    }

    /**
     * 删除
     *
     * @param specId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(long specId) {
        return productSpecvaluesService.delete(specId);
    }

    /**
     * 根据规格项查找
     *
     * @param specId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findBySpecId")
    public List<ProductSpecvalues> findBySpecId(long specId) {
        return productSpecvaluesService.findBySpecId(specId);
    }

    /**
     * 规格项级联删除
     *
     * @param specId
     */
    @ResponseBody
    @RequestMapping("/cascadeDelete")
    public String cascadeDelete(long specId) {
        return productSpecvaluesService.cascadeDelete(specId);
    }
}


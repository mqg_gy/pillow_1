package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductImages;
import com.ocs.pillow_1.service.goods.product.ProductImagesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/productImages")
public class ProductImagesController {
    @Resource
    ProductImagesService productImagesService = null;

    /**
     * 删除商品图实体
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String productId) {
        return productImagesService.delete(productId);
    }

    /**
     * 根据id查找商品图
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public ProductImages findById(String productId) {
        return productImagesService.findById(productId);
    }

    /**
     * 修改商品图
     *
     * @param productId
     * @param master
     * @param large
     * @param detail1
     * @param detail2
     * @param box
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public String update(String productId, String master, String large, String detail1, String detail2, String box) {
        return productImagesService.update(productId, master, large, detail1, detail2, box);
    }
}


package com.ocs.pillow_1.controller.goods.device;


import com.ocs.pillow_1.entity.goods.device.Device;
import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.goods.device.DeviceService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/device")
public class DeviceController {
    @Resource
    DeviceService deviceService = null;
    @Resource
    DeviceRelationService deviceRelationService = null;
    @Resource
    UtilService utilService = null;


    /**
     * 修改销售状态
     *
     * @param deviceId
     * @param isSaled
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateSaled")
    public String updateSaled(String deviceId, Integer isSaled) {
        return deviceService.updateSaled(deviceId, isSaled);
    }

    /**
     * 修改激活状态
     *
     * @param deviceId
     * @param isActivation
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateActivation")
    public String updateActivation(String deviceId, Integer isActivation) {
        return deviceService.updateActivation(deviceId, isActivation);
    }

    /**
     * 查找用户设备类型，用于客服沟通
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/deviceType")
    public String findType(String userId) {
        DeviceRelation relation = deviceRelationService.findIsExist(userId);
        Long type = 0l;//0：用户并没有设备，或设备没有型号
        if (relation != null) {
            type = deviceService.findById(relation.getDeviceId()).getType(); //查找设备串号
        }
        String data = "无类型";
        switch (type.intValue()) {
            case 1:
                data = "小月";
                break;
        }
        return data;
    }

    /**
     * 显示设备信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/show")
    public Object show(String deviceId) {
        Device device = deviceService.findById(deviceId);
        String model = device.getModel();
        String sn = device.getSn();
        String insurance = device.getInsurance();
        String source = device.getSource();
        String code;
        String message;
        Map param = new HashMap();
        Device data = deviceService.show(model, sn, insurance, source);
        if (data != null) {
            code = "200";
            message = "有数据";
//            Device data = deviceService.show(model, sn, insurance, source);
            param.put("data", data);
        } else {
            code = "201";
            message = "无数据";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


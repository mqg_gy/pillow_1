package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.Product;
import com.ocs.pillow_1.service.goods.product.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 商品前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/product")
public class ProductController {
    @Resource
    ProductService productService = null;

    /**
     * 创建商品
     *
     * @param name
     * @param originalPrice
     * @param price
     * @param specItems
     * @param describes
     * @param floorPrice
     * @param spellAmount
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String name, Double originalPrice, Double price, String specItems, String describes, Double floorPrice, Double spellAmount) {
        return productService.create(name, originalPrice, price, specItems, describes, floorPrice, spellAmount);
    }

    /**
     * 修改名字、原价、价格、规格项、描述、拼团底价
     *
     * @param productId
     * @param name
     * @param originalPrice
     * @param price
     * @param specItems
     * @param describes
     * @param floorPrice
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateBaseItem")
    public String updateBaseItem(String productId, String name, Double originalPrice, Double price, String specItems, String describes, Double floorPrice, Double spellAmount) {
        return productService.updateBaseItem(productId, name, originalPrice, price, specItems, describes, floorPrice, spellAmount);
    }

    /**
     * 修改是否上架、先购后返、私人定制、分销、积分购买、拼团
     *
     * @param productId
     * @param isShelves
     * @param isBack
     * @param isCustom
     * @param isDistribute
     * @param isIntegral
     * @param isSpell
     */
    @ResponseBody
    @RequestMapping("/updateIs")
    public String updateIs(String productId, Integer isShelves, Integer isBack, Integer isCustom, Integer isDistribute, Integer isIntegral, Integer isSpell) {
        return productService.updateIs(productId, isShelves, isBack, isCustom, isDistribute, isIntegral, isSpell);
    }

    /**
     * 根据Id查找
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Product findById(String productId) {
        return productService.findById(productId);
    }

    /**
     * 查询同一类型的商品。是否上架、先购后返、私人定制、分销、积分购买、拼团
     *
     * @param isShelves
     * @param isBack
     * @param isCustom
     * @param isDistribute
     * @param isIntegral
     * @param isSpell
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByIs")
    public List<Product> findByIs(Integer isShelves, Integer isBack, Integer isCustom, Integer isDistribute, Integer isIntegral, Integer isSpell) {
        return productService.findByIs(isShelves, isBack, isCustom, isDistribute, isIntegral, isSpell);
    }

    /**
     * 删除产品
     *
     * @param productId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String productId) {
        return productService.delete(productId);
    }
}


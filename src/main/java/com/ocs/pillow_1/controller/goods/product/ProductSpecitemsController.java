package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSpecitems;
import com.ocs.pillow_1.service.goods.product.ProductSpecitemsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 规格项前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/productSpecitems")
public class ProductSpecitemsController {
    @Resource
    ProductSpecitemsService productSpecitemsService = null;

    /**
     * 创建规格项
     *
     * @param specName
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String specName) {
        return productSpecitemsService.create(specName);
    }

    /**
     * 查找
     *
     * @param specId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public ProductSpecitems findById(long specId) {
        return productSpecitemsService.findById(specId);
    }

    /**
     * 更新
     *
     * @param specId
     * @param specName
     */
    @ResponseBody
    @RequestMapping("/update")
    public String update(long specId, String specName) {
        return productSpecitemsService.update(specId, specName);
    }

    /**
     * 删除
     *
     * @param specId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(long specId) {
        return productSpecitemsService.delete(specId);
    }

}


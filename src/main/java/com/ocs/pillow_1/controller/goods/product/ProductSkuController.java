package com.ocs.pillow_1.controller.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSku;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 库存前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/productSku")
public class ProductSkuController {
    @Resource
    ProductSkuService productSkuService = null;

    /**
     * 创建库存
     *
     * @param inventory
     * @param cost
     * @param productId
     * @param speValues
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(Long inventory, Double cost, String productId, String speValues) {
        return productSkuService.create(inventory, cost, productId, speValues);
    }

    /**
     * 根据Id查找
     *
     * @param skuId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public ProductSku findById(String skuId) {
        return productSkuService.findById(skuId);
    }

    /**
     * 根据商品Id查找
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByProductId")
    public List<ProductSku> findByProductId(String productId) {
        return productSkuService.findByProductId(productId);
    }

    /**
     * 查找所有库存
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/findAll")
    public List<ProductSku> findAll() {
        return productSkuService.findAll();
    }

    /**
     * 更新库存单位
     *
     * @param skuId
     * @param cost
     * @param speValues
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public String update(String skuId, Double cost, String speValues) {
        return productSkuService.update(skuId, cost, speValues);
    }

    /**
     * 删除库存记录
     *
     * @param skuId
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String skuId) {
        return productSkuService.delete(skuId);
    }

    /**
     * 获得库存的id
     *
     * @param productId
     * @param speValues
     * @return
     */
    public ProductSku getSkuId(String productId, String speValues) {
        return productSkuService.getSkuId(productId, speValues);
    }

    /**
     * 更新库存数量
     *
     * @param skuId
     * @param inventory
     * @return
     */
    public String updateInventory(String skuId, long inventory) {
        return productSkuService.updateInventory(skuId, inventory);
    }
}


package com.ocs.pillow_1.controller.order.exchangebyintegral;


import com.ocs.pillow_1.service.order.exchangebyintegral.OrderIntegralbaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderIntegralbase")
public class OrderIntegralbaseController {
    @Resource
    OrderIntegralbaseService orderIntegralbaseService = null;

    /**
     * 创建基本
     *
     * @param productId
     * @param distance
     * @param needIntegral
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String productId, Double distance, Double needIntegral) {
        return orderIntegralbaseService.create(productId, distance, needIntegral);
    }

    /**
     * 修改额外的差价
     *
     * @param productId
     * @param distance
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateDistance")
    public String updateDistance(String productId, Double distance) {
        return orderIntegralbaseService.updateDistance(productId, distance);
    }

    /**
     * 修改所需的积分
     *
     * @param productId
     * @param needIntegral
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateIntegral")
    public String updateIntegral(String productId, Double needIntegral) {
        return orderIntegralbaseService.updateIntegral(productId, needIntegral);
    }

    /**
     * 删除
     *
     * @param productId
     */
    public String delete(String productId) {
        return orderIntegralbaseService.delete(productId);
    }
}


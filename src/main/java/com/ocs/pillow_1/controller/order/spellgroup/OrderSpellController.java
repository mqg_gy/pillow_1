package com.ocs.pillow_1.controller.order.spellgroup;


import com.ocs.pillow_1.entity.order.spellgroup.OrderSpell;
import com.ocs.pillow_1.service.order.spellgroup.OrderSpellService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderSpell")
public class OrderSpellController {
    @Resource
    OrderSpellService orderSpellService = null;

    /**
     * 全款购买、参与拼团接口
     *
     * @param productId
     * @param payMoney
     * @param count
     * @param groupId
     * @param speValues
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String productId, Double payMoney, Long count, String groupId, String speValues, String userId) {
        return orderSpellService.create(productId, payMoney, count, groupId, speValues, userId);
    }

    /**
     * 查找一个团的订单
     *
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByGroupId")
    public List<OrderSpell> findByGroupId(String groupId) {
        return orderSpellService.findByGroupId(groupId);
    }

    /**
     * 修改订单状态
     *
     * @param orderId
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String orderId, Integer state) {
        return orderSpellService.updateState(orderId, state);
    }
}


package com.ocs.pillow_1.controller.order.exchangebyintegral;


import com.ocs.pillow_1.service.order.exchangebyintegral.OrderIntegralService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderIntegral")
public class OrderIntegralController {
    @Resource
    OrderIntegralService orderIntegralService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建订单
     *
     * @param token
     * @param orderId
     * @param payMoney
     * @param count
     * @param productId
     * @param speValues
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String token, String orderId, Double payMoney, Long count, String productId, String speValues) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return orderIntegralService.create(orderId, payMoney, count, productId, userId, speValues);
    }

    /**
     * 修改订单状态
     *
     * @param orderId
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String orderId, Integer state) {
        return orderIntegralService.updateState(orderId, state);
    }

}


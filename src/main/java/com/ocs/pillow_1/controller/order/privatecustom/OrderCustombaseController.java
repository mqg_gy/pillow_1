package com.ocs.pillow_1.controller.order.privatecustom;


import com.ocs.pillow_1.entity.order.privatecustom.OrderCustombase;
import com.ocs.pillow_1.service.order.privatecustom.OrderCustombaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 私人定制商品列表实体（提前写下的）前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderCustombase")
public class OrderCustombaseController {
    @Resource
    OrderCustombaseService orderCustombaseService = null;

    /**
     * 创建项
     *
     * @param distancePrice
     * @param noteItem
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(Double distancePrice, String noteItem, String productId) {
        return orderCustombaseService.create(distancePrice, noteItem, productId);
    }

    /**
     * 查找同一商品
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByProductId")
    public List<OrderCustombase> findByProductId(String productId) {
        return orderCustombaseService.findByProductId(productId);
    }

    /**
     * 修改项目差价
     *
     * @param customId
     * @param distancePrice
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateDistancePrice")
    public String updateDistancePrice(String customId, Double distancePrice) {
        return orderCustombaseService.updateDistancePrice(customId, distancePrice);
    }

    /**
     * 修改定制类目
     *
     * @param customId
     * @param noteItem
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateNoteItem")
    public String updateNoteItem(String customId, String noteItem) {
        return orderCustombaseService.updateNoteItem(customId, noteItem);
    }
}


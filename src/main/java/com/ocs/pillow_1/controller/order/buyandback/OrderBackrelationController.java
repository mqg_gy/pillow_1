package com.ocs.pillow_1.controller.order.buyandback;


import com.ocs.pillow_1.service.order.buyandback.OrderBackrelationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * <p>
 * 返还用户关系前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
@Controller
@RequestMapping("/orderBackrelation")
public class OrderBackrelationController {
    @Resource
    OrderBackrelationService orderBackrelationService = null;

    /**
     * 创建关系
     *
     * @param backOrderId
     * @param subUserId
     * @return
     */
    public String create(String backOrderId, String subUserId) {
        return orderBackrelationService.create(backOrderId, subUserId);
    }

}


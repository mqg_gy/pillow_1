package com.ocs.pillow_1.controller.order.spellgroup;


import com.ocs.pillow_1.entity.order.spellgroup.OrderSpellbase;
import com.ocs.pillow_1.service.order.spellgroup.OrderSpellbaseService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderSpellbase")
public class OrderSpellbaseController {
    @Resource
    OrderSpellbaseService orderSpellbaseService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 开团
     *
     * @param productId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String token, String productId, Double payMoney, Long count, String speValues) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return orderSpellbaseService.create(productId, payMoney, count, speValues, userId);
    }

    /**
     * 查找团
     *
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public OrderSpellbase findById(String groupId) {
        return orderSpellbaseService.findById(groupId);
    }

    /**
     * 查找某个商品的团
     *
     * @param productId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findSpelling")
    public List<OrderSpellbase> findSpelling(String productId) {
        return orderSpellbaseService.findSpelling(productId);
    }

    /**
     * 拼团人数增加
     *
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/addCount")
    public String addCount(String groupId) {
        return orderSpellbaseService.addCount(groupId);
    }

    /**
     * 团成立
     *
     * @param groupId
     */
    @ResponseBody
    @RequestMapping("/endGroup")
    public String endGroup(String groupId) {
        return orderSpellbaseService.endGroup(groupId);
    }
}


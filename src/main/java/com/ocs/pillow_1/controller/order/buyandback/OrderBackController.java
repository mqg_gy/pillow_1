package com.ocs.pillow_1.controller.order.buyandback;


import com.ocs.pillow_1.service.order.buyandback.OrderBackService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
@Controller
@RequestMapping("/orderBack")
public class OrderBackController {
    @Resource
    OrderBackService orderBackService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建购物订单
     *
     * @param orderId   前端生成编号，一个购物记录中有几个商品，公用一个orderId
     * @param paymoney
     * @param count
     * @param productId
     * @param token
     * @param speValues
     * @param parentId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String orderId, Double paymoney, Long count, String productId, String token, String speValues, String parentId) {
        String userId;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            return "token不存在";
        }
        return orderBackService.create(orderId, paymoney, count, productId, userId, speValues, parentId);
    }

    /**
     * 修改订单状态
     *
     * @param orderId
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String orderId, Integer state) {
        return orderBackService.updateState(orderId, state);
    }
}


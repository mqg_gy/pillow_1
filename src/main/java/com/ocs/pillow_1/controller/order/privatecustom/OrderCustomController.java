package com.ocs.pillow_1.controller.order.privatecustom;


import com.ocs.pillow_1.service.order.privatecustom.OrderCustomService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/orderCustom")
public class OrderCustomController {
    @Resource
    OrderCustomService orderCustomService = null;

    /**
     * 创建订单
     *
     * @param orderId
     * @param payMoney
     * @param count
     * @param noteItem
     * @param productId
     * @param note
     * @param userId
     * @param speValues
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String orderId, Double payMoney, Long count, String noteItem, String productId, String note, String userId, String speValues) {
        return orderCustomService.create(orderId, payMoney, count, noteItem, productId, note, userId, speValues);
    }

    /**
     * 修改订单状态
     *
     * @param orderId
     * @param state
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateState")
    public String updateState(String orderId, Integer state) {
        return orderCustomService.updateState(orderId, state);
    }
}


package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.service.article.article.ArticleThumbService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 文章点赞记录前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleThumb")
public class ArticleThumbController {
    @Resource
    ArticleThumbService articleThumbService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 文章点赞接口
     *
     * @param articleId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String articleId, String token) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleThumbService.create(articleId, userId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else if (data == "204") {
            code = "204";
            message = "点赞记录已经存在";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除文章点赞记录
     *
     * @param articleThumbId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String articleThumbId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleThumbService.delete(articleThumbId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


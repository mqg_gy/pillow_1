package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.entity.article.article.Article;
import com.ocs.pillow_1.service.article.article.ArticleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/article")
public class ArticleController {
    @Resource
    ArticleService articleService = null;

    /**
     * 查找
     *
     * @param articleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String articleId) {
        String code;
        String message;
        Map param = new HashMap();
        Article data = articleService.findById(articleId);
        if (data != null) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找同一类型的文章
     *
     * @param articleTypeId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByTypeId")
    public Object findByTypeId(String articleTypeId) {
        String code;
        String message;
        Map param = new HashMap();
        List<Article> data = articleService.findByTypeId(articleTypeId);
        if (data != null) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


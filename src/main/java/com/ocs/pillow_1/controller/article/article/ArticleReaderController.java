package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleReader;
import com.ocs.pillow_1.mapper.util.UtilMapper;
import com.ocs.pillow_1.service.article.article.ArticleReaderService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章阅读记录前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleReader")
public class ArticleReaderController {
    @Resource
    ArticleReaderService articleReaderService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 添加阅读记录
     *
     * @param articleId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String articleId, String token) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleReaderService.create(articleId, userId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else if (data == "204") {
            code = "204";
            message = "阅读记录已存在";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查询一篇文章的所有阅读记录
     *
     * @param articleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/readerList")
    public Object readerList(String articleId) {
        Map param = new HashMap();
        String message;
        String code;
        List<ArticleReader> data = articleReaderService.readerList(articleId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleComment;
import com.ocs.pillow_1.service.article.article.ArticleCommentService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章评论前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleComment")
public class ArticleCommentController {
    @Resource
    ArticleCommentService articleCommentService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 增加评论记录
     *
     * @param articleId
     * @param commentContent
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String articleId, String commentContent, String token) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleCommentService.create(articleId, commentContent, userId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找评论
     *
     * @param commentId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String commentId) {
        Map param = new HashMap();
        String message;
        String code;
        ArticleComment data = articleCommentService.findById(commentId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除评论记录
     *
     * @param commentId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String commentId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleCommentService.delete(commentId);
        if (data == "200") {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找同一篇文章的评论记录
     *
     * @param articleId
     * @return
     */
    @ResponseBody
    @RequestMapping("/commentList")
    public Object commentList(String articleId) {
        Map param = new HashMap();
        String message;
        String code;
        List<ArticleComment> data = articleCommentService.commentList(articleId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


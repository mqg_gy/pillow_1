package com.ocs.pillow_1.controller.article.review;


import com.ocs.pillow_1.entity.article.review.ArticleReview;
import com.ocs.pillow_1.service.article.review.ArticleReviewService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleReview")
public class ArticleReviewController {
    @Resource
    ArticleReviewService articleReviewService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建评论的点评记录
     *
     * @param token
     * @param reviewContent
     * @param commentId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String token, String reviewContent, String commentId) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleReviewService.create(userId, reviewContent, commentId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找点评记录
     *
     * @param reviewId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String reviewId) {
        Map param = new HashMap();
        String message;
        String code;
        ArticleReview data = articleReviewService.findById(reviewId);
        if (data != null) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除点评记录
     *
     * @param reviewId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String reviewId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleReviewService.delete(reviewId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找评论下的所有点评
     *
     * @param commentId
     * @return
     */
    @ResponseBody
    @RequestMapping("/reviewList")
    public Object reviewList(String commentId) {
        Map param = new HashMap();
        String message;
        String code;
        List<ArticleReview> data = articleReviewService.reviewList(commentId);
        if (data != null) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


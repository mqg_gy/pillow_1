package com.ocs.pillow_1.controller.article.reply;


import com.ocs.pillow_1.controller.util.UtilController;
import com.ocs.pillow_1.entity.article.reply.ArticleReply;
import com.ocs.pillow_1.service.article.reply.ArticleReplyService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleReply")
public class ArticleReplyController {
    @Resource
    ArticleReplyService articleReplyService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 查找回复
     *
     * @param replyId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findById")
    public Object findById(String replyId) {
        Map param = new HashMap();
        String code;
        String message;
        ArticleReply data = articleReplyService.findById(replyId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 创建回复
     *
     * @param replyContent
     * @param reviewId
     * @param token
     * @param atReplyId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String replyContent, String reviewId, String token, String atReplyId) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleReplyService.create(replyContent, reviewId, userId, atReplyId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除回复记录
     *
     * @param replyId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String replyId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleReplyService.delete(replyId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查询同一点评下的所有回复
     *
     * @param reviewId
     * @return
     */
    @ResponseBody
    @RequestMapping("/replyList")
    public Object replyList(String reviewId) {
        Map param = new HashMap();
        String message;
        String code;
        List<ArticleReply> data = articleReplyService.replyList(reviewId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


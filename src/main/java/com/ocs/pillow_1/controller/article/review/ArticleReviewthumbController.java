package com.ocs.pillow_1.controller.article.review;


import com.ocs.pillow_1.service.article.review.ArticleReviewthumbService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleReviewthumb")
public class ArticleReviewthumbController {
    @Resource
    ArticleReviewthumbService articleReviewthumbService = null;

    /**
     * 创建点评点赞记录
     *
     * @param reviewId
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public String create(String reviewId, String userId) {
        return articleReviewthumbService.create(reviewId, userId);
    }

    /**
     * 删除点赞记录
     *
     * @param reviewThumbId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public String delete(String reviewThumbId) {
        return articleReviewthumbService.delete(reviewThumbId);
    }

}


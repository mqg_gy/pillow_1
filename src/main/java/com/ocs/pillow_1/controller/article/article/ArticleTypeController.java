package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleType;
import com.ocs.pillow_1.service.article.article.ArticleTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 文章类型前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleType")
public class ArticleTypeController {
    @Resource
    ArticleTypeService articleTypeService = null;

    /**
     * 查找类型
     *
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByCode")
    public ArticleType findByCode(Integer code) {
        return articleTypeService.findByCode(code);
    }

    /**
     * 查询所有的栏目类型
     */
    @ResponseBody
    @RequestMapping("/findAll")
    public List<ArticleType> findAll() {
        return articleTypeService.findAll();
    }

    /**
     * 修改栏目名称
     *
     * @param articleTypeId
     * @param typeName
     */
    @ResponseBody
    @RequestMapping("/updateName")
    public String updateName(String articleTypeId, String typeName) {
        return articleTypeService.updateName(articleTypeId, typeName);
    }
}


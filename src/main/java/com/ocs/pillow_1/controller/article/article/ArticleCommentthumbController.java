package com.ocs.pillow_1.controller.article.article;


import com.ocs.pillow_1.service.article.article.ArticleCommentthumbService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 文章评论的点赞前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleCommentthumb")
public class ArticleCommentthumbController {
    @Resource
    ArticleCommentthumbService articleCommentthumbService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建评论的点赞记录
     *
     * @param commentId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String commentId, String token) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleCommentthumbService.create(commentId, userId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else if (data == "204") {
            code = "204";
            message = "点赞记录已经存在";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除评论的点赞记录
     *
     * @param commentThumbId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String commentThumbId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleCommentthumbService.delete(commentThumbId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


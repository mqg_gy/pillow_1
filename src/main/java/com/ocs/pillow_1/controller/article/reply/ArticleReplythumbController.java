package com.ocs.pillow_1.controller.article.reply;


import com.ocs.pillow_1.service.article.reply.ArticleReplythumbService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 回复点赞前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/articleReplythumb")
public class ArticleReplythumbController {
    @Resource
    ArticleReplythumbService articleReplythumbService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建点赞记录
     *
     * @param replyId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(String replyId, String token) {
        String userId;
        Map param = new HashMap();
        String message;
        String code;
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = articleReplythumbService.create(replyId, userId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else if (data == "204") {
            code = "204";
            message = "点赞记录已存在";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 取消点赞记录
     *
     * @param replyThumbId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String replyThumbId) {
        Map param = new HashMap();
        String message;
        String code;
        String data = articleReplythumbService.delete(replyThumbId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


package com.ocs.pillow_1.controller.relation.ordinary;


import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.relation.ordinary.RelationRequestService;
import com.ocs.pillow_1.service.relation.ordinary.RelationResponseService;
import com.ocs.pillow_1.service.relation.ordinary.RelationService;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.util.UtilService;
import org.apache.http.protocol.ResponseServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 关系前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/relation")
public class RelationController {
    @Resource
    RelationService relationService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserDetailService userDetailService = null;
    @Resource
    DeviceRelationService deviceRelationService = null;
    @Resource
    RelationResponseService relationResponseService = null;

    /**
     * 好友列表接口
     * 用户的好友列表调用结果，授权修改等接口已经获得了该条记录
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }

        List<Relation> careMe = relationService.careMe(userId);
        List<Relation> iCare = relationService.ICare2(userId);
        List group = relationService.findGroup(userId);
        if (careMe == null && iCare == null && group == null) {
            code = "201";
            message = "无数据";
        } else {
            code = "200";
            message = "成功";
            param.put("careMe", careMe);
            param.put("iCare", iCare);
            param.put("group", group);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 添加好友
     * @param token
     * @param friendId
     * @return
     */
   /* @ResponseBody
    @RequestMapping("/save")
    public String create(String token,String friendId){
        String userId;
        if (utilService.loginReturn(token)!=null){
            userId = utilService.loginReturn(token);
        }else {
            return "token不存在";
        }
        return relationService.create(friendId,userId);
    }*/

    /**
     * 好友列表接口
     * 用户的好友列表调用结果，授权修改等接口已经获得了该条记录
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserIds")
    public Object findByUserIds(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<Relation> data = relationService.findByUserId(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 好友权限列表
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findPerFriend")
    public Object findPerFriend(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<Relation> data = relationService.friendPermission(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 修改好友的备注，或权限，或角色
     *
     * @param token
     * @param friendId
     * @param relationId
     * @param note
     * @param permission
     * @param role
     * @return
     */
    @ResponseBody
    @RequestMapping("/update")
    public Object update(String token, String friendId, String relationId, String note, Integer permission, Integer role, String description) {
        String userId;
        String code;
        String message = null;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = relationService.update(userId, friendId, relationId, note, permission, role, description);
        code = data;
        switch (data) {
            case "200":
                message = "成功";
                break;
            case "201":
                message = "失败";
                break;
            case "202":
                message = "该好友没有查看报告的权限";
                break;
            case "203":
                message = "亲情榜已满";
                break;
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 关心我
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/careMe")
    public Object careMe(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<Relation> data = relationService.careMe(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 亲情榜
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/affectionList")
    public Object affectionList(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<Relation> data = relationService.affectionList(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 删除好友
     *
     * @param friendId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(String friendId, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }

        String data = relationService.delete(friendId, userId);
        if (data != null) {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 判断是否是好友
     *
     * @param friendId
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByFandU")
    public Object findByFandU(String friendId, String userId) {
        String code;
        String message;
        Map param = new HashMap();
        Relation data = relationService.findByFandU(friendId, userId);
        if (data != null) {
            code = "200";
            message = "是好友";
        } else {
            code = "201";
            message = "不是好友";
        }
        data.setFriend(userDetailService.findById(friendId));
        param.put("data", data);
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 给好友添加进分组
     *
     * @param relationId
     * @param groupId
     * @return
     */
    @RequestMapping("/addGroup")
    @ResponseBody
    public Object addGroup(String relationId, String groupId) {
        String code;
        String message;
        Map param = new HashMap();
        String data = relationService.addGroup(relationId, groupId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 将好友移出分组
     *
     * @param relationId
     * @return
     */
    @RequestMapping("/removeGroup")
    @ResponseBody
    public Object removeGroup(String relationId) {
        String code;
        String message;
        Map param = new HashMap();
        String data = relationService.removeGroup(relationId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 分组查找指定用户的好友
     *
     * @param token
     * @return
     */
    @RequestMapping("/findGroup")
    @ResponseBody
    public Object findGroup(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List data = relationService.findGroup(userId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找陌生人
     *
     * @param token
     * @param friendName
     * @return
     */
    @ResponseBody
    @RequestMapping("/findStranger")
    public Object findStranger(String token, String friendName) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        Map data = relationService.findStranger(userId, friendName);
        if (data.isEmpty()) {
            code = "201";
            message = "失败";
        } else {
            code = "200";
            message = "成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查看我是否获取了好友报告的查看权限
     *
     * @param token
     * @param friendId
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkMyPremission")
    public Object checkMyPremission(String token, String friendId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        Relation relation=new Relation();
        relation=null;
        relation = relationService.findByFandU(friendId, userId);
        if (relation.getPermission() == 0||relation==null) {
            code = "201";
            message = "无权限";
        }
        else {
            code = "200";
            message = "有权限";
        }

        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查看是否亲人
     *
     * @param token
     * @param friendId
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkRole")
    public Object checkRole(String token, String friendId) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        Relation relation = relationService.findByFandU(friendId, userId);
        if (relation.getRole() == 1) {
            code = "200";
            message = "亲人";
        } else {
            code = "201";
            message = "普通朋友";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 将好友添加进入亲情榜
     *
     * @param friendId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/addAffection")
    public Object addAffection(String friendId, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }

        Relation relation = relationService.findByFandU(friendId, userId);//查找该好友与我的好友关系
        DeviceRelation deviceRelation = deviceRelationService.findIsExist(friendId);//查找该好友是否已经有设备
        if (relation != null && deviceRelation != null && relation.getPermission() == 1) {//好友关系存在，并且已经给了我权限
            List<Relation> list = relationService.findByUserId(userId);
            int count = 0;
            for (Relation r : list) {
                if (relation.getFriendId() != relation.getUserId() && relation.getPermission() == 1 && relation.getRole() == 1) {
                    count++;
                }
                if (count >= 4) {
                    code = "203";//亲情榜已满
                    message = "亲情榜已满";
                    param.put("code", code);
                    param.put("message", message);
                    return param;
                }
            }
            String data = relationService.addAffection(friendId, userId);
            if (data.equals("200")) {
                code = "200";
                message = "添加成功";
            } else {
                code = "201";
                message = "添加失败";
            }
        } else {
            code = "201";
            message = "该好友并未对你开放权限";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 删除亲情榜
     *
     * @param friendId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/removeAffection")
    public Object removeAffection(String friendId, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = relationService.removeAffection(friendId, userId);
        if (data.equals("200")) {
            code = "200";
            message = "移除成功";
        } else {
            code = "201";
            message = "移除失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除权限
     *
     * @param friendId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/removeFriend")
    public Object removeFriend(String friendId, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = relationService.removeFriend(friendId, userId);
        if (data.equals("200")) {
            code = "200";
            message = "移除成功";
            relationResponseService.createClose(userId, friendId, LocalDateTime.now(), 0, 0, 0L);
        } else {
            code = "201";
            message = "移除失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }


    /**
     * 向好友开放权限
     *
     * @param friendId
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/openFriend")
    public Object openFriend(String friendId, String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = relationService.openFriend(friendId, userId);
        if (data.equals("200")) {
            code = "200";
            message = "添加权限成功";
            relationResponseService.createOpen(userId, friendId, LocalDateTime.now(), 0, 0, 0L);
        } else {
            code = "201";
            message = "添加权限失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 我关心
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/ICare")
    public Object ICare(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<Relation> data = relationService.ICare(userId);
        if (data == null) {
            code = "201";
            message = "无信息";
        } else {
            code = "200";
            message = "查找成功";
            param.put("data", data);
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 我关心
     * @param token
     * @return
     */
//    @ResponseBody
//    @RequestMapping("/ICare2")
//    public Object ICare2(String token){
//        String userId;
//        String code;
//        String message;
//        Map param=new HashMap();
//        if (utilService.loginReturn(token)!=null){
//            userId = utilService.loginReturn(token);
//        }else {
//            code="203";
//            message="该token不存在";
//            param.put("code",code);
//            param.put("message",message);
//            return param;
//        }
//        List<Relation> data=relationService.ICare2(userId);
//        if (data==null){
//            code="201";
//            message="无信息";
//        }else {
//            code="200";
//            message="查找成功";
//            param.put("data",data);
//        }
//        param.put("code",code);
//        param.put("message",message);
//        return param;
//    }

}


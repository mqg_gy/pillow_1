package com.ocs.pillow_1.controller.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.RelationGroup;
import com.ocs.pillow_1.service.relation.ordinary.RelationGroupService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
@Controller
@RequestMapping("/relationGroup")
public class RelationGroupController {
    @Resource
    RelationGroupService relationGroupService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建用户分组
     *
     * @param token
     * @param groupName
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object create(String token, String groupName) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        String data = relationGroupService.create(userId, groupName);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 删除分组
     *
     * @param groupId
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String groupId) {
        String code;
        String message;
        Map param = new HashMap();
        String data = relationGroupService.delete(groupId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找用户的所有分组
     *
     * @param token
     * @return
     */
    @RequestMapping("/findByUserId")
    @ResponseBody
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "该token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<RelationGroup> data = relationGroupService.findByUserId(userId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 根据id查找分组信息
     *
     * @param groupId
     * @return
     */
    @RequestMapping("/findById")
    @ResponseBody
    public Object findById(String groupId) {
        String code;
        String message;
        Map param = new HashMap();
        RelationGroup data = relationGroupService.findById(groupId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

}


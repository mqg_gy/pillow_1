package com.ocs.pillow_1.controller.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.RelationRequest;
import com.ocs.pillow_1.service.relation.ordinary.RelationRequestService;
import com.ocs.pillow_1.service.relation.ordinary.RelationResponseService;
import com.ocs.pillow_1.service.util.UtilService;
import org.apache.http.protocol.ResponseServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 关系请求前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Controller
@RequestMapping("/relationRequest")
public class RelationRequestController {
    @Resource
    RelationRequestService relationRequestService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    RelationResponseService relationResponseService = null;

    /**
     * 创建请求
     *
     * @param friendId
     * @param token
     * @param message
     * @param type
     */
    @ResponseBody
    @RequestMapping("/save")
    public Object create(@RequestParam("friendId") String friendId, @RequestParam("token") String token, String message, @RequestParam("type") Integer type) {
        String userId;
        String code;
        String mes = null;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "token不存在";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }

        String data = relationRequestService.create(friendId, userId, message, type);
        code = data;
        switch (data) {
            case "200":
                mes = "创建请求成功";
                break;
            case "2100":
                code = "200";
                mes = "更新请求成功";
                break;
            case "201":
                mes = "创建请求失败";
                break;
            case "2101":
                mes = "更新请求失败";
                break;
            case "206":
                mes = "该用户已经是你的好友";
                break;
            case "207":
                mes = "你已经具有该用户报告的查看权限";
                break;
            case "208":
                mes = "你已经是该用户的亲情好友";
                break;
            case "209":
                mes = "今日请求次数已达上限";
                break;
//            case "210":
//                mes="你已向该用户解锁查看报告资料的权限";
//            break;
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找用户中的请求列表
     *
     * @param token
     * @return
     */
    @ResponseBody
    @RequestMapping("/findByUserId")
    public Object findByUserId(String token) {
        String userId;
        String code;
        String mes = null;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "token不存在";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        List<RelationRequest> data = relationRequestService.findByUserId(userId);
        if (data != null) {
            code = "200";
            mes = "成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "无信息";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 回应接口
     *
     * @param token
     * @param requestId
     * @param agreeState
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateAgree")
    public Object updateAgree(String token, String requestId, Integer agreeState) {
        String userId;
        String code;
        String mes = null;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "token不存在";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        String data = relationRequestService.updateAgree(requestId, agreeState);
        if (data == "200") {
            code = "200";
            mes = "成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "无信息";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 删除message中的信息
     *
     * @param requestId
     * @param index     ###4:nihaoa4###3:nihaoa3###2:nihaoa2###1:nihaoa1
     * @return
     */
    @ResponseBody
    @RequestMapping("/deleteMessage")
    public Object deleteMessage(String requestId, Integer index) {
        String code;
        String mes = null;
        Map param = new HashMap();
        String data = relationRequestService.deleteMessage(requestId, index + 1);
        if (data == "200") {
            code = "200";
            mes = "成功";
        } else {
            code = "201";
            mes = "失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }

    /**
     * 查找别人发给我的请求中的未读消息数
     *
     * @param token
     * @return
     */
    @RequestMapping("/findNotReadCount")
    @ResponseBody
    public Object findNotReadCount(String token) {
        String userId;
        String code;
        String mes = null;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            mes = "token不存在";
            param.put("code", code);
            param.put("message", mes);
            return param;
        }
        Long data1 = relationRequestService.findNotReadCount(userId);
        Long data2 = relationResponseService.findNotReadCount(userId);
        System.out.println(data1);
        System.out.println(data2);

        if (data1 != null || data2 != null) {
            Long data;
            if (data1 == null) {
                data = data2;
                System.out.println(data);
            }
            if (data2 == null) {
                data = data1;
            } else {
                data = data1 + data2;
            }
            code = "200";
            mes = "成功";
            param.put("data", data);
        } else {
            code = "201";
            mes = "失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }


    /**
     * 修改已读状态
     *
     * @param requestId
     */
    @RequestMapping("/updateReadState")
    @ResponseBody
    public Object updateReadState(String requestId) {
//        String userId;
        String code;
        String mes;
        Map param = new HashMap();
//        if (utilService.loginReturn(token)!=null){
//            userId = utilService.loginReturn(token);
//        }else {
//            code="203";
//            mes="该token不存在";
//            param.put("code",code);
//            param.put("message",mes);
//            return param;
//        }
        String data = relationRequestService.updateReadState(requestId);
        String data1 = relationResponseService.updateReadState(requestId);
        if (data == "200") {
            code = "200";
            mes = "成功";
        } else {
            code = "201";
            mes = "失败";
        }
        if (data1 == "200") {
            code = "200";
            mes = "成功";
        } else {
            code = "201";
            mes = "失败";
        }
        param.put("code", code);
        param.put("message", mes);
        return param;
    }
}


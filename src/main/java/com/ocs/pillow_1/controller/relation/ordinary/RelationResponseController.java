package com.ocs.pillow_1.controller.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.RelationResponse;
import com.ocs.pillow_1.service.relation.ordinary.RelationResponseService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author linj123
 * @since 2019-05-21
 */
@Controller
@RequestMapping("/relationResponse")
public class RelationResponseController {
    @Resource
    RelationResponseService relationResponseService = null;
    @Resource
    UtilService utilService = null;


    /**
     * 删除指定消息
     *
     * @param responseId
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String responseId) {
        String code;
        String message;
        Map param = new HashMap();
        String data = relationResponseService.delete(responseId);
        if (data == "200") {
            code = "200";
            message = "成功";
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }

    /**
     * 查找指定用户的所有返回信息
     *
     * @param token
     * @return
     */
    @RequestMapping("/findByUserId")
    @ResponseBody
    public Object findByUserId(String token) {
        String userId;
        String code;
        String message;
        Map param = new HashMap();
        if (utilService.loginReturn(token) != null) {
            userId = utilService.loginReturn(token);
        } else {
            code = "203";
            message = "token不存在";
            param.put("code", code);
            param.put("message", message);
            return param;
        }
        List<RelationResponse> data = relationResponseService.findByUserId(userId);
        if (data != null) {
            code = "200";
            message = "成功";
            param.put("data", data);
        } else {
            code = "201";
            message = "失败";
        }
        param.put("code", code);
        param.put("message", message);
        return param;
    }
}


package com.ocs.pillow_1.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class ReportData {

    @Id
    private ObjectId id;

    private String deviceId;

    private String date;

    private int hr;

    private int bhr;

    private int ahr;

    private int br;

    private int cmovCs;

    private int csnoreCs;

    private int wakeCs;

    private int outbedCs;

    private String onbedtime;

    private String outbedtime;

    private String fallinsleepTime;//第一次入睡时间

    private String reportName;

    private String relationId;

    private String did;

    private String createTime;

    private int totalItems;

    private int effectiveSleepTime;

    private String osaMessage;

    private int hu;

    private String huMessage;

    private int tp;

    private String tpMessage;

    private int ns;

    private String nsMessage;

    private int lt;

    private String ltMessage;

    //"hu":null,"tp":null,"ns":null,"lt":null

    private double remReference;
    private double lightSleepReference;
    private double deepSleepReference;
    private double durWakeReference;


    private int brains;//脑力

    private int energy;//体力

    private int immunity;//免疫力

    private int cardiovascular;//心血管

    private int sleepingAid;//助眠力

    private int breathingAid;//助呼吸

    private int durReportItem;//有效报告数量

    private int remSleepTime;

    private int wakeSleepTime;

    private int deepSleepTime;

    private int lightSleepTime;
    private String remSleepTimeStr;

    private String wakeSleepTimeStr;

    private String deepSleepTimeStr;

    private String lightSleepTimeStr;

    public int getRemSleepTime() {
        return remSleepTime;
    }

    public void setRemSleepTime(int remSleepTime) {
        this.remSleepTime = remSleepTime;
    }

    public int getWakeSleepTime() {
        return wakeSleepTime;
    }

    public void setWakeSleepTime(int wakeSleepTime) {
        this.wakeSleepTime = wakeSleepTime;
    }

    public int getDeepSleepTime() {
        return deepSleepTime;
    }

    public void setDeepSleepTime(int deepSleepTime) {
        this.deepSleepTime = deepSleepTime;
    }

    public int getLightSleepTime() {
        return lightSleepTime;
    }

    public void setLightSleepTime(int lightSleepTime) {
        this.lightSleepTime = lightSleepTime;
    }

    public int getDurReportItem() {
        return durReportItem;
    }

    public void setDurReportItem(int durReportItem) {
        this.durReportItem = durReportItem;
    }

    public int getBrains() {
        return brains;
    }

    public void setBrains(int brains) {
        this.brains = brains;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getImmunity() {
        return immunity;
    }

    public void setImmunity(int immunity) {
        this.immunity = immunity;
    }

    public int getCardiovascular() {
        return cardiovascular;
    }

    public void setCardiovascular(int cardiovascular) {
        this.cardiovascular = cardiovascular;
    }

    public int getSleepingAid() {
        return sleepingAid;
    }

    public void setSleepingAid(int sleepingAid) {
        this.sleepingAid = sleepingAid;
    }

    public int getBreathingAid() {
        return breathingAid;
    }

    public void setBreathingAid(int breathingAid) {
        this.breathingAid = breathingAid;
    }

    public double getRemReference() {
        return remReference;
    }

    public void setRemReference(double remReference) {
        this.remReference = remReference;
    }

    public double getLightSleepReference() {
        return lightSleepReference;
    }

    public void setLightSleepReference(double lightSleepReference) {
        this.lightSleepReference = lightSleepReference;
    }

    public double getDeepSleepReference() {
        return deepSleepReference;
    }

    public void setDeepSleepReference(double deepSleepReference) {
        this.deepSleepReference = deepSleepReference;
    }

    public double getDurWakeReference() {
        return durWakeReference;
    }

    public void setDurWakeReference(double durWakeReference) {
        this.durWakeReference = durWakeReference;
    }

    public int getBhr() {
        return bhr;
    }

    public void setBhr(int bhr) {
        this.bhr = bhr;
    }

    public int getAhr() {
        return ahr;
    }

    public void setAhr(int ahr) {
        this.ahr = ahr;
    }

    public String getOsaMessage() {
        return osaMessage;
    }

    public void setOsaMessage(String osaMessage) {
        this.osaMessage = osaMessage;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getEffectiveSleepTime() {
        return effectiveSleepTime;
    }

    public void setEffectiveSleepTime(int effectiveSleepTime) {
        this.effectiveSleepTime = effectiveSleepTime;
    }

    public int getCsnoreCs() {
        return csnoreCs;
    }

    public void setCsnoreCs(int csnoreCs) {
        this.csnoreCs = csnoreCs;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getHr() {
        return hr;
    }

    public void setHr(int hr) {
        this.hr = hr;
    }

    public int getBr() {
        return br;
    }

    public void setBr(int br) {
        this.br = br;
    }


    public String getOnbedtime() {
        return onbedtime;
    }

    public void setOnbedtime(String onbedtime) {
        this.onbedtime = onbedtime;
    }

    public String getOutbedtime() {
        return outbedtime;
    }

    public void setOutbedtime(String outbedtime) {
        this.outbedtime = outbedtime;
    }

    public int getCmovCs() {
        return cmovCs;
    }

    public void setCmovCs(int cmovCs) {
        this.cmovCs = cmovCs;
    }

    public int getWakeCs() {
        return wakeCs;
    }

    public void setWakeCs(int wakeCs) {
        this.wakeCs = wakeCs;
    }

    public int getOutbedCs() {
        return outbedCs;
    }

    public void setOutbedCs(int outbedCs) {
        this.outbedCs = outbedCs;
    }

    public String getFallinsleepTime() {
        return fallinsleepTime;
    }

    public void setFallinsleepTime(String fallinsleepTime) {
        this.fallinsleepTime = fallinsleepTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public int getHu() {
        return hu;
    }

    public void setHu(int hu) {
        this.hu = hu;
    }

    public String getHuMessage() {
        return huMessage;
    }

    public void setHuMessage(String huMessage) {
        this.huMessage = huMessage;
    }

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public String getTpMessage() {
        return tpMessage;
    }

    public void setTpMessage(String tpMessage) {
        this.tpMessage = tpMessage;
    }

    public int getNs() {
        return ns;
    }

    public void setNs(int ns) {
        this.ns = ns;
    }

    public String getNsMessage() {
        return nsMessage;
    }

    public void setNsMessage(String nsMessage) {
        this.nsMessage = nsMessage;
    }

    public int getLt() {
        return lt;
    }

    public void setLt(int lt) {
        this.lt = lt;
    }

    public String getLtMessage() {
        return ltMessage;
    }

    public void setLtMessage(String ltMessage) {
        this.ltMessage = ltMessage;
    }

    public String getRemSleepTimeStr() {
        return remSleepTimeStr;
    }

    public void setRemSleepTimeStr(String remSleepTimeStr) {
        this.remSleepTimeStr = remSleepTimeStr;
    }

    public String getWakeSleepTimeStr() {
        return wakeSleepTimeStr;
    }

    public void setWakeSleepTimeStr(String wakeSleepTimeStr) {
        this.wakeSleepTimeStr = wakeSleepTimeStr;
    }

    public String getDeepSleepTimeStr() {
        return deepSleepTimeStr;
    }

    public void setDeepSleepTimeStr(String deepSleepTimeStr) {
        this.deepSleepTimeStr = deepSleepTimeStr;
    }

    public String getLightSleepTimeStr() {
        return lightSleepTimeStr;
    }

    public void setLightSleepTimeStr(String lightSleepTimeStr) {
        this.lightSleepTimeStr = lightSleepTimeStr;
    }

    @Override
    public String toString() {
        return "ReportData{" +
                "id=" + id +
                ", deviceId='" + deviceId + '\'' +
                ", date='" + date + '\'' +
                ", hr=" + hr +
                ", br=" + br +
                ", cmovCs=" + cmovCs +
                ", wakeCs=" + wakeCs +
                ", outbedCs=" + outbedCs +
                ", onbedtime='" + onbedtime + '\'' +
                ", outbedtime='" + outbedtime + '\'' +
                ", fallinsleepTime='" + fallinsleepTime + '\'' +
                ", reportName='" + reportName + '\'' +
                ", relationId='" + relationId + '\'' +
                ", did='" + did + '\'' +
                ", createTime='" + createTime + '\'' +
                ", totalItems=" + totalItems +
                '}';
    }
}

package com.ocs.pillow_1.entity.user.system;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-06-04
 */
public class SysUser {

    private static final long serialVersionUID = 1L;


    private Long systemId;
    /**
     * 工号
     */
    private String userCode;
    /**
     * 姓名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 权限角色等级
     */
    private Long roleLevel;


    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRoleLevel() {
        return roleLevel;
    }

    public void setRoleLevel(Long roleLevel) {
        this.roleLevel = roleLevel;
    }


    @Override
    public String toString() {
        return "SysUser{" +
                "systemId=" + systemId +
                ", userCode=" + userCode +
                ", username=" + username +
                ", password=" + password +
                ", roleLevel=" + roleLevel +
                "}";
    }
}

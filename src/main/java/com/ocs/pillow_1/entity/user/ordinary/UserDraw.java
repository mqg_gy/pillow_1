package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
public class UserDraw {

    private static final long serialVersionUID = 1L;

    /**
     * 标志
     */
    private String drawId;
    /**
     * 用户Id
     */
    private String userId;
    /**
     * 中奖内容
     */
    private String drawMessage;
    /**
     * 中奖时间
     */
    private LocalDateTime time;


    public String getDrawId() {
        return drawId;
    }

    public void setDrawId(String drawId) {
        this.drawId = drawId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDrawMessage() {
        return drawMessage;
    }

    public void setDrawMessage(String drawMessage) {
        this.drawMessage = drawMessage;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "UserDraw{" +
                "drawId=" + drawId +
                ", userId=" + userId +
                ", drawMessage=" + drawMessage +
                ", time=" + time +
                "}";
    }
}

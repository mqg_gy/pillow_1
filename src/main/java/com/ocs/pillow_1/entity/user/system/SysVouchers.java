package com.ocs.pillow_1.entity.user.system;


import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public class SysVouchers {

    private static final long serialVersionUID = 1L;

    /**
     * 券ID
     */
    private String voucherId;
    /**
     * 面值
     */
    private String faceValue;
    /**
     * 适用范围。全品、牙刷类、牙膏类
     */
    private String range;
    /**
     * 适用商品。商品Id用*隔开
     */
    private String products;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;


    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    @Override
    public String toString() {
        return "SysVouchers{" +
                "voucherId=" + voucherId +
                ", faceValue=" + faceValue +
                ", range=" + range +
                ", products=" + products +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                "}";
    }
}

package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户实名认证实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public class UserAutonym {

    private static final long serialVersionUID = 1L;

    private String userId;
    /**
     * 身份证正面照
     */
    private String positivePhoto;
    /**
     * 身份证背面照
     */
    private String backPhoto;
    /**
     * 姓名
     */
    private String name;
    /**
     * 名族
     */
    private String national;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 地址
     */
    private String address;
    /**
     * 身份证号
     */
    private String idNumber;
    /**
     * 有效期
     */
    private Date deadline;
    /**
     * 签发机关
     */
    private String organ;
    /**
     * 验证状态。0：审核中；1：审核通过；
     */
    private String verifyState;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPositivePhoto() {
        return positivePhoto;
    }

    public void setPositivePhoto(String positivePhoto) {
        this.positivePhoto = positivePhoto;
    }

    public String getBackPhoto() {
        return backPhoto;
    }

    public void setBackPhoto(String backPhoto) {
        this.backPhoto = backPhoto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNational() {
        return national;
    }

    public void setNational(String national) {
        this.national = national;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getOrgan() {
        return organ;
    }

    public void setOrgan(String organ) {
        this.organ = organ;
    }

    public String getVerifyState() {
        return verifyState;
    }

    public void setVerifyState(String verifyState) {
        this.verifyState = verifyState;
    }


    @Override
    public String toString() {
        return "UserAutonym{" +
                "userId=" + userId +
                ", positivePhoto=" + positivePhoto +
                ", backPhoto=" + backPhoto +
                ", name=" + name +
                ", national=" + national +
                ", birthday=" + birthday +
                ", address=" + address +
                ", idNumber=" + idNumber +
                ", deadline=" + deadline +
                ", organ=" + organ +
                ", verifyState=" + verifyState +
                "}";
    }
}

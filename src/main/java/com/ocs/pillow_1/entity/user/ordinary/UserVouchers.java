package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public class UserVouchers {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 券Id
     */
    private String voucherId;
    /**
     * 用户Id
     */
    private String userId;
    /**
     * 状态。0：未使用；1：已使用；2：已过期
     */
    private String state;
    /**
     * 来源
     */
    private String source;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }


    @Override
    public String toString() {
        return "UserVouchers{" +
                "id=" + id +
                ", voucherId=" + voucherId +
                ", userId=" + userId +
                ", state=" + state +
                ", source=" + source +
                "}";
    }
}

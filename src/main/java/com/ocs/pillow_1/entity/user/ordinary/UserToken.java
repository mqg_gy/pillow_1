package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户token表
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public class UserToken {

    private static final long serialVersionUID = 1L;

    private String userId;
    private String username;
    private String token;
    private LocalDateTime lastUsedTime;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getLastUsedTime() {
        return lastUsedTime;
    }

    public void setLastUsedTime(LocalDateTime lastUsedTime) {
        this.lastUsedTime = lastUsedTime;
    }

    @Override
    public String toString() {
        return "UserToken{" +
                "userId=" + userId +
                ", username=" + username +
                ", token=" + token +
                ", lastUsedTime=" + lastUsedTime +
                "}";
    }
}

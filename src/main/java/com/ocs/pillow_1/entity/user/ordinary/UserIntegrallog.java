package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-28
 */
public class UserIntegrallog {

    private static final long serialVersionUID = 1L;

    /**
     * 记录Id
     */
    private String logId;
    /**
     * 用户Id
     */
    private String userId;
    /**
     * 增加或减少的积分数额
     */
    private Double amount;
    /**
     * 如果是订单，则存在；若是修改信息则不需要
     */
    private String orderId;
    /**
     * 存放积分的获取或支出信息
     */
    private String message;
    /**
     * 记录发生的时间
     */
    private LocalDateTime logTime;


    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getLogTime() {
        return logTime;
    }

    public void setLogTime(LocalDateTime logTime) {
        this.logTime = logTime;
    }

    @Override
    public String toString() {
        return "UserIntegrallog{" +
                "logId=" + logId +
                ", userId=" + userId +
                ", amount=" + amount +
                ", orderId=" + orderId +
                ", message=" + message +
                ", logTime=" + logTime +
                "}";
    }
}

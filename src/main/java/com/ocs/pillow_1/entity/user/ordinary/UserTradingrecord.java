package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户交易记录实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public class UserTradingrecord {

    private static final long serialVersionUID = 1L;

    private String recordId;
    /**
     * 交易时间
     */
    private LocalDateTime time;
    /**
     * 交易金额
     */
    private Double amount;
    /**
     * 支付方式
     */
    private String method;
    /**
     * 交易类型。1：先购后返；2：拼团；3：私人定制；4：分销；5：积分；6：充值；7：体现
     */
    private String type;
    private String userId;
    /**
     * 订单号。如果是订单就存在
     */
    private String orderId;


    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    @Override
    public String toString() {
        return "UserTradingrecord{" +
                "recordId=" + recordId +
                ", time=" + time +
                ", amount=" + amount +
                ", method=" + method +
                ", type=" + type +
                ", userId=" + userId +
                ", orderId=" + orderId +
                "}";
    }
}

package com.ocs.pillow_1.entity.user;

import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;

public class UserWapper {
    private String userId;
    private User user;
    private UserDetail userDetail;

    public User getUser() {
        return user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }
}

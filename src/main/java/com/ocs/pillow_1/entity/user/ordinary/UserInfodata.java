package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-06-11
 */
public class UserInfodata {

    private static final long serialVersionUID = 1L;

    /**
     * 微信的openId
     */
    private String openId;
    /**
     * 微信昵称
     */
    private String nickname;
    /**
     * 微信头像
     */
    private String userImg;
    /**
     * 微信的性别
     */
    private String sex;
    /**
     * 表示同一个企业的公众号
     */
    private String unionId;


    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }


    @Override
    public String toString() {
        return "UserInfodataMapper{" +
                "openId=" + openId +
                ", nickname=" + nickname +
                ", userImg=" + userImg +
                ", sex=" + sex +
                ", unionId=" + unionId +
                "}";
    }
}

package com.ocs.pillow_1.entity.user.ordinary;

import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户详情实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public class UserDetail {

    private static final long serialVersionUID = 1L;

    private String userId;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 头像
     */
    private String headIcon;
    /**
     * 性别，默认是男
     */
    private String sex;
    /**
     * 电话号码。11位
     */
    private String phone;
    /**
     * 手机的系统类型。1：安卓；2：iOS；3：两者都有
     */
    private Integer systemType;
    /**
     * 是否实名认证,0：不是；1：正在审核；2：是
     */
    private Integer isAutonym;
    /**
     * 是否为医生，默认不是。0：不是；1：正在审核；2：是
     */
    private Integer isDoctor;
    /**
     * 分销的上级。
     */
    private String parentId;
    /**
     * 行业
     */
    private String industry;
    /**
     * 单位
     */
    private String unit;
    /**
     * 职务
     */
    private String position;
    /**
     * 住址
     */
    private String address;

    /**
     * 个人签名
     */
    private String signature;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 体重
     */
    private Double weight;
    /**
     * 平时起床时间
     */
    private String wakeupTime;
    /**
     * 理想起床时间
     */
    private String idealWakeupTime;
    /**
     * 下载途径
     */
    private String loadWay;
    /**
     * 微信的open的Id
     */
    private String openId;


    /**
     * 昵称
     * 不存入数据库
     */
    private String nickname;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(String headIcon) {
        this.headIcon = headIcon;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public Integer getIsAutonym() {
        return isAutonym;
    }

    public void setIsAutonym(Integer isAutonym) {
        this.isAutonym = isAutonym;
    }

    public Integer getIsDoctor() {
        return isDoctor;
    }

    public void setIsDoctor(Integer isDoctor) {
        this.isDoctor = isDoctor;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getWakeupTime() {
        return wakeupTime;
    }

    public void setWakeupTime(String wakeupTime) {
        this.wakeupTime = wakeupTime;
    }

    public String getIdealWakeupTime() {
        return idealWakeupTime;
    }

    public void setIdealWakeupTime(String idealWakeupTime) {
        this.idealWakeupTime = idealWakeupTime;
    }

    public String getLoadWay() {
        return loadWay;
    }

    public void setLoadWay(String loadWay) {
        this.loadWay = loadWay;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "userId=" + userId +
                ", birthday=" + birthday +
                ", headIcon=" + headIcon +
                ", sex=" + sex +
                ", phone=" + phone +
                ", systemType=" + systemType +
                ", isAutonym=" + isAutonym +
                ", isDoctor=" + isDoctor +
                ", parentId=" + parentId +
                ", industry=" + industry +
                ", unit=" + unit +
                ", position=" + position +
                ", address=" + address +
                "}";
    }
}

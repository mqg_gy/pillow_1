package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户银行卡实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public class UserCard {

    private static final long serialVersionUID = 1L;

    private String cardId;
    /**
     * 银行卡类型。某某银行，借记卡还是什么卡
     */
    private String type;
    /**
     * 卡号
     */
    private String number;
    /**
     * 持卡人姓名
     */
    private String name;
    /**
     * 持卡人手机号
     */
    private String phone;
    /**
     * 关联时间
     */
    private LocalDateTime relevanTime;
    private String userId;


    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getRelevanTime() {
        return relevanTime;
    }

    public void setRelevanTime(LocalDateTime relevanTime) {
        this.relevanTime = relevanTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserCard{" +
                "cardId=" + cardId +
                ", type=" + type +
                ", number=" + number +
                ", name=" + name +
                ", phone=" + phone +
                ", relevanTime=" + relevanTime +
                ", userId=" + userId +
                "}";
    }
}

package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 购物车实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class UserCar {

    private static final long serialVersionUID = 1L;

    private String shopId;
    private String userId;
    private String productId;
    /**
     * 商品类型。1：先购后返；2：拼团；3：私人定制；4：分销；5：积分
     */
    private Integer type;
    /**
     * 商品数量
     */
    private Integer count;
    /**
     * 商品规格值
     */
    private String specValues;
    /**
     * 加购时间
     */
    private LocalDateTime addTime;


    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getSpecValues() {
        return specValues;
    }

    public void setSpecValues(String specValues) {
        this.specValues = specValues;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "UserCar{" +
                "shopId=" + shopId +
                ", userId=" + userId +
                ", productId=" + productId +
                ", type=" + type +
                ", count=" + count +
                ", specValues=" + specValues +
                ", addTime=" + addTime +
                "}";
    }
}

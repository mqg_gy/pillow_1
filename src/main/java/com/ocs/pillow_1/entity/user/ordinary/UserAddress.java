package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;

/**
 * <p>
 * 用户购物车地址实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public class UserAddress {

    private static final long serialVersionUID = 1L;

    private String addressId;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 街道
     */
    private String street;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 收货人
     */
    private String consignee;
    /**
     * 收货电话
     */
    private String consigneePhone;
    /**
     * 是否默认
     */
    private Integer isDefault;
    private String userId;
    /**
     * 地址类型。0：普通地址；1：家；2：公司；3：学校
     */
    private String type;


    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneePhone() {
        return consigneePhone;
    }

    public void setConsigneePhone(String consigneePhone) {
        this.consigneePhone = consigneePhone;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserAddress{" +
                "addressId=" + addressId +
                ", province=" + province +
                ", city=" + city +
                ", area=" + area +
                ", street=" + street +
                ", address=" + address +
                ", consignee=" + consignee +
                ", consigneePhone=" + consigneePhone +
                ", isDefault=" + isDefault +
                ", userId=" + userId +
                ", type=" + type +
                "}";
    }
}

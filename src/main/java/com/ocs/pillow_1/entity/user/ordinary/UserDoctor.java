package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
public class UserDoctor {

    private static final long serialVersionUID = 1L;

    private String userId;
    /**
     * 验证状态。0：审核中；1：审核通过；
     */
    private String verifyState;
    /**
     * 执照正面照
     */
    private String licPositivePhoto;
    /**
     * 执照背面照
     */
    private String licBackPhoto;
    /**
     * 工作照
     */
    private String workPhoto;
    /**
     * 简介
     */
    private String introduce;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVerifyState() {
        return verifyState;
    }

    public void setVerifyState(String verifyState) {
        this.verifyState = verifyState;
    }

    public String getLicPositivePhoto() {
        return licPositivePhoto;
    }

    public void setLicPositivePhoto(String licPositivePhoto) {
        this.licPositivePhoto = licPositivePhoto;
    }

    public String getLicBackPhoto() {
        return licBackPhoto;
    }

    public void setLicBackPhoto(String licBackPhoto) {
        this.licBackPhoto = licBackPhoto;
    }

    public String getWorkPhoto() {
        return workPhoto;
    }

    public void setWorkPhoto(String workPhoto) {
        this.workPhoto = workPhoto;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }


    @Override
    public String toString() {
        return "UserDoctor{" +
                "userId=" + userId +
                ", verifyState=" + verifyState +
                ", licPositivePhoto=" + licPositivePhoto +
                ", licBackPhoto=" + licBackPhoto +
                ", workPhoto=" + workPhoto +
                ", introduce=" + introduce +
                "}";
    }
}

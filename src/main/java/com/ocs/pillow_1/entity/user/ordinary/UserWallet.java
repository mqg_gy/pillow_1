package com.ocs.pillow_1.entity.user.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 普通用户钱包实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
public class UserWallet {

    private static final long serialVersionUID = 1L;

    private String userId;
    /**
     * 最新更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 余额
     */
    private Double balance;
    /**
     * 积分
     */
    private Double integral;

    /**
     * 支付密码。规定只能是6位
     */
    private String payNumber;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public String getPayNumber() {
        return payNumber;
    }

    public void setPayNumber(String payNumber) {
        this.payNumber = payNumber;
    }

    @Override
    public String toString() {
        return "UserWallet{" +
                "userId=" + userId +
                ", updateTime=" + updateTime +
                ", balance=" + balance +
                ", integral=" + integral +
                "}";
    }
}

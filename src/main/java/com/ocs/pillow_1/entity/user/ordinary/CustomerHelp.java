package com.ocs.pillow_1.entity.user.ordinary;

/**
 * 客服实体类
 */
public class CustomerHelp {

    private String customerHelpId;

    //客服名称
    private String customerHelpName;

    public String getCustomerHelpId() {
        return customerHelpId;
    }

    public void setCustomerHelpId(String customerHelpId) {
        this.customerHelpId = customerHelpId;
    }

    public String getCustomerHelpName() {
        return customerHelpName;
    }

    public void setCustomerHelpName(String customerHelpName) {
        this.customerHelpName = customerHelpName;
    }

    @Override
    public String toString() {
        return "CustomerHelp{" +
                "customerHelpId='" + customerHelpId + '\'' +
                ", customerHelpName='" + customerHelpName + '\'' +
                '}';
    }
}

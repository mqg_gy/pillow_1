package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;

/**
 * <p>
 * 商品图实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class ProductImages {

    private static final long serialVersionUID = 1L;

    private String productId;
    /**
     * 主图
     */
    private String master;
    /**
     * 大图
     */
    private String large;
    /**
     * 细节图1
     */
    private String detail1;
    /**
     * 细节图2
     */
    private String detail2;
    /**
     * 外观图
     */
    private String box;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getDetail1() {
        return detail1;
    }

    public void setDetail1(String detail1) {
        this.detail1 = detail1;
    }

    public String getDetail2() {
        return detail2;
    }

    public void setDetail2(String detail2) {
        this.detail2 = detail2;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }


    @Override
    public String toString() {
        return "ProductImages{" +
                "productId=" + productId +
                ", master=" + master +
                ", large=" + large +
                ", detail1=" + detail1 +
                ", detail2=" + detail2 +
                ", box=" + box +
                "}";
    }
}

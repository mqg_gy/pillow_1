package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 规格项实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class ProductSpecitems {

    private static final long serialVersionUID = 1L;

    private long specId;
    private String specName;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;


    public long getSpecId() {
        return specId;
    }

    public void setSpecId(long specId) {
        this.specId = specId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ProductSpecitems{" +
                "specId=" + specId +
                ", specName=" + specName +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}

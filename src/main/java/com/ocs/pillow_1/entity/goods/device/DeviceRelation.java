package com.ocs.pillow_1.entity.goods.device;


import com.ocs.pillow_1.entity.user.ordinary.User;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 设备用户关系表
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class DeviceRelation {

    private static final long serialVersionUID = 1L;

    private String relationId;
    private String deviceId;
    private String userId;
    private User user;
    /**
     * 绑定即激活时间
     */
    private LocalDateTime bingingTime;
    /**
     * 状态，0：绑定状态；1：解绑状态
     */
    private Integer state;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getBingingTime() {
        return bingingTime;
    }

    public void setBingingTime(LocalDateTime bingingTime) {
        this.bingingTime = bingingTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "DeviceUserRelation{" +
                "relationId=" + relationId +
                ", deviceId=" + deviceId +
                ", userId=" + userId +
                ", bingingTime=" + bingingTime +
                "}";
    }
}

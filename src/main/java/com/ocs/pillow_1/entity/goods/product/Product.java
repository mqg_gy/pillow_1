package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class Product {

    private static final long serialVersionUID = 1L;

    private String productId;
    /**
     * 商品名
     */
    private String name;
    /**
     * 原价
     */
    private Double originalPrice;
    /**
     * 价格
     */
    private Double price;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 商品规格项
     */
    private String specItems;
    /**
     * 是否上架
     */
    private Integer isShelves;
    /**
     * 商品描述
     */
    private String describes;
    /**
     * 是否为先购后返商品.0:不是  1：是
     */
    private Integer isBack;
    /**
     * 是否为私人定制商品.0:不是  1：是
     */
    private Integer isCustom;
    /**
     * 是否为分销商品.0:不是  1：是
     */
    private Integer isDistribute;
    /**
     * 是否为积分商品.0:不是  1：是
     */
    private Integer isIntegral;
    /**
     * 是否为拼团商品.0:不是  1：是
     */
    private Integer isSpell;
    /**
     * 拼团底价
     */
    private Double floorPrice;
    /**
     * 拼团底价
     */
    private Double spellAmount;

    /**
     * 用于显示的商品规格项
     *
     * @return
     */
    private List<String> itemList = new ArrayList<>();


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getSpecItems() {
        return specItems;
    }

    public void setSpecItems(String specItems) {
        this.specItems = specItems;
    }

    public Integer getIsShelves() {
        return isShelves;
    }

    public void setIsShelves(Integer isShelves) {
        this.isShelves = isShelves;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public Integer getIsBack() {
        return isBack;
    }

    public void setIsBack(Integer isBack) {
        this.isBack = isBack;
    }

    public Integer getIsCustom() {
        return isCustom;
    }

    public void setIsCustom(Integer isCustom) {
        this.isCustom = isCustom;
    }

    public Integer getIsDistribute() {
        return isDistribute;
    }

    public void setIsDistribute(Integer isDistribute) {
        this.isDistribute = isDistribute;
    }

    public Integer getIsIntegral() {
        return isIntegral;
    }

    public void setIsIntegral(Integer isIntegral) {
        this.isIntegral = isIntegral;
    }

    public Integer getIsSpell() {
        return isSpell;
    }

    public void setIsSpell(Integer isSpell) {
        this.isSpell = isSpell;
    }

    public Double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(Double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public Double getSpellAmount() {
        return spellAmount;
    }

    public void setSpellAmount(Double spellAmount) {
        this.spellAmount = spellAmount;
    }

    public List<String> getItemList() {
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", name=" + name +
                ", price=" + price +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", specItems=" + specItems +
                ", isShelves=" + isShelves +
                ", describes=" + describes +
                ", isBack=" + isBack +
                ", isCustom=" + isCustom +
                ", isDistribute=" + isDistribute +
                ", isIntegral=" + isIntegral +
                ", isSpell=" + isSpell +
                ", floorPrice=" + floorPrice +
                "}";
    }
}

package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
public class ProductShelvetime {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    private String timeId;
    /**
     * 商品Id
     */
    private String productId;
    /**
     * 上架时间
     */
    private LocalDateTime upTime;
    /**
     * 下架时间
     */
    private LocalDateTime downTime;
    /**
     * 上架操作员的Id
     */
    private String upUserId;
    /**
     * 下架操作员的Id
     */
    private String downUserId;
    /**
     * 状态。0：在架上；1：已经下架
     */
    private Integer state;


    public String getTimeId() {
        return timeId;
    }

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public LocalDateTime getUpTime() {
        return upTime;
    }

    public void setUpTime(LocalDateTime upTime) {
        this.upTime = upTime;
    }

    public LocalDateTime getDownTime() {
        return downTime;
    }

    public void setDownTime(LocalDateTime downTime) {
        this.downTime = downTime;
    }

    public String getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(String upUserId) {
        this.upUserId = upUserId;
    }

    public String getDownUserId() {
        return downUserId;
    }

    public void setDownUserId(String downUserId) {
        this.downUserId = downUserId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    @Override
    public String toString() {
        return "ProductShelvetime{" +
                "timeId=" + timeId +
                ", productId=" + productId +
                ", upTime=" + upTime +
                ", downTime=" + downTime +
                ", state=" + state +
                "}";
    }
}

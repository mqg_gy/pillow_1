package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 规格值实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class ProductSpecvalues {

    private static final long serialVersionUID = 1L;

    private long specValueId;
    /**
     * 规格项目值的名称
     */
    private String valueName;
    /**
     * 所属的规格项目
     */
    private long specId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    public long getSpecValueId() {
        return specValueId;
    }

    public void setSpecValueId(long specValueId) {
        this.specValueId = specValueId;
    }

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    public long getSpecId() {
        return specId;
    }

    public void setSpecId(long specId) {
        this.specId = specId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ProductSpecvalues{" +
                "specValueId=" + specValueId +
                ", valueName=" + valueName +
                ", specId=" + specId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}

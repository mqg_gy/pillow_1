package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
public class ProductThumb {

    private static final long serialVersionUID = 1L;

    private String productThumbId;
    /**
     * 商品Id
     */
    private String productId;
    /**
     * 点赞时间
     */
    private LocalDateTime thumbTime;
    /**
     * 点赞用户
     */
    private String userId;


    public String getProductThumbId() {
        return productThumbId;
    }

    public void setProductThumbId(String productThumbId) {
        this.productThumbId = productThumbId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public LocalDateTime getThumbTime() {
        return thumbTime;
    }

    public void setThumbTime(LocalDateTime thumbTime) {
        this.thumbTime = thumbTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "ProductThumb{" +
                "productThumbId=" + productThumbId +
                ", productId=" + productId +
                ", thumbTime=" + thumbTime +
                ", userId=" + userId +
                "}";
    }
}

package com.ocs.pillow_1.entity.goods.product;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 库存实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class ProductSku {

    private static final long serialVersionUID = 1L;

    private String skuId;
    /**
     * 库存量
     */
    private Long inventory;
    /**
     * 成本价
     */
    private Double cost;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    private String productId;
    /**
     * 规格值。item：value样式
     */
    private String speValues;

    /**
     * 用于前端显示规格值
     *
     * @return
     */
    private List<String> valueList = new ArrayList<>();


    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Long getInventory() {
        return inventory;
    }

    public void setInventory(Long inventory) {
        this.inventory = inventory;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSpeValues() {
        return speValues;
    }

    public void setSpeValues(String speValues) {
        this.speValues = speValues;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }

    @Override
    public String toString() {
        return "ProductSku{" +
                "skuId=" + skuId +
                ", inventory=" + inventory +
                ", cost=" + cost +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", productId=" + productId +
                ", speValues=" + speValues +
                "}";
    }
}

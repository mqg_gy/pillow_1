package com.ocs.pillow_1.entity.goods.device;


import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 设备实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class Device {

    private static final long serialVersionUID = 1L;

    /**
     * 设备Id
     */
    private String deviceId;
    /**
     * 设备串号
     */
    private String sn;
    /**
     * 设备卡号
     */
    private String sim;
    /**
     * 所属的库存
     */
    private String skuId;
    private String productId;
    /**
     * 出厂时间
     */
    private String deliveryTime;
    /**
     * 是否已卖出。0：未卖出；1：已卖出；-1：已损坏
     */
    private Integer isSaled;
    /**
     * 是否激活(绑定就是激活)。0：未激活；1：已经激活。
     */
    private Integer isActivation;
    /**
     * 设备的二维码
     */
    private String qrcode;
    /**
     * 设备的来源，不同长度的串号来自不同的企业
     */
    private String source;
    /**
     * 设备类型（小月、慕思等）1：小月
     */
    private Long type;
    /**
     * 型号
     */
    private String model;
    /**
     * 有限保修
     */
    private String insurance;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getIsSaled() {
        return isSaled;
    }

    public void setIsSaled(Integer isSaled) {
        this.isSaled = isSaled;
    }

    public Integer getIsActivation() {
        return isActivation;
    }

    public void setIsActivation(Integer isActivation) {
        this.isActivation = isActivation;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceId=" + deviceId +
                ", sn=" + sn +
                ", skuId=" + skuId +
                ", deliveryTime=" + deliveryTime +
                ", isSaled=" + isSaled +
                ", isActivation=" + isActivation +
                "}";
    }
}

package com.ocs.pillow_1.entity.order.distribute;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 分销订单基本实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderDistributebase {

    private static final long serialVersionUID = 1L;

    private String productId;
    /**
     * 一级分销比例
     */
    private Double oneRatio;
    /**
     * 二级分销比例
     */
    private Double twoRatio;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getOneRatio() {
        return oneRatio;
    }

    public void setOneRatio(Double oneRatio) {
        this.oneRatio = oneRatio;
    }

    public Double getTwoRatio() {
        return twoRatio;
    }

    public void setTwoRatio(Double twoRatio) {
        this.twoRatio = twoRatio;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "OrderDistributebase{" +
                "productId=" + productId +
                ", oneRatio=" + oneRatio +
                ", twoRatio=" + twoRatio +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}

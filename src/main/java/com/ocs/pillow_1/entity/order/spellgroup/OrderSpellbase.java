package com.ocs.pillow_1.entity.order.spellgroup;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 已经存在的团表实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderSpellbase {

    private static final long serialVersionUID = 1L;

    private String groupId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 实时的拼团价格
     */
    private Double realTimePrice;
    /**
     * 成交的价格
     */
    private Double dealPrice;
    private String productId;
    /**
     * 目前的拼团人数
     */
    private Long count;
    /**
     * 团的状态。0：正在组团；-1：已经结束
     */
    private Integer state;


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Double getRealTimePrice() {
        return realTimePrice;
    }

    public void setRealTimePrice(Double realTimePrice) {
        this.realTimePrice = realTimePrice;
    }

    public Double getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(Double dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    @Override
    public String toString() {
        return "OrderSpellbase{" +
                "groupId=" + groupId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", realTimePrice=" + realTimePrice +
                ", dealPrice=" + dealPrice +
                ", productId=" + productId +
                ", count=" + count +
                ", state=" + state +
                "}";
    }
}

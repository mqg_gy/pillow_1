package com.ocs.pillow_1.entity.order.spellgroup;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 拼团订单实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderSpell {

    private static final long serialVersionUID = 1L;

    /**
     * 拼团的订单号
     */
    private String orderId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;
    /**
     * 完成时间
     */
    private LocalDateTime completeTime;
    /**
     * 支付价格
     */
    private Double payMoney;
    /**
     * 商品数量
     */
    private Long count;
    /**
     * 所属的团
     */
    private String groupId;
    /**
     * 根据规格值修改库存
     */
    private String skuId;
    /**
     * 下单用户
     */
    private String userId;
    /**
     * 订单状态。1：已付款；2：已发货；3：退货中；4：已退货；5：退款中；6：已退款；7：已收货
     */
    private Integer state;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDateTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(LocalDateTime completeTime) {
        this.completeTime = completeTime;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    @Override
    public String toString() {
        return "OrderSpell{" +
                "orderId=" + orderId +
                ", createTime=" + createTime +
                ", deliveryTime=" + deliveryTime +
                ", completeTime=" + completeTime +
                ", payMoney=" + payMoney +
                ", count=" + count +
                ", groupId=" + groupId +
                ", skuId=" + skuId +
                ", userId=" + userId +
                ", state=" + state +
                "}";
    }
}

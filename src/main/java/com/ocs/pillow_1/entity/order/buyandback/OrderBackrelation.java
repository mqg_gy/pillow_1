package com.ocs.pillow_1.entity.order.buyandback;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 返还用户关系表
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public class OrderBackrelation {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String backOrderId;
    /**
     * 下属的用户Id
     */
    private String subUserId;
    /**
     * 加入的时间
     */
    private LocalDateTime addTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBackOrderId() {
        return backOrderId;
    }

    public void setBackOrderId(String backOrderId) {
        this.backOrderId = backOrderId;
    }

    public String getSubUserId() {
        return subUserId;
    }

    public void setSubUserId(String subUserId) {
        this.subUserId = subUserId;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "OrderBackrelation{" +
                "id=" + id +
                ", backOrderId=" + backOrderId +
                ", subUserId=" + subUserId +
                ", addTime=" + addTime +
                "}";
    }
}

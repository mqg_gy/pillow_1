package com.ocs.pillow_1.entity.order.exchangebyintegral;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 积分订单实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderIntegral {

    private static final long serialVersionUID = 1L;

    private String integralOrderId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;
    /**
     * 完成时间
     */
    private LocalDateTime completeTime;
    /**
     * 物流的订单号
     */
    private String orderId;
    /**
     * 支付的价格，可能用券
     */
    private Double payMoney;
    /**
     * 真实的价格
     */
    private Double price;
    /**
     * 商品数量
     */
    private Long count;
    private String productId;
    private String userId;
    private String skuId;
    /**
     * 订单状态。1：已付款；2：已发货；3：退货中；4：已退货；5：退款中；6：已退款；7：已收货
     */
    private Integer state;


    public String getIntegralOrderId() {
        return integralOrderId;
    }

    public void setIntegralOrderId(String integralOrderId) {
        this.integralOrderId = integralOrderId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDateTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(LocalDateTime completeTime) {
        this.completeTime = completeTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(Double payMoney) {
        this.payMoney = payMoney;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    @Override
    public String toString() {
        return "OrderIntegral{" +
                "integralOrderId=" + integralOrderId +
                ", createTime=" + createTime +
                ", deliveryTime=" + deliveryTime +
                ", completeTime=" + completeTime +
                ", orderId=" + orderId +
                ", payMoney=" + payMoney +
                ", price=" + price +
                ", count=" + count +
                ", productId=" + productId +
                ", userId=" + userId +
                ", skuId=" + skuId +
                ", state=" + state +
                "}";
    }
}

package com.ocs.pillow_1.entity.order.privatecustom;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 私人定制商品列表实体（提前写下的）
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderCustombase {

    private static final long serialVersionUID = 1L;

    private String customId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 差价，不同的商品、不同的类目有不同的差价
     */
    private Double distancePrice;
    /**
     * 定制类目。比如刻字、组装颜色等定制
     */
    private String noteItem;
    private String productId;


    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Double getDistancePrice() {
        return distancePrice;
    }

    public void setDistancePrice(Double distancePrice) {
        this.distancePrice = distancePrice;
    }

    public String getNoteItem() {
        return noteItem;
    }

    public void setNoteItem(String noteItem) {
        this.noteItem = noteItem;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


    @Override
    public String toString() {
        return "OrderCustombase{" +
                "customId=" + customId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", distancePrice=" + distancePrice +
                ", noteItem=" + noteItem +
                ", productId=" + productId +
                "}";
    }
}

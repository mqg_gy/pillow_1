package com.ocs.pillow_1.entity.order.buyandback;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 返回详情表
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public class OrderBackdetail {

    private static final long serialVersionUID = 1L;

    private String backOrderId;
    /**
     * 开始返还的时间
     */
    private Date startTime;
    /**
     * 已经返回的天数
     */
    private Long reternDays;
    /**
     * 还要持续的天数
     */
    private Long remainDays;
    /**
     * 已返还的金额
     */
    private Double returnMoney;
    /**
     * 基本返本率，元/天
     */
    private Double baseRatio;
    /**
     * 当前的返本率，元/天
     */
    private Double presentRatio;
    /**
     * 一次积存的分享次数
     */
    private Long onrShareCount;


    public String getBackOrderId() {
        return backOrderId;
    }

    public void setBackOrderId(String backOrderId) {
        this.backOrderId = backOrderId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getReternDays() {
        return reternDays;
    }

    public void setReternDays(Long reternDays) {
        this.reternDays = reternDays;
    }

    public Long getRemainDays() {
        return remainDays;
    }

    public void setRemainDays(Long remainDays) {
        this.remainDays = remainDays;
    }

    public Double getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Double returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Double getBaseRatio() {
        return baseRatio;
    }

    public void setBaseRatio(Double baseRatio) {
        this.baseRatio = baseRatio;
    }

    public Double getPresentRatio() {
        return presentRatio;
    }

    public void setPresentRatio(Double presentRatio) {
        this.presentRatio = presentRatio;
    }

    public Long getOnrShareCount() {
        return onrShareCount;
    }

    public void setOnrShareCount(Long onrShareCount) {
        this.onrShareCount = onrShareCount;
    }

    @Override
    public String toString() {
        return "OrderBackdetail{" +
                "backOrderId=" + backOrderId +
                ", startTime=" + startTime +
                ", reternDays=" + reternDays +
                ", remainDays=" + remainDays +
                ", returnMoney=" + returnMoney +
                ", baseRatio=" + baseRatio +
                ", presentRatio=" + presentRatio +
                ", onrShareCount=" + onrShareCount +
                "}";
    }
}

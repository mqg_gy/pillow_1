package com.ocs.pillow_1.entity.order.buyandback;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 先购后返订单表
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
public class OrderBack {

    private static final long serialVersionUID = 1L;

    private String backOrderId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 发货时间
     */
    private LocalDateTime deliveryTime;
    /**
     * 完成时间
     */
    private LocalDateTime completeTime;
    private String orderId;
    /**
     * 支付价格，不能用券
     */
    private Double paymoney;
    /**
     * 真实价格
     */
    private Double price;
    /**
     * 商品数量
     */
    private Long count;
    private String productId;
    private String userId;
    private String skuId;
    /**
     * 订单状态。1：已付款；2：已发货；3：退货中；4：已退货；5：退款中；6：已退款；7：已收货
     */
    private Integer state;
    /**
     * 先购后返的上级id
     */
    private String parentId;


    public String getBackOrderId() {
        return backOrderId;
    }

    public void setBackOrderId(String backOrderId) {
        this.backOrderId = backOrderId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDateTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public LocalDateTime getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(LocalDateTime completeTime) {
        this.completeTime = completeTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getPaymoney() {
        return paymoney;
    }

    public void setPaymoney(Double paymoney) {
        this.paymoney = paymoney;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


    @Override
    public String toString() {
        return "OrderBack{" +
                "backOrderId=" + backOrderId +
                ", createTime=" + createTime +
                ", deliveryTime=" + deliveryTime +
                ", completeTime=" + completeTime +
                ", orderId=" + orderId +
                ", paymoney=" + paymoney +
                ", price=" + price +
                ", count=" + count +
                ", productId=" + productId +
                ", userId=" + userId +
                ", skuId=" + skuId +
                ", state=" + state +
                ", parentId=" + parentId +
                "}";
    }
}

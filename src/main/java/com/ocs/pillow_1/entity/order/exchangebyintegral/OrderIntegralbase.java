package com.ocs.pillow_1.entity.order.exchangebyintegral;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 积分订单基本表实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
public class OrderIntegralbase {

    private static final long serialVersionUID = 1L;

    private String productId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 额外的差价
     */
    private Double distance;
    /**
     * 所需的积分
     */
    private Double needIntegral;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getNeedIntegral() {
        return needIntegral;
    }

    public void setNeedIntegral(Double needIntegral) {
        this.needIntegral = needIntegral;
    }


    @Override
    public String toString() {
        return "OrderIntegralbase{" +
                "productId=" + productId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", distance=" + distance +
                ", needIntegral=" + needIntegral +
                "}";
    }
}

package com.ocs.pillow_1.entity.article.article;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章评论点赞实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleCommentthumb {

    private static final long serialVersionUID = 1L;

    private String commentThumbId;
    private String commentId;
    private LocalDateTime thumbTime;
    private String userId;


    public String getCommentThumbId() {
        return commentThumbId;
    }

    public void setCommentThumbId(String commentThumbId) {
        this.commentThumbId = commentThumbId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public LocalDateTime getThumbTime() {
        return thumbTime;
    }

    public void setThumbTime(LocalDateTime thumbTime) {
        this.thumbTime = thumbTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "ArticleCommentthumb{" +
                "commentThumbId=" + commentThumbId +
                ", commentId=" + commentId +
                ", thumbTime=" + thumbTime +
                ", userId=" + userId +
                "}";
    }
}

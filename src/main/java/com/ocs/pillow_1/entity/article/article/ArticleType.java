package com.ocs.pillow_1.entity.article.article;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章类型实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleType {

    private static final long serialVersionUID = 1L;

    private String articleTypeId;
    private Integer code;
    private LocalDateTime createTime;
    private String typeName;


    public String getArticleTypeId() {
        return articleTypeId;
    }

    public void setArticleTypeId(String articleTypeId) {
        this.articleTypeId = articleTypeId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    @Override
    public String toString() {
        return "ArticleType{" +
                "articleTypeId=" + articleTypeId +
                ", code=" + code +
                ", createTime=" + createTime +
                ", typeName=" + typeName +
                "}";
    }
}

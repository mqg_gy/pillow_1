package com.ocs.pillow_1.entity.article.article;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章阅读记录实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleReader {

    private static final long serialVersionUID = 1L;

    private String articleReaderId;
    private String articleId;
    private LocalDateTime readTime;
    private String userId;


    public String getArticleReaderId() {
        return articleReaderId;
    }

    public void setArticleReaderId(String articleReaderId) {
        this.articleReaderId = articleReaderId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public LocalDateTime getReadTime() {
        return readTime;
    }

    public void setReadTime(LocalDateTime readTime) {
        this.readTime = readTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ArticleReader{" +
                "articleReaderId=" + articleReaderId +
                ", articleId=" + articleId +
                ", readTime=" + readTime +
                ", userId=" + userId +
                "}";
    }
}

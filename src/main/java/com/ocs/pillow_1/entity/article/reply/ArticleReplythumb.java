package com.ocs.pillow_1.entity.article.reply;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章回复点赞实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleReplythumb {

    private static final long serialVersionUID = 1L;

    private String replyThumbId;
    private String replyId;
    private LocalDateTime thumbTime;
    private String userId;


    public String getReplyThumbId() {
        return replyThumbId;
    }

    public void setReplyThumbId(String replyThumbId) {
        this.replyThumbId = replyThumbId;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public LocalDateTime getThumbTime() {
        return thumbTime;
    }

    public void setThumbTime(LocalDateTime thumbTime) {
        this.thumbTime = thumbTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "ArticleReplythumb{" +
                "replyThumbId=" + replyThumbId +
                ", replyId=" + replyId +
                ", thumbTime=" + thumbTime +
                ", userId=" + userId +
                "}";
    }
}

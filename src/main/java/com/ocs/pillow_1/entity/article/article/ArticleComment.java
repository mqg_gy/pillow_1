package com.ocs.pillow_1.entity.article.article;

import java.beans.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章评论实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleComment {

    private static final long serialVersionUID = 1L;

    private String commentId;
    private String articleId;
    private String commentContent;
    private LocalDateTime commentTime;
    private String nickname;
    private long replyCount;
    private long thumbCount;
    private String userId;


    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public LocalDateTime getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(LocalDateTime commentTime) {
        this.commentTime = commentTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(long replyCount) {
        this.replyCount = replyCount;
    }

    public long getThumbCount() {
        return thumbCount;
    }

    public void setThumbCount(long thumbCount) {
        this.thumbCount = thumbCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "ArticleComment{" +
                "commentId=" + commentId +
                ", articleId=" + articleId +
                ", commentContent=" + commentContent +
                ", commentTime=" + commentTime +
                ", nickname=" + nickname +
                ", replyCount=" + replyCount +
                ", thumbCount=" + thumbCount +
                ", userId=" + userId +
                "}";
    }
}

package com.ocs.pillow_1.entity.article.article;


import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * <p>
 * 文章实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class Article {

    private static final long serialVersionUID = 1L;

    private String articleId;
    /**
     * 文章内容
     */
    private byte[] articleContent;
    /**
     * 文章类型
     */
    private String articleTypeId;
    /**
     * 作者
     */
    private String author;
    /**
     * 下架时间
     */
    private Date downTime;
    /**
     * 上架时间
     */
    private Date onTime;
    /**
     * 发表者姓名
     */
    private String publishersName;
    /**
     * 阅读总数
     */
    private long readCount;
    /**
     * 回复总数
     */
    private long replyCount;
    /**
     * 点赞总数
     */
    private long thumbCount;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 是否置顶
     */
    private long isTop;
    private String userId;
    /**
     * 文章封面图
     */
    private String image;


    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public byte[] getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(byte[] articleContent) {
        this.articleContent = articleContent;
    }

    public String getArticleTypeId() {
        return articleTypeId;
    }

    public void setArticleTypeId(String articleTypeId) {
        this.articleTypeId = articleTypeId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDownTime() {
        return downTime;
    }

    public void setDownTime(Date downTime) {
        this.downTime = downTime;
    }

    public Date getOnTime() {
        return onTime;
    }

    public void setOnTime(Date onTime) {
        this.onTime = onTime;
    }

    public String getPublishersName() {
        return publishersName;
    }

    public void setPublishersName(String publishersName) {
        this.publishersName = publishersName;
    }

    public long getReadCount() {
        return readCount;
    }

    public void setReadCount(long readCount) {
        this.readCount = readCount;
    }

    public long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(long replyCount) {
        this.replyCount = replyCount;
    }

    public long getThumbCount() {
        return thumbCount;
    }

    public void setThumbCount(long thumbCount) {
        this.thumbCount = thumbCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getIsTop() {
        return isTop;
    }

    public void setIsTop(long isTop) {
        this.isTop = isTop;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    @Override
    public String toString() {
        return "Article{" +
                "articleId=" + articleId +
                ", articleContent=" + articleContent +
                ", articleTypeId=" + articleTypeId +
                ", author=" + author +
                ", downTime=" + downTime +
                ", onTime=" + onTime +
                ", publishersName=" + publishersName +
                ", readCount=" + readCount +
                ", replyCount=" + replyCount +
                ", thumbCount=" + thumbCount +
                ", title=" + title +
                ", isTop=" + isTop +
                ", userId=" + userId +
                ", image=" + image +
                "}";
    }
}

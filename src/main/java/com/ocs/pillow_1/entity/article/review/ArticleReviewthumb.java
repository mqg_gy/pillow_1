package com.ocs.pillow_1.entity.article.review;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章点评实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleReviewthumb {

    private static final long serialVersionUID = 1L;

    private String reviewThumbId;
    private String reviewId;
    private LocalDateTime thumbTime;
    private String userId;


    public String getReviewThumbId() {
        return reviewThumbId;
    }

    public void setReviewThumbId(String reviewThumbId) {
        this.reviewThumbId = reviewThumbId;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public LocalDateTime getThumbTime() {
        return thumbTime;
    }

    public void setThumbTime(LocalDateTime thumbTime) {
        this.thumbTime = thumbTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ArticleReviewthumb{" +
                "reviewThumbId=" + reviewThumbId +
                ", reviewId=" + reviewId +
                ", thumbTime=" + thumbTime +
                ", userId=" + userId +
                "}";
    }
}

package com.ocs.pillow_1.entity.article.reply;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章回复实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleReply {

    private static final long serialVersionUID = 1L;

    private String replyId;
    private String nickname;
    private String replyContent;
    private long replyCount;
    private LocalDateTime replyTime;
    private String reviewId;
    private long thumbCount;
    private String userId;
    private String atReplyId;


    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(long replyCount) {
        this.replyCount = replyCount;
    }

    public LocalDateTime getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(LocalDateTime replyTime) {
        this.replyTime = replyTime;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public long getThumbCount() {
        return thumbCount;
    }

    public void setThumbCount(long thumbCount) {
        this.thumbCount = thumbCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAtReplyId() {
        return atReplyId;
    }

    public void setAtReplyId(String atReplyId) {
        this.atReplyId = atReplyId;
    }


    @Override
    public String toString() {
        return "ArticleReply{" +
                "replyId=" + replyId +
                ", nickname=" + nickname +
                ", replyContent=" + replyContent +
                ", replyCount=" + replyCount +
                ", replyTime=" + replyTime +
                ", reviewId=" + reviewId +
                ", thumbCount=" + thumbCount +
                ", userId=" + userId +
                ", atReplyId=" + atReplyId +
                "}";
    }
}

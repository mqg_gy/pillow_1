package com.ocs.pillow_1.entity.article.article;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章点赞实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleThumb {

    private static final long serialVersionUID = 1L;

    private String articleThumbId;
    private String articleId;
    private LocalDateTime thumbTime;
    private String userId;


    public String getArticleThumbId() {
        return articleThumbId;
    }

    public void setArticleThumbId(String articleThumbId) {
        this.articleThumbId = articleThumbId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public LocalDateTime getThumbTime() {
        return thumbTime;
    }

    public void setThumbTime(LocalDateTime thumbTime) {
        this.thumbTime = thumbTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ArticleThumb{" +
                "articleThumbId=" + articleThumbId +
                ", articleId=" + articleId +
                ", thumbTime=" + thumbTime +
                ", userId=" + userId +
                "}";
    }
}

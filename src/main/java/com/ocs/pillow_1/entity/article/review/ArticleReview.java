package com.ocs.pillow_1.entity.article.review;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 文章点评实体类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
public class ArticleReview {

    private static final long serialVersionUID = 1L;

    private String reviewId;
    private String commentId;
    private String nickname;
    private long replyCount;
    private String reviewContent;
    private LocalDateTime reviewTime;
    private long thumbCount;
    private String userId;


    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(long replyCount) {
        this.replyCount = replyCount;
    }

    public String getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent) {
        this.reviewContent = reviewContent;
    }

    public LocalDateTime getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(LocalDateTime reviewTime) {
        this.reviewTime = reviewTime;
    }

    public long getThumbCount() {
        return thumbCount;
    }

    public void setThumbCount(long thumbCount) {
        this.thumbCount = thumbCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "ArticleReview{" +
                "reviewId=" + reviewId +
                ", commentId=" + commentId +
                ", nickname=" + nickname +
                ", replyCount=" + replyCount +
                ", reviewContent=" + reviewContent +
                ", reviewTime=" + reviewTime +
                ", thumbCount=" + thumbCount +
                ", userId=" + userId +
                "}";
    }
}

package com.ocs.pillow_1.entity;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-28
 */

public class DeviceUserRelation {


    @Id
    private Object id;

    private String relationId;

    private String deviceId;

    private String userId;

    private String did;

    private String group;

    private int status;
    private int sex;
    private String birth;

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DeviceUserRelation(Object id, String relationId, String deviceId, String userId, String did, String group, int status, int sex, String birth) {
        this.id = id;
        this.relationId = relationId;
        this.deviceId = deviceId;
        this.userId = userId;
        this.did = did;
        this.group = group;
        this.status = status;
        this.sex = sex;
        this.birth = birth;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    @Override
    public String toString() {
        return "DeviceUserRelation{" +
                "id=" + id +
                ", deviceId='" + deviceId + '\'' +
                ", userId='" + userId + '\'' +
                ", did='" + did + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}

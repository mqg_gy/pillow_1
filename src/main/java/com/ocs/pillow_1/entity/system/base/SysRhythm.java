package com.ocs.pillow_1.entity.system.base;


import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
public class SysRhythm {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Long id;
    /**
     * 年龄下界限
     */
    private Long minAge;
    /**
     * 年龄上界限（不包上）
     */
    private Long maxAge;
    /**
     * 入睡时间
     */
    private Time sleepTime;
    /**
     * 男性睡眠时长
     */
    private Double manTime;
    /**
     * 男性睡眠结束时间
     */
    private Time manEndTime;
    /**
     * 女性睡眠时长
     */
    private Double womenTime;
    /**
     * 女性睡眠结束时间
     */
    private Time womenEndTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMinAge() {
        return minAge;
    }

    public void setMinAge(Long minAge) {
        this.minAge = minAge;
    }

    public Long getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Long maxAge) {
        this.maxAge = maxAge;
    }

    public Time getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(Time sleepTime) {
        this.sleepTime = sleepTime;
    }

    public Time getManEndTime() {
        return manEndTime;
    }

    public void setManEndTime(Time manEndTime) {
        this.manEndTime = manEndTime;
    }

    public Time getWomenEndTime() {
        return womenEndTime;
    }

    public void setWomenEndTime(Time womenEndTime) {
        this.womenEndTime = womenEndTime;
    }

    public Double getManTime() {
        return manTime;
    }

    public void setManTime(Double manTime) {
        this.manTime = manTime;
    }

    public Double getWomenTime() {
        return womenTime;
    }

    public void setWomenTime(Double womenTime) {
        this.womenTime = womenTime;
    }


    @Override
    public String toString() {
        return "SysRhythm{" +
                "id=" + id +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                ", sleepTime=" + sleepTime +
                ", manTime=" + manTime +
                ", womenTime=" + womenTime +
                "}";
    }
}

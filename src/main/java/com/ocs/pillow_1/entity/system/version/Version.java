package com.ocs.pillow_1.entity.system.version;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-16
 */
public class Version {

    private static final long serialVersionUID = 1L;

    /**
     * 版本号
     */
    private String versionId;
    /**
     * 版本大小
     */
    private Long apkSize;
    /**
     * 文件路径
     */
    private String apk;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    private Long code;


    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Long getApkSize() {
        return apkSize;
    }

    public void setApkSize(Long apkSize) {
        this.apkSize = apkSize;
    }

    public String getApk() {
        return apk;
    }

    public void setApk(String apk) {
        this.apk = apk;
    }


    @Override
    public String toString() {
        return "Version{" +
                "versionId=" + versionId +
                ", apkSize=" + apkSize +
                ", apk=" + apk +
                "}";
    }
}

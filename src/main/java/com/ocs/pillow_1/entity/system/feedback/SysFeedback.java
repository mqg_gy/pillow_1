package com.ocs.pillow_1.entity.system.feedback;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
public class SysFeedback {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 反馈内容
     */
    private String content;
    /**
     * 联系方式
     */
    private String contact;
    /**
     * 反馈时间
     */
    private LocalDateTime createTime;

    /**
     * 反馈的处理结果
     */
    private String result;

    /**
     * 反馈处理状态。0：未处理；1：已处理；2：无法处理
     */
    private Integer state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysFeedback{" +
                "id=" + id +
                ", content=" + content +
                ", contact=" + contact +
                ", createTime=" + createTime +
                "}";
    }
}

package com.ocs.pillow_1.entity.system.base;


import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
public class SysCircadian {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 入睡时间
     */
    private Double timeDu;
    /**
     * 节律钟点内容
     */
    private String content;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTimeDu() {
        return timeDu;
    }

    public void setTimeDu(Double timeDu) {
        this.timeDu = timeDu;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public String toString() {
        return "SysCircadian{" +
                "id=" + id +
                ", timeDu=" + timeDu +
                ", content=" + content +
                "}";
    }
}

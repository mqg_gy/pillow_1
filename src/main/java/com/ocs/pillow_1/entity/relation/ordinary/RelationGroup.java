package com.ocs.pillow_1.entity.relation.ordinary;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */

public class RelationGroup {

    private static final long serialVersionUID = 1L;

    private String groupId;
    /**
     * 分组名
     */
    private String groupName;
    /**
     * 所属的用户
     */
    private String userId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "RelationGroup{" +
                "groupId=" + groupId +
                ", groupName=" + groupName +
                ", userId=" + userId +
                ", createTime=" + createTime +
                "}";
    }
}

package com.ocs.pillow_1.entity.relation.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import org.springframework.aop.target.LazyInitTargetSource;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class RelationRequest {

    private static final long serialVersionUID = 1L;

    private String requestId;
    private String friendId;
    private UserDetail friend;
    private String userId;
    /**
     * 请求信息
     */
    private String message;
    /**
     * 回应状态。0：未回应；1：不同意
     */
    private Integer agreeState;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 请求次数
     */
    private Long count;
    /**
     * 请求类型。1：好友请求；2：查看权限请求；3：亲情榜请求。
     */
    private Integer type;

    /**
     * 消息阅读状态。0：未读；1：已读
     */
    private Long readState;

    /**
     * 转化message，用于前端使用
     */
    private List<String> messageList;

    String note;


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAgreeState() {
        return agreeState;
    }

    public void setAgreeState(Integer agreeState) {
        this.agreeState = agreeState;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public Long getReadState() {
        return readState;
    }

    public void setReadState(Long readState) {
        this.readState = readState;
    }

    public UserDetail getFriend() {
        return friend;
    }

    public void setFriend(UserDetail friend) {
        this.friend = friend;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "RelationRequest{" +
                "requestId='" + requestId + '\'' +
                ", friendId='" + friendId + '\'' +
                ", friend=" + friend +
                ", userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                ", agreeState=" + agreeState +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", count=" + count +
                ", type=" + type +
                ", readState=" + readState +
                ", messageList=" + messageList +
                ", note='" + note + '\'' +
                '}';
    }
}

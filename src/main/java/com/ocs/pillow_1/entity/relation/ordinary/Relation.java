package com.ocs.pillow_1.entity.relation.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;

import java.io.Serializable;

/**
 * <p>
 * 关系实体
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
public class Relation {

    private static final long serialVersionUID = 1L;

    private String relationId;
    private String friendId;
    private UserDetail friend;

    private String userId;
    private User user;
    /**
     * 给朋友的备注
     */
    private String note;
    /**
     * 查看我的报告权限。0：不能；1：能
     */
    private Integer permission;
    /**
     * 角色，是否为亲人朋友。0：普通好友；1：亲人
     */
    private Integer role;

    /**
     * 好友分组。若没有分配，则默认是我的好友分组，即为0.
     */
    private String groupId;

    /*不存入数据库*/
    /**
     * 查找用户是否有设备
     */
    private Integer isExist = 0;
    /**
     * 组别的名字
     */
    private String groupName;
    /**
     * 给好友的备注描述
     */
    private String description;

//    private Integer isAffection;
//
//    public Integer getIsAffection() {
//        return isAffection;
//    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public UserDetail getFriend() {
        return friend;
    }

    public void setFriend(UserDetail friend) {
        this.friend = friend;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getIsExist() {
        return isExist;
    }

    public void setIsExist(Integer isExist) {
        this.isExist = isExist;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "relationId=" + relationId +
                ", friendId=" + friendId +
                ", userId=" + userId +
                ", note=" + note +
                ", permission=" + permission +
                ", role=" + role +
                "}";
    }
}

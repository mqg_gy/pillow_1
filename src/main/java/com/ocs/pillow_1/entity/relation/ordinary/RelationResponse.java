package com.ocs.pillow_1.entity.relation.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserDetail;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author linj123
 * @since 2019-05-21
 */

public class RelationResponse {

    private static final long serialVersionUID = 1L;

    /**
     * 标识
     */
    private String responseId;
    /**
     * 发送目标的Id
     */
    private String friendId;
    private UserDetail friend;
    /**
     * 自己的Id
     */
    private String userId;
    /**
     * 请求创建的时间
     */
    private LocalDateTime createTime;
    /**
     * 回应的时间
     */
    private LocalDateTime returnTime;
    /**
     * 回应的内容，有基本模板
     */
    private String content;

    private Long readState;

    //备注
    private String note;

    public Long getReadState() {
        return readState;
    }

    public void setReadState(Long readState) {
        this.readState = readState;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(LocalDateTime returnTime) {
        this.returnTime = returnTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserDetail getFriend() {
        return friend;
    }

    public void setFriend(UserDetail friend) {
        this.friend = friend;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "RelationResponse{" +
                "responseId='" + responseId + '\'' +
                ", friendId='" + friendId + '\'' +
                ", friend=" + friend +
                ", userId='" + userId + '\'' +
                ", createTime=" + createTime +
                ", returnTime=" + returnTime +
                ", content='" + content + '\'' +
                ", readState=" + readState +
                ", note='" + note + '\'' +
                '}';
    }
}

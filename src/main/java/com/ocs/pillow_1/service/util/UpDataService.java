package com.ocs.pillow_1.service.util;

import com.ocs.pillow_1.entity.DeviceUserRelation;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.repository.primary.DeviceUserRelationRepository;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UpDataService {
    @Autowired
    private DeviceUserRelationRepository deviceUserRelationRepository;
    @Resource
    private UserDetailService userDetailService;

    /**
     * 保存用户、设备的关联记录
     * @param relationId
     * @param deviceId
     * @param userId
     * @param did
     * @param group
     * @param status
     */
    public void save(String relationId,String deviceId,String userId,String did,String group,Integer status){
        UserDetail userDetail=userDetailService.findById(userId);
        Integer sex;
        String birthday =null;
        if(userDetail==null){
            sex=2;
            birthday = null;
        }else {
            birthday=userDetail.getBirthday();
        if (userDetail.getSex().equals("男")) {
                sex = 1;
            } else {
                sex = 2;
            }
        }
        
        deviceUserRelationRepository.save(new DeviceUserRelation(new ObjectId(),relationId,deviceId,userId,did,group,status,sex,birthday));
    }

    /**
     * 更新用户、设备的关联状态
     * @param deviceId
     * @param userId
     * @param status
     */
    public void update(String deviceId,String userId,Integer status){
        System.out.println("deviceId:"+deviceId+"---------userId:"+userId);
        DeviceUserRelation deviceUserRelation=deviceUserRelationRepository.findByDeviceIdAndUserId(deviceId, userId);
        deviceUserRelation.setStatus(status);
        deviceUserRelationRepository.save(deviceUserRelation);
    }

    /**
     * 用户修改性别
     * @param sex
     * @param userId
     */
    public void changeSex(String userId,String sex){
        Integer upSex;
        if (sex.equals("男")){
            upSex=1;
        }else {
            upSex=2;
        }
        List<DeviceUserRelation> deviceUserRelations=deviceUserRelationRepository.findByUserId(userId);
        for (DeviceUserRelation d:deviceUserRelations) {
            d.setSex(upSex);
            deviceUserRelationRepository.save(d);
        }
    }

    /**
     * 用户修改年龄
     * @param userId
     * @param birthday
     */
    public void changeAge(String userId,String birthday){
        List<DeviceUserRelation> deviceUserRelations=deviceUserRelationRepository.findByUserId(userId);
        for (DeviceUserRelation d:deviceUserRelations) {
            d.setBirth(birthday);
            deviceUserRelationRepository.save(d);
        }
    }

}

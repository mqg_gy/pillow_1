package com.ocs.pillow_1.service.util;

import org.springframework.stereotype.Service;

/**
 * 发送短信验证码
 */
@Service
public class DataService {

    /**
     * 创建验证码，并发送到相应的手机中，同时前端也能收到
     *
     * @param phone
     * @return
     */
    public Integer malCode(String phone){
        int n=0;
        while (n<100000){
            n=(int)(Math.random()*1000000);
        }
        return n;
    }
}

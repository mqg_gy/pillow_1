package com.ocs.pillow_1.service.util;

import com.ocs.pillow_1.config.FileBean;
import com.ocs.pillow_1.service.article.article.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 处理上传的文章、图片
 */
@Service
public class FileServlet extends HttpServlet {
    @Autowired
    FileBean fileBean = null;

    public void destory(){
        super.destroy();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
        response.setContentType("application/json");
        PrintWriter out=response.getWriter();
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        Enumeration reqs=request.getParameterNames();
        String message=request.getParameter("message");
        Map param=new HashMap();

        if (message.equals("article")){//ajax文件对应创建文章
            while (reqs.hasMoreElements()){
                String thisName=(String)reqs.nextElement();
                String thisValue=request.getParameter(thisName);
                param.put(thisName,thisValue);//将所有的request中的插件值传给方法
            }
        }
        ArticleService articleService=new ArticleService();
        //调用文章的新建接口
    }

    public String doFile(String path) throws IOException{
        try{
            File oldfile=new File(path);
            File newfile=new File("C:\\upload1\\"+ oldfile.getName());

            InputStream inputStream=new FileInputStream(oldfile);
            FileOutputStream fs=new FileOutputStream(newfile);

            int size=3*1024*1024;//上限是3M
            if (oldfile.length()>size){
                return "文件过大";
            }else {
                byte[] buf = new byte[size];
                fs.write(buf, 0, inputStream.read(buf));
            }

            inputStream.close();
            fs.close();
            return "上传成功";
        }catch (Exception e){
            System.out.println(e);
            return "错误"+e;
        }
    }

    public String doImg(String oldpath,String newpath) throws IOException{
        try{
            File oldfile=new File(oldpath);
            File newfile=new File(newpath+oldfile.getName());

            InputStream inputStream=new FileInputStream(oldfile);
            FileOutputStream fs=new FileOutputStream(newfile);

            int size=3*1024*1024;//上限是3M
            if (oldfile.length()>size){
                return "文件过大";
            }else {
                byte[] buf = new byte[size];
                fs.write(buf, 0, inputStream.read(buf));
            }

            inputStream.close();
            fs.close();
            return newpath+oldfile.getName();
        }catch (Exception e){
            System.out.println(e);
            return "错误"+e;
        }
    }

    public String doIcon(MultipartFile file) throws IOException{
        try {
            String savePath =fileBean.getSavePath();
            String readPath = fileBean.getReadPath();

            InputStream inputStream = new FileInputStream((File) file);
            String fileName=LocalDateTime.now()+file.getName();
            String newName=savePath+"/"+ fileName;
            FileOutputStream fs = new FileOutputStream(newName);

            int size = 3 * 1024 * 1024;//上限是3M
            if (((File) file).length() > size) {
                return "文件过大";
            } else {
                byte[] buf = new byte[size];
                fs.write(buf, 0, inputStream.read(buf));
            }

            inputStream.close();
            fs.close();
            return readPath+fileName;
        }catch (Exception e){
            return "错误"+e;
        }
    }
}

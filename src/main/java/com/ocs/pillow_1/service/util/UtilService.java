package com.ocs.pillow_1.service.util;

import com.ocs.pillow_1.entity.user.ordinary.UserToken;
import com.ocs.pillow_1.mapper.util.UtilMapper;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserTokenService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UtilService {
    @Resource
    UtilMapper utilMapper=null;
    @Resource
    UserTokenService userTokenService=null;

    /**
     * 根据token获得userId，并更新token的使用时间
     * @param token
     * @return
     */
    public String loginReturn(String token){
        UserToken userToken=userTokenService.findByToken(token);
        if (userToken!=null) {
            Map param = new HashMap();
            param.put("userId",userToken.getUserId());
            param.put("lastUsedTime", LocalDateTime.now());
            utilMapper.updateUserTime(param);
            return userToken.getUserId();
        }else {
            return null;
        }
    }
    /*
    String userId;
    if (utilService.loginReturn(token)!=null){
        userId = utilService.loginReturn(token);
    }else {
        return "token不存在";
    }
    */



    /**
     * 获取字符串中的元素值
     * @return
     */
    public List<Integer> getElement(String str){
        List<Integer> list=new ArrayList<>();
        while (str!=null){
            int index=str.indexOf("*");
            if (index==-1){
                list.add(Integer.parseInt(str.substring(0)));
                break;
            }
            list.add(Integer.parseInt(str.substring(0,index)));
            str=str.substring(index+1);
        }
        //System.out.println(list.get(1));
        return list;
    }
    /*
    List<Integer> list=new ArrayList<>();
    list=utilService.getElement(str);
    */

    public boolean compareDate(Date date){
        Date now=new Date();
        if (date.before(now)){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 加密
     * @param userId
     * @param password
     * @return
     */
    public String string2MD5(String userId,String password){
        String salt=userId.substring(20,26);//用户密码的盐
        password=salt+password;
        System.out.println("password:"+" "+password);

        MessageDigest md5=null;
        try{
            md5=MessageDigest.getInstance("MD5");
        }catch (Exception e){
            return "失败"+e;
        }
        char[] charArray=password.toCharArray();
        byte[] byteArray=new byte[charArray.length];

        for (int i=0;i<charArray.length;i++){
            byteArray[i]=(byte)charArray[i];
        }
        byte[] md5Bytes=md5.digest(byteArray);
        StringBuffer hexValue=new StringBuffer();
        for (int i=0;i<md5Bytes.length;i++){
            int val=((int)md5Bytes[i])&0xff;
            if (val<16){
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    /**
     *计算年纪
     * @param strBirth
     * @return
     * @throws ParseException
     */
    public Integer calculateBirth(String strBirth) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthday=sdf.parse(strBirth);
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthday)) {
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthday);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            }else{
                age--;
            }
        }
        return age;
    }






}

package com.ocs.pillow_1.service.goods.product;

import com.ocs.pillow_1.entity.goods.product.Product;
import com.ocs.pillow_1.entity.goods.product.ProductSku;
import com.ocs.pillow_1.mapper.goods.product.ProductMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 商品服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class ProductService {
    @Resource
    ProductMapper productMapper = null;
    @Resource
    ProductSpecitemsService productSpecitemsService = null;
    @Resource
    UtilService utilService = null;

    /**
     * 创建商品
     *
     * @param name
     * @param originalPrice
     * @param price
     * @param specItems
     * @param describes
     * @param floorPrice
     * @param spellAmount
     * @return
     */
    public String create(String name, Double originalPrice, Double price, String specItems, String describes, Double floorPrice, Double spellAmount) {
        Product product = new Product();
        product.setProductId(UUID.randomUUID().toString().replace("-", ""));
        product.setName(name);
        product.setOriginalPrice(originalPrice);
        product.setPrice(price);
        product.setCreateTime(LocalDateTime.now());
        product.setUpdateTime(LocalDateTime.now());
        product.setSpecItems(specItems);
        product.setDescribes(describes);
        product.setFloorPrice(floorPrice);
        product.setSpellAmount(spellAmount);
        try {
            productMapper.save(product);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 修改名字、原价、价格、规格项、描述、拼团底价
     *
     * @param productId
     * @param name
     * @param originalPrice
     * @param price
     * @param specItems
     * @param describes
     * @param floorPrice
     * @param spellAmount
     * @return
     */
    public String updateBaseItem(String productId, String name, Double originalPrice, Double price, String specItems, String describes, Double floorPrice, Double spellAmount) {
        Map param = new HashMap();
        param.put("productId", productId);
        param.put("name", name);
        param.put("originalPrice", originalPrice);
        param.put("price", price);
        param.put("specItems", specItems);
        param.put("describes", describes);
        param.put("floorPrice", floorPrice);
        param.put("updateTime", LocalDateTime.now());
        param.put("spellAmount", spellAmount);
        try {
            productMapper.updateBaseItem(param);
            return "修改成功";
        } catch (Exception e) {
            return "修改失败" + e;
        }
    }

    /**
     * 修改是否上架、先购后返、私人定制、分销、积分购买、拼团
     *
     * @param productId
     * @param isShelves
     * @param isBack
     * @param isCustom
     * @param isDistribute
     * @param isIntegral
     * @param isSpell
     */
    public String updateIs(String productId, Integer isShelves, Integer isBack, Integer isCustom, Integer isDistribute, Integer isIntegral, Integer isSpell) {
        Map param = new HashMap();
        param.put("productId", productId);
        param.put("isShelves", isShelves);
        param.put("isBack", isBack);
        param.put("isCustom", isCustom);
        param.put("isDistribute", isDistribute);
        param.put("isIntegral", isIntegral);
        param.put("isSpell", isSpell);
        param.put("updateTime", LocalDateTime.now());
        try {
            productMapper.updateIs(param);
            return "更新成功";
        } catch (Exception e) {
            return "更新失败" + e;
        }
    }

    /**
     * 根据Id查找
     *
     * @param productId
     * @return
     */
    public Product findById(String productId) {
        try {
            Product product = productMapper.findById(productId);
            Product product1 = new Product();
            List<Integer> itemIdList = new ArrayList<>();
            itemIdList = utilService.getElement(product.getSpecItems());//规格项的id集合，[1,2,3,4]
            List<String> list = new ArrayList<>();
            for (int i = 0; i < itemIdList.size(); i++) {
                list.clear();
                list.add(productSpecitemsService.findById(itemIdList.get(i)).getSpecName());
            }
            product1.setItemList(list);
            return product1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查询同一类型的商品。是否上架、先购后返、私人定制、分销、积分购买、拼团
     *
     * @param isShelves
     * @param isBack
     * @param isCustom
     * @param isDistribute
     * @param isIntegral
     * @param isSpell
     * @return
     */
    public List<Product> findByIs(Integer isShelves, Integer isBack, Integer isCustom, Integer isDistribute, Integer isIntegral, Integer isSpell) {
        Map param = new HashMap();
        param.put("isShelves", isShelves);
        param.put("isBack", isBack);
        param.put("isCustom", isCustom);
        param.put("isDistribute", isDistribute);
        param.put("isIntegral", isIntegral);
        param.put("isSpell", isSpell);
        try {
            List<Product> productList = productMapper.findByIs(param);
            List<Product> productList1 = new ArrayList<>();
            for (Product product : productList) {
                List<Integer> itemIdList = new ArrayList<>();
                itemIdList = utilService.getElement(product.getSpecItems());//规格值的id集合，[1,2,3,4]
                List<String> list = new ArrayList<>();
                for (int i = 0; i < itemIdList.size(); i++) {
                    list.clear();
                    list.add(productSpecitemsService.findById(itemIdList.get(i)).getSpecName());
                }
                product.setItemList(list);
                productList1.add(product);
            }
            return productList1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 删除产品
     *
     * @param productId
     */
    public String delete(String productId) {
        try {
            productMapper.delete(productId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败" + e;
        }
    }
}

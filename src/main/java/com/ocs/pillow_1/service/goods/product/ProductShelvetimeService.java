package com.ocs.pillow_1.service.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductShelvetime;
import com.ocs.pillow_1.mapper.goods.product.ProductShelvetimeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
@Service
public class ProductShelvetimeService {
    @Resource
    ProductShelvetimeMapper productShelvetimeMapper = null;

    /**
     * 创建上架记录
     *
     * @param productId
     * @param upUserId
     * @return
     */
    public String create(String productId, String upUserId) {
        ProductShelvetime productShelvetime = new ProductShelvetime();
        productShelvetime.setTimeId(UUID.randomUUID().toString().replace("-", ""));
        productShelvetime.setProductId(productId);
        productShelvetime.setUpTime(LocalDateTime.now());
        productShelvetime.setUpUserId(upUserId);
        productShelvetime.setState(0);
        try {
            productShelvetimeMapper.save(productShelvetime);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 商品下架
     *
     * @param productId
     * @param downUserId
     * @return
     */
    public String update(String productId, String downUserId) {
        Map param = new HashMap();
        param.put("productId", productId);
        param.put("downUserId", downUserId);
        param.put("downTime", LocalDateTime.now());
        param.put("state", 1);
        try {
            productShelvetimeMapper.update(param);
            return "成功";
        } catch (Exception e) {
            return "失败";
        }
    }

}

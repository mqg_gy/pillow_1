package com.ocs.pillow_1.service.goods.product;

import com.ocs.pillow_1.entity.goods.product.ProductSku;
import com.ocs.pillow_1.mapper.goods.product.ProductSkuMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 库存服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class ProductSkuService {
    @Resource
    ProductSkuMapper productSkuMapper = null;
    @Resource
    UtilService utilService = null;
    @Resource
    ProductSpecvaluesService productSpecvaluesService = null;

    /**
     * 创建库存
     *
     * @param inventory
     * @param cost
     * @param productId
     * @param speValues
     * @return
     */
    public String create(Long inventory, Double cost, String productId, String speValues) {
        ProductSku sku = new ProductSku();
        sku.setSkuId(UUID.randomUUID().toString().replace("-", ""));
        sku.setInventory(inventory);
        sku.setCost(cost);
        sku.setCreateTime(LocalDateTime.now());
        sku.setUpdateTime(LocalDateTime.now());
        sku.setProductId(productId);
        sku.setSpeValues(speValues);
        try {
            productSkuMapper.save(sku);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 根据Id查找
     *
     * @param skuId
     * @return
     */
    public ProductSku findById(String skuId) {
        try {
            ProductSku sku = productSkuMapper.findById(skuId);
            ProductSku sku1 = new ProductSku();
            List<Integer> valueIdList = new ArrayList<>();
            valueIdList = utilService.getElement(sku.getSpeValues());//规格值的id集合，[1,2,3,4]
            List<String> list = new ArrayList<>();
            for (int i = 0; i < valueIdList.size(); i++) {
                list.clear();
                list.add(productSpecvaluesService.findById(valueIdList.get(i)).getValueName());
            }
            sku1.setValueList(list);
            return sku1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据商品Id查找
     *
     * @param productId
     * @return
     */
    public List<ProductSku> findByProductId(String productId) {
        try {
            List<ProductSku> skuList = productSkuMapper.findByProductId(productId);
            List<ProductSku> skuList1 = new ArrayList<>();
            for (ProductSku sku : skuList) {
                List<Integer> valueIdList = new ArrayList<>();
                valueIdList = utilService.getElement(sku.getSpeValues());//规格值的id集合，[1,2,3,4]
                List<String> list = new ArrayList<>();
                for (int i = 0; i < valueIdList.size(); i++) {
                    list.clear();
                    list.add(productSpecvaluesService.findById(valueIdList.get(i)).getValueName());
                }
                sku.setValueList(list);
                skuList1.add(sku);
            }
            return skuList1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查找所有库存
     *
     * @return
     */
    public List<ProductSku> findAll() {
        try {
            List<ProductSku> skuList = productSkuMapper.findAll();
            List<ProductSku> skuList1 = new ArrayList<>();
            for (ProductSku sku : skuList) {
                List<Integer> valueIdList = new ArrayList<>();
                valueIdList = utilService.getElement(sku.getSpeValues());//规格值的id集合，[1,2,3,4]
                List<String> list = new ArrayList<>();
                for (int i = 0; i < valueIdList.size(); i++) {
                    list.clear();
                    list.add(productSpecvaluesService.findById(valueIdList.get(i)).getValueName());
                }
                sku.setValueList(list);
                skuList1.add(sku);
            }
            return skuList1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 更新库存
     *
     * @param skuId
     * @param cost
     * @param speValues
     * @return
     */
    public String update(String skuId, Double cost, String speValues) {
        Map param = new HashMap();
        param.put("skuId", skuId);
        param.put("cost", cost);
        param.put("speValues", speValues);
        param.put("updateTime", LocalDateTime.now());
        try {
            productSkuMapper.update(param);
            return "更新成功";
        } catch (Exception e) {
            return "更新失败" + e;
        }
    }

    /**
     * 删除库存记录
     *
     * @param skuId
     */
    public String delete(String skuId) {
        try {
            productSkuMapper.delete(skuId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败" + e;
        }
    }

    /**
     * 获得库存的id
     *
     * @param productId
     * @param speValues
     * @return
     */
    public ProductSku getSkuId(String productId, String speValues) {
        try {
            return productSkuMapper.getSkuId(productId, speValues);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 更新库存数量
     *
     * @param skuId
     * @param inventory
     * @return
     */
    public String updateInventory(String skuId, long inventory) {
        Map param = new HashMap();
        long number = findById(skuId).getInventory() + inventory;
        if (number >= 0) {
            param.put("inventory", number);
        } else {
            return "202";//库存不足
        }
        param.put("skuId", skuId);
        try {
            productSkuMapper.updateInventory(param);
            return "200";//更新成功
        } catch (Exception e) {
            return "201";//更新失败
        }
    }


}

package com.ocs.pillow_1.service.goods.device;

import com.ocs.pillow_1.entity.goods.device.Device;
import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.mapper.goods.device.DeviceRelationMapper;
import com.ocs.pillow_1.service.relation.ordinary.RelationService;
import com.ocs.pillow_1.service.util.UpDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  设备服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class DeviceRelationService {
    @Resource
    DeviceRelationMapper deviceRelationMapper=null;
    @Resource
    DeviceService deviceService=null;
    @Resource
    UpDataService upDataService=null;
    @Resource
    RelationService relationService=null;



    
    /**
     * 用户激活设备之后，创建
     * @param sn
     * @param userId
     * @return
     */
    public String create(String sn,String userId){
        DeviceRelation deviceRelation=new DeviceRelation();
        String relationId=UUID.randomUUID().toString().replace("-","");
        deviceRelation.setRelationId(relationId);
        String deviceId=deviceService.findBySn(sn).getDeviceId(); //根据设备串号查询设备id
        if (findByDevAndSate(deviceId)!=null){//查找制定设备 设备已经被绑定了
            return "204";//该设备已经被绑定！
        }
        else if (findByDevAndUser(deviceId,userId)!=null){//设备绑定关系已经存在  绑定过 只是解绑了
            return updateState(deviceId,userId,0);  //再次绑定
        }
        deviceRelation.setDeviceId(deviceId);
        deviceRelation.setUserId(userId);
        deviceRelation.setBingingTime(LocalDateTime.now());
        deviceRelation.setState(0);
        deviceRelationMapper.save(deviceRelation);
        relationService.addMeToAffection(userId);//绑定设备后，将自己存入亲情榜
        upDataService.save(relationId,deviceId,userId,sn,"小月",1);//mongodb中status=1表示绑定了
        deviceService.updateActivation(deviceId,1);//绑定设备后，设备的激活状态改变
        return "200";
    }

    /**
     * 查找设备与用户关系
     * @param deviceId
     * @param userId
     * @return
     */
    public DeviceRelation findByDevAndUser(String deviceId,String userId){
        try{
            return deviceRelationMapper.findByDevAndUser(deviceId, userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 更新设备的绑定状态
     * @param deviceId
     * @param userId
     * @param state
     * @return
     */
    @Transactional
    public String updateState(String deviceId,String userId,Integer state){
        Map param=new HashMap();
        param.put("deviceId",deviceId);
        param.put("userId",userId);
        param.put("state",state);
        //判断设备是否已经被绑定
//        if(state==0){  //为绑定状态
//            if (findByDevAndSate(deviceId)!=null){   //根据设备id查找到设备不为空  设备已经被绑定
//                return "201";
//            }
//        }
        if (state==0){  //绑定状态
            if (findByDevAndSate(deviceId)!=null){   //根据设备id查找到设备不为空  设备已经被绑定
                return "201";
            }
            if (findByDevAndUser(deviceId,userId)!=null){   //存在关系
                upDataService.update(deviceId,userId,1);//mongoDB 绑定
                relationService.addMeToAffection(userId);//绑定设备后，将自己存入亲情榜
                deviceService.updateActivation(deviceId,1);//绑定设备后，设备的激活状态改变  已激活
            }
        }else {  //解绑状态
            upDataService.update(deviceId,userId,0);  //mongoDB 解绑
            deviceService.updateActivation(deviceId,0);//解绑设备后，设备的激活状态改变 未激活
            relationService.delete(userId,userId);//删除亲情榜内的自己
            List<Relation> relations=relationService.findByUserId(userId);  //根据用户id查找用户信息
            if (relations.size()>0){
                for (Relation r:relations) {
                    relationService.update(r.getFriendId(),r.getUserId(),null,null,0,null,null);//收回所有关心我的人的查看权限
                    relationService.removeAffection(r.getUserId(),r.getFriendId());//将自己从好友的亲情榜中移除
                }
            }
        }
        deviceRelationMapper.updateState(param);
        return "200";
    }

    /**
     * 查找指定设备  且已经被绑定
     * @param deviceId
     * @return
     */
    public DeviceRelation findByDevAndSate(String deviceId){
        try{
            return deviceRelationMapper.findByDevAndSate(deviceId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找指定用户是否有设备
     * @param userId
     * @return
     */
    public DeviceRelation findIsExist(String userId){
        try{
            return deviceRelationMapper.findIsExist(userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 根据用户Id查找
     * @param userId
     * @return
     */
    public List<DeviceRelation> findByUserId(String userId){
        try{
            return deviceRelationMapper.findByUserId(userId);
        }catch (Exception e){
            return null;
        }
    }

    public List<DeviceRelation> findAll() {
        return deviceRelationMapper.findAll();
    }

    public DeviceRelation findById(String relationId) {
        return deviceRelationMapper.findById(relationId);
    }
}

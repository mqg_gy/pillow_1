package com.ocs.pillow_1.service.goods.device;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ocs.pillow_1.config.FileBean;
import com.ocs.pillow_1.entity.goods.device.Device;
import com.ocs.pillow_1.mapper.goods.device.DeviceMapper;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * <p>
 * 设备服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class DeviceService {
    @Resource
    DeviceMapper deviceMapper = null;
    @Resource
    ProductSkuService productSkuService = null;
    @Autowired
    FileBean fileBean = null;


    /**
     * 显示设备信息
     *
     * @param model
     * @param sn
     * @param insurance
     * @param source
     * @return
     */
    public Device show(String model, String sn, String insurance, String source) {
        Device device = deviceMapper.findBySn(sn);
        device.setModel(model);
        device.setSn(sn);
        device.setInsurance(insurance);
        device.setSource(source);
        return device;
    }

    /**
     * 录入设备
     *
     * @param sn
     * @param deliveryTime
     * @return
     */
    public String create(String sn, String deliveryTime, String sim, String type, String source, String model, String insurance) {
        Device device = new Device();
        device.setDeviceId(UUID.randomUUID().toString().replace("-", ""));
        device.setSn(sn);
        device.setDeliveryTime(deliveryTime);
        device.setSim(sim);
        device.setType(Long.valueOf(type));
        device.setSource(source);
        device.setModel(model);
        device.setInsurance(insurance);
        String data = createQR(sn);
        if (data.equals("201")) {
            return "201";
        } else {
            device.setQrcode(data);
        }
        try {
            deviceMapper.save(device);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 修改二维码
     *
     * @param deviceId
     */
    public String changeQR(String deviceId, String sn) {
        Map map = new HashMap();
        String data = createQR(sn);
        if (data.equals("201")) {
            return "201";
        } else {
            map.put("qrcode", data);
        }
        map.put("deviceId", deviceId);
        deviceMapper.changeQR(map);
        return "200";
    }

    /**
     * 修改销售状态
     *
     * @param deviceId
     * @param isSaled
     * @return
     */
    public String updateSaled(String deviceId, Integer isSaled) {
        Map param = new HashMap();
        param.put("deviceId", deviceId);
        param.put("isSaled", isSaled);
        try {
            deviceMapper.updateSaled(param);
            return "修改成功";
        } catch (Exception e) {
            return "修改失败" + e;
        }
    }

    /**
     * 修改激活状态
     *
     * @param deviceId
     * @param isActivation
     * @return
     */
    public String updateActivation(String deviceId, Integer isActivation) {
        Map param = new HashMap();
        param.put("deviceId", deviceId);
        param.put("isActivation", isActivation);
        try {
            deviceMapper.updateActivation(param);
            return "修改成功";
        } catch (Exception e) {
            return "修改失败" + e;
        }
    }

    /**
     * 设备能返回设备串号，据此查找设备Id
     *
     * @param sn
     * @return
     */
    public Device findBySn(String sn) {
        try {
            return deviceMapper.findBySn(sn);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查找所有设备
     *
     * @return
     */
    public List<Device> findAll() {
        return deviceMapper.findAll();
    }

    /**
     * 创建二维码
     *
     * @return
     */
    public String createQR(String sn) {
        /*创建二维码*/
        String savePath = fileBean.getSavePath();
        String readPath = fileBean.getReadPath();
        String basePath = fileBean.getBasePath();
        Integer width = 300;
        Integer height = 300;
        String format = "png";
        String fileName = sn + "_" + UUID.randomUUID().toString().replace("-", "") + ".png";
        File path = new File(savePath);//判断文件路径下的文件夹是否存在，不存在则创建
        if (!path.exists()) {
            path.mkdirs();
        }
        String saveFilePath = savePath + "/" + fileName;
        HashMap hints = new HashMap();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(basePath + "user/register?did=" + sn, BarcodeFormat.QR_CODE, width, height, hints);
            File file = new File(saveFilePath);
            MatrixToImageWriter.writeToFile(bitMatrix, format, file);
            return readPath + fileName;
        } catch (WriterException e) {
            e.printStackTrace();
            return "201";
        } catch (IOException e) {
            e.printStackTrace();
            return "201";
        }
    }

    /**
     * 查找指定设备Id的串号
     *
     * @param deviceId
     * @return
     */
    public Device findById(String deviceId) {
        try {
            return deviceMapper.findById(deviceId);
        } catch (Exception e) {
            return null;
        }
    }

}

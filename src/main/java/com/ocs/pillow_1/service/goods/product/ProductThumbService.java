package com.ocs.pillow_1.service.goods.product;

import com.ocs.pillow_1.entity.goods.product.ProductThumb;
import com.ocs.pillow_1.mapper.goods.product.ProductThumbMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Service
public class ProductThumbService {
    @Resource
    ProductThumbMapper productThumbMapper = null;

    /**
     * 创建点赞记录
     *
     * @param productId
     * @param userId
     * @return
     */
    public String create(String productId, String userId) {
        ProductThumb productThumb = new ProductThumb();
        productThumb.setProductThumbId(UUID.randomUUID().toString().replace("-", ""));
        productThumb.setProductId(productId);
        productThumb.setUserId(userId);
        productThumb.setThumbTime(LocalDateTime.now());
        try {
            productThumbMapper.save(productThumb);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 根据指定的peoductId和userId查找点赞记录
     *
     * @param productId
     * @param userId
     * @return
     */
    public ProductThumb findByProAndUser(String productId, String userId) {
        Map param = new HashMap();
        param.put("productId", productId);
        param.put("userId", userId);
        try {
            return productThumbMapper.findByProAndUser(param);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查找商品的所有点赞记录
     *
     * @param productId
     * @return
     */
    public List<ProductThumb> findByProductId(String productId) {
        try {
            return productThumbMapper.findByProductId(productId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 删除点赞记录
     *
     * @param productThumbId
     */
    public String delete(String productThumbId) {
        try {
            productThumbMapper.delete(productThumbId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败";
        }
    }

}

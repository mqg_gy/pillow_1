package com.ocs.pillow_1.service.goods.product;

import com.ocs.pillow_1.entity.goods.product.ProductImages;
import com.ocs.pillow_1.mapper.goods.product.ProductImagesMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 商品图片服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class ProductImagesService {
    @Resource
    ProductImagesMapper productImagesMapper = null;

    /**
     * 创建商品图实体
     *
     * @param productId
     * @param master
     * @param large
     * @param detail1
     * @param detail2
     * @param box
     * @return
     */
    public String save(String productId, String master, String large, String detail1, String detail2, String box) {
        ProductImages productImages = new ProductImages();
        productImages.setProductId(productId);
        productImages.setMaster(master);
        productImages.setLarge(large);
        productImages.setDetail1(detail1);
        productImages.setDetail2(detail2);
        productImages.setBox(box);
        try {
            productImagesMapper.save(productImages);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 删除商品图实体
     *
     * @param productId
     * @return
     */
    public String delete(String productId) {
        try {
            productImagesMapper.delete(productId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败" + e;
        }
    }

    /**
     * 根据id查找商品图
     *
     * @param productId
     * @return
     */
    public ProductImages findById(String productId) {
        try {
            return productImagesMapper.findById(productId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 修改商品图
     *
     * @param productId
     * @param master
     * @param large
     * @param detail1
     * @param detail2
     * @param box
     * @return
     */
    public String update(String productId, String master, String large, String detail1, String detail2, String box) {
        Map param = new HashMap();
        param.put("productId", productId);
        param.put("master", master);
        param.put("large", large);
        param.put("detail1", detail1);
        param.put("detail2", detail2);
        param.put("box", box);
        try {
            productImagesMapper.update(param);
            return "修改成功";
        } catch (Exception e) {
            return "修改失败" + e;
        }
    }

}

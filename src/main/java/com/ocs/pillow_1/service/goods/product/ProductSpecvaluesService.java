package com.ocs.pillow_1.service.goods.product;


import com.ocs.pillow_1.entity.goods.product.ProductSpecvalues;
import com.ocs.pillow_1.mapper.goods.product.ProductSpecvaluesMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 规格值服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class ProductSpecvaluesService {
    @Resource
    ProductSpecvaluesMapper productSpecvaluesMapper = null;

    /**
     * 创建规格值
     *
     * @param valueName
     * @param specId
     * @return
     */
    public String create(String valueName, long specId) {
        ProductSpecvalues productSpecvalues = new ProductSpecvalues();
        productSpecvalues.setValueName(valueName);
        productSpecvalues.setSpecId(specId);
        productSpecvalues.setCreateTime(LocalDateTime.now());
        productSpecvalues.setUpdateTime(LocalDateTime.now());
        try {
            productSpecvaluesMapper.save(productSpecvalues);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 根据规格项Id查找
     *
     * @param specValueId
     * @return
     */
    public ProductSpecvalues findById(long specValueId) {
        try {
            return productSpecvaluesMapper.findById(specValueId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 更新
     *
     * @param specValueId
     * @param valueName
     */
    public String update(long specValueId, String valueName) {
        ProductSpecvalues productSpecvalues = findById(specValueId);
        productSpecvalues.setUpdateTime(LocalDateTime.now());
        productSpecvalues.setValueName(valueName);
        try {
            productSpecvaluesMapper.update(productSpecvalues);
            return "更新成功";
        } catch (Exception e) {
            return "更新失败" + e;
        }
    }

    /**
     * 删除
     *
     * @param specId
     */
    public String delete(long specId) {
        try {
            productSpecvaluesMapper.delete(specId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败" + e;
        }
    }

    /**
     * 根据规格项查找
     *
     * @param specId
     * @return
     */
    public List<ProductSpecvalues> findBySpecId(long specId) {
        try {
            return productSpecvaluesMapper.findBySpecId(specId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 规格项级联删除
     *
     * @param specId
     */
    public String cascadeDelete(long specId) {
        try {
            productSpecvaluesMapper.cascadeDelete(specId);
            return "级联删除成功";
        } catch (Exception e) {
            return "级联删除失败" + e;
        }
    }

}

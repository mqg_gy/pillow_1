package com.ocs.pillow_1.service.goods.product;

import com.ocs.pillow_1.entity.goods.product.ProductSpecitems;
import com.ocs.pillow_1.mapper.goods.product.ProductSpecitemsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 规格项服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class ProductSpecitemsService {
    @Resource
    ProductSpecitemsMapper productSpecitemsMapper = null;
    @Resource
    ProductSpecvaluesService productSpecvaluesService = null;

    /**
     * 创建规格项
     *
     * @param specName
     * @return
     */
    public String create(String specName) {
        ProductSpecitems productSpecitems = new ProductSpecitems();
        productSpecitems.setSpecName(specName);
        productSpecitems.setCreateTime(LocalDateTime.now());
        productSpecitems.setUpdateTime(LocalDateTime.now());
        try {
            productSpecitemsMapper.save(productSpecitems);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 查找
     *
     * @param specId
     * @return
     */
    public ProductSpecitems findById(long specId) {
        try {
            return productSpecitemsMapper.findById(specId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 更新
     *
     * @param specId
     * @param specName
     */
    public String update(long specId, String specName) {
        ProductSpecitems productSpecitems = findById(specId);
        productSpecitems.setSpecName(specName);
        productSpecitems.setUpdateTime(LocalDateTime.now());
        try {
            productSpecitemsMapper.update(productSpecitems);
            return "更新成功";
        } catch (Exception e) {
            return "更新失败" + e;
        }
    }

    /**
     * 删除
     * 需要做到级联删除，如果规格项删除，values中的规格值也需要删除
     *
     * @param specId
     */
    public String delete(long specId) {
        try {
            productSpecvaluesService.cascadeDelete(specId);//删除该规格项下的所有规格值
            productSpecitemsMapper.delete(specId);
            return "删除成功";
        } catch (Exception e) {
            return "删除失败" + e;
        }
    }

}

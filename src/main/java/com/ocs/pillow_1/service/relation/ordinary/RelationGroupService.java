package com.ocs.pillow_1.service.relation.ordinary;

import com.ocs.pillow_1.entity.relation.ordinary.RelationGroup;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationGroupMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
@Service
public class RelationGroupService{
    @Resource
    RelationGroupMapper relationGroupMapper=null;

    /**
     * 创建用户分组
     * @param userId
     * @param groupName
     * @return
     */
    public String create(String userId,String groupName){
        RelationGroup relationGroup=new RelationGroup();
        relationGroup.setCreateTime(LocalDateTime.now());
        relationGroup.setGroupId(UUID.randomUUID().toString().replace("-",""));
        relationGroup.setGroupName(groupName);
        relationGroup.setUserId(userId);
        try{
            relationGroupMapper.save(relationGroup);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 删除分组
     * @param groupId
     */
    public String delete(String groupId){
        try{
            relationGroupMapper.delete(groupId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找用户的所有分组
     * @param userId
     * @return
     */
    public List<RelationGroup> findByUserId(String userId){
        try{
            return relationGroupMapper.findByUserId(userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 根据id查找分组信息
     * @param groupId
     * @return
     */
    public RelationGroup findById(String groupId){
        try{
            return relationGroupMapper.findById(groupId);
        }catch (Exception e){
            return null;
        }
    }
}

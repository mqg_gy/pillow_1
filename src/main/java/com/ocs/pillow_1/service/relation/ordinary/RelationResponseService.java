package com.ocs.pillow_1.service.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.entity.relation.ordinary.RelationResponse;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationMapper;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationResponseMapper;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-21
 */
@Service
public class RelationResponseService {
    @Resource
    RelationResponseMapper relationResponseMapper=null;
    @Resource
    UserDetailService userDetailService=null;
    @Resource
    UtilService utilService=null;
    @Resource
    RelationMapper relationMapper=null;

    /**
     * 请求的回应结果
     * @param friendId
     * @param userId
     * @param createTime
     * @param type
     * @param agreeState
     * @return
     */
    public String create(String friendId, String userId, LocalDateTime createTime,Integer type,Integer agreeState,Long readState){
        RelationResponse relationResponse=new RelationResponse();
        relationResponse.setResponseId(UUID.randomUUID().toString().replace("-",""));
        relationResponse.setFriendId(friendId);
        relationResponse.setUserId(userId);
        relationResponse.setCreateTime(createTime);
        relationResponse.setReturnTime(LocalDateTime.now());
        relationResponse.setReadState(readState);
        System.out.println("555555555555555555555555555");
        System.out.println(relationResponse.toString());
        String content;

        if (agreeState==1) {//同意了
            if (type == 1) {//好友请求
                content="同意了你的好友申请！";
            } else if (type == 2) {//权限请求
                content="同意了你的报告查看权限申请！";
            } else if (type == 3) {//亲情榜请求
                content="同意了你的亲情榜权限申请！";
            }else {
                return "202";//请求类型不对
            }
        }else if (agreeState==-1){//不同意
            if (type == 1) {//好友请求
                content="拒绝了你的好友申请！";
            } else if (type == 2) {//权限请求
                content="拒绝了你的报告查看权限申请！";
            } else if (type == 3) {//亲情榜请求
                content="拒绝了你的亲情榜权限申请！";
            }else {
                return "202";//请求类型不对
            }
        }
        else {
            return "201";//同意的状态结果不对
        }
        relationResponse.setContent(content);
        relationResponseMapper.save(relationResponse);
        return "200";
    }

    /**
     * 权限通知
     * @param friendId
     * @param userId
     * @param createTime
     * @param type
     * @param agreeState
     */
    public void createOpen(String friendId, String userId, LocalDateTime createTime,Integer type,Integer agreeState,Long readState){
        RelationResponse relationResponse=new RelationResponse();
        relationResponse.setResponseId(UUID.randomUUID().toString().replace("-",""));
        relationResponse.setFriendId(friendId);
        relationResponse.setUserId(userId);
        relationResponse.setCreateTime(createTime);
        relationResponse.setReturnTime(LocalDateTime.now());
        relationResponse.setReadState(readState);
        String content="该用户已对你开放了设备权限!";
        relationResponse.setContent(content);
        relationResponseMapper.save(relationResponse);
    }

    public void createClose(String friendId, String userId, LocalDateTime createTime,Integer type,Integer agreeState,Long readState){
        RelationResponse relationResponse=new RelationResponse();
        relationResponse.setResponseId(UUID.randomUUID().toString().replace("-",""));
        relationResponse.setFriendId(friendId);
        relationResponse.setUserId(userId);
        relationResponse.setCreateTime(createTime);
        relationResponse.setReturnTime(LocalDateTime.now());
        relationResponse.setReadState(readState);
        String content="该用户已对你关闭了设备权限!";
        relationResponse.setContent(content);
        relationResponseMapper.save(relationResponse);
    }


    /**
     * 查找响应中的未读消息
     * @param userId
     * @return
     */
    public Long findNotReadCount(String userId){
        try{
            Long data=relationResponseMapper.findNotReadCount(userId);
            return data;
        }catch (Exception e){
            return null;
        }
    }


    /**
         * 删除指定消息
         * @param responseId
         * @return
         */
    public String delete(String responseId){
        try{
            relationResponseMapper.delete(responseId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找指定用户的所有返回信息
     * @param userId
     * @return
     */
    public List<RelationResponse> findByUserId(String userId){
        try {
            List<RelationResponse> list = relationResponseMapper.findByUserId(userId);
            List<Relation> all = relationMapper.findAll();
            for (RelationResponse r : list) {

                Relation relation1 =new Relation();
                int a1=0;
                for(Relation aa :all){
                    if(aa.getFriendId().equals(r.getFriendId())&&(aa.getUserId().equals(r.getUserId())) )
                    {
                        relation1=aa;
                        a1=1;
                        break;
                    }
                }
                if(a1==1)
                {
                    r.setNote(relation1.getNote());
                    a1=0;
                }
                else if(a1==0)
                {
                    r.setNote("好友");
                }



                r.setFriend(userDetailService.findById(r.getFriendId()));
            }
            return list;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改已读状态
     * @param responseId
     * @return
     */
    public String updateReadState(String responseId){
        try {
            relationResponseMapper.updateReadState(responseId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }
}

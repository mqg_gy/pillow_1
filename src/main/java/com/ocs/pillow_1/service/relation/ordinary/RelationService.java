package com.ocs.pillow_1.service.relation.ordinary;

import com.ocs.pillow_1.entity.goods.device.DeviceRelation;
import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.entity.relation.ordinary.RelationGroup;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationMapper;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import com.ocs.pillow_1.utils.Utils;
import javafx.print.Collation;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.ocs.pillow_1.utils.Utils.distinctByKey;

/**
 * <p>
 *  关系服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class RelationService  {
    @Resource
    RelationMapper relationMapper=null;
    @Resource
    UserDetailService userDetailService=null;
    @Resource
    DeviceRelationService deviceRelationService=null;
    @Resource
    RelationGroupService relationGroupService=null;
    @Resource
    UserService userService=null;

    /**
     * 添加好友
     * @param friendId
     * @param userId
     * @return
     */
    public String create(String friendId,String userId){
        Relation relation1=new Relation();
        String relationId_1= UUID.randomUUID().toString().replace("-","");
        relation1.setRelationId(relationId_1);
        relation1.setFriendId(friendId);
        relation1.setUserId(userId);

        Relation relation2=new Relation();
        String relationId_2= UUID.randomUUID().toString().replace("-","");
        relation2.setRelationId(relationId_2);
        relation2.setFriendId(userId);
        relation2.setUserId(friendId);

        try {
            if (friendId.equals(userId)){
                relation1.setPermission(1);
                relation1.setRole(1);
                relationMapper.save(relation1);
            }else {
                relationMapper.save(relation1);
                relationMapper.save(relation2);
            }
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找具体好友记录
     * @param relationId
     * @return
     */
    public Relation findById(String relationId){
        try{
            return relationMapper.findById(relationId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找用户
     * 用户的列表调用结果，授权修改等接口已经获得了该条记录
     * @param userId
     * @return
     */
    public List<Relation> findByUserId(String userId){
        List<Relation> list=relationMapper.findByUserId(userId);   //查询用户集合
        for (Relation r:list) {   //循环集合
            r.setFriend(userDetailService.findById(r.getFriendId()));    //根据好友列表中的好友id查询好友详情  并设置
            if (deviceRelationService.findIsExist(r.getFriendId())!=null){   //如果有设备
                r.setIsExist(1);  //设置为1
            }
            RelationGroup relationGroup=relationGroupService.findById(r.getGroupId());  //根据好友分组id查询分组信息
            if (relationGroup!=null){  //如果分组不为空
                r.setGroupName(relationGroup.getGroupName());  //设置分组名称
            }
        }
        try{
            return list;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改好友的备注，或权限
     * @param userId
     * @param friendId
     * @param relationId
     * @param note
     * @param permission
     * @param role
     * @return
     */
    public String update(String userId,String friendId,String relationId,String note,Integer permission,Integer role,String description){
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("friendId",friendId);
        param.put("note",note);
        param.put("permission",permission);
        param.put("description",description);
        try{
            relationMapper.update(param);
            return "200";
        }catch (Exception e){
            System.out.println(e);
            return "201";
        }
    }

    /**
     * 关心我的好友列表
     * @param userId
     * @return
     */
    public List<Relation> careMe(String userId){
        List<Relation> list=relationMapper.findByFrirndId(userId);
        List<Relation> returnList=new ArrayList<>();
        List<Relation> all = relationMapper.findAll();

        for (Relation relation:list) {
            if (relation.getPermission()==1){
                relation.setFriend(userDetailService.findById(relation.getUserId()));
                if (deviceRelationService.findIsExist(relation.getFriendId())!=null){
                    relation.setIsExist(1);
                }
                RelationGroup relationGroup=relationGroupService.findById(relation.getGroupId());
                if (relationGroup!=null){
                    relation.setGroupName(relationGroup.getGroupName());
                }
                if (!relation.getUserId().equals(relation.getFriendId())) {

                    //用于转化备注，这里偷懒写了
                  //  System.out.println(all);

                    Relation relation1 =new Relation();
                    for(Relation aa :all){
                        if(aa.getFriendId().equals(relation.getUserId())&&(aa.getUserId().equals(relation.getFriendId())) )
                        {
                            relation1=aa;
                            break;
                        }
                    }

                    System.out.println("relation1:"+relation1.toString());
//                    List<Relation> collect = all.stream().filter((re) -> (re.getFriendId().equals(relation.getUserId())) &&
//                            (re.getUserId().equals(relation.getFriendId())) && (re.getPermission() == 1)).collect(Collectors.toList());
//                    Relation relation1 =collect.get(0);
                            String note = relation1.getNote();
                    relation.setNote(note);


                    returnList.add(relation);
                }
            }
        }
        try{
            return returnList;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 亲情榜列表
     * @param userId
     * @return
     */
    public List<Relation> affectionList(String userId){
        List<Relation> list=findByUserId(userId);
        List<Relation> returnList=new ArrayList<>();
        for (Relation relation:list) {
            if(relation.getNote()==null||relation.getNote()=="")
            {
                relation.setNote("好友");
            }


            if (relation.getRole()==1){
                Relation r=findByFandU(relation.getFriendId(),userId);//查找该好友与我的好友关系
                DeviceRelation deviceRelation=deviceRelationService.findIsExist(relation.getFriendId());//查找该好友是否已经有设备
                if (r!=null&&deviceRelation!=null&&r.getPermission()==1) {//已经是好友关系，该好友有设备，对我开放了权限
                    relation.setFriend(userDetailService.findById(relation.getFriendId()));
                    returnList.add(relation);
                }
            }
        }
        try{
            return returnList;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 我关心列表
     * @param userId
     * @return
     */
    public List<Relation> ICare(String userId){
        List<Relation> list=relationMapper.ICare(userId); //我关心列表
        List<Relation> returnList=new ArrayList<>();
        for (Relation r:list) {
            r.setFriend(userDetailService.findById(r.getFriendId()));  //好友id查询好友详情
            if (deviceRelationService.findIsExist(r.getFriendId())!=null){  //好友没有设备
                r.setIsExist(1);
            }
            RelationGroup relationGroup=relationGroupService.findById(r.getGroupId());
            if (relationGroup!=null){
                r.setGroupName(relationGroup.getGroupName());
            }
            if (!r.getFriendId().equals(r.getUserId())){
                returnList.add(r);
            }
        }
        try{
            return returnList;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 关系记录
     * @param friendId
     * @param userId
     * @return
     */
    public Relation findByFandU(String friendId,String userId){
        Map param=new HashMap();
        param.put("friendId",friendId);
        param.put("userId",userId);
        Relation s=relationMapper.findByFandU(param);
        try {
            return relationMapper.findByFandU(param);
        }catch (Exception e){
            return null;
        }
    }


    /**
     * 删除好友
     * @param friendId
     * @param userId
     * @return
     */
    public String delete(String friendId,String userId){
        try{
            if (friendId.equals(userId)) {
                relationMapper.delete(friendId, userId);
            }else {
                relationMapper.delete(friendId, userId);
                relationMapper.delete(userId, friendId);
            }
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 给好友添加进分组
     * @param relationId
     * @param groupId
     * @return
     */
    public String addGroup(String relationId,String groupId){
        Map param=new HashMap();
        param.put("relationId",relationId);
        param.put("groupId",groupId);
        try{
            relationMapper.addGroup(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 将好友移出分组
     * @param relationId
     * @return
     */
    public String removeGroup(String relationId){
        Map param=new HashMap();
        param.put("relationId",relationId);
        param.put("groupId","0");
        try{
            relationMapper.addGroup(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }




    /**
     * 分组查找指定用户的好友
     * @param userId
     * @return
     */
    public List<Object> findGroup(String userId){
        List<Relation> list11=relationMapper.findByUserId(userId);


        //因为好友圈有一定几率出现重复好友，去重（应该是加好友时就出问题，没找到）
        List<Relation> list = list11.stream().filter(Utils.distinctByKey((p) -> (p.getFriendId()))).collect(Collectors.toList());
        //直接把重复的删除吧..
        if(list11.size()!=list.size())
        {
            list11.removeAll(list);
            System.out.println(list11);
             list11.stream().forEach((r)->{
                    relationMapper.deleteById( r.getRelationId());
            });
        }

        List<Object> returnList=new ArrayList<>();
        try{
            for (Relation r:list) {//给每一个未分组的好友添加进默认分组
                r.setFriend(userDetailService.findById(r.getFriendId()));
                if (deviceRelationService.findIsExist(r.getFriendId())!=null){//显示好友是否有设备
                    r.setIsExist(1);
                }
                RelationGroup relationGroup=relationGroupService.findById(r.getGroupId());
                if (relationGroup==null){
                    if (r.getGroupId().equals("0")){
                        r.setGroupName("我的好友");
                    }
                }else {
                    r.setGroupName(relationGroup.getGroupName());
                }
            }

            List<RelationGroup> groups=relationGroupService.findByUserId(userId);//查找该用户有多少分组
            if (groups.size()==0){
                Map<Object,Object> map=new HashMap<>();
                map.put("name","我的好友");
                map.put("list",list);
                returnList.add(map);
            }else {
                //将默认分组添加进用户的分组种类中
                RelationGroup relationGroup=new RelationGroup();
                relationGroup.setGroupId("0");
                relationGroup.setGroupName("我的好友");
                groups.add(relationGroup);

                for (int i=groups.size()-1;i>=0;i--){//按照组别，将用户添加到该集合中
                    RelationGroup g=groups.get(i);
                    List<Relation> list1=new ArrayList<>();
                    Map<Object,Object> map=new HashMap<>();
                    for (Relation r:list) {
                        if (g.getGroupId().equals(r.getGroupId())){
                            list1.add(r);
                        }
                    }
                    map.put("name",g.getGroupName());
                    if (list1.size()==0){
                        List<Relation> nullList=new ArrayList<>();
                        map.put("list",nullList);
                    }else {
                        map.put("list",list1);
                    }
                    returnList.add(map);
                }
            }
            return returnList;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找陌生人
     * @param userId
     * @param friendName
     * @return
     */
    public Map findStranger(String userId,String friendName){
        Map map = new HashMap();
        try {
            User friend = userService.findByUsername(friendName);
            UserDetail friendDetail = userDetailService.findById(friend.getUserId());
            Integer isFriend = 0;//0 表示是陌生人
            Map param = new HashMap();
            param.put("userId", userId);
            param.put("friendId", friend.getUserId());
            Relation relation=relationMapper.findByFandU(param);
            map.put("note","好友");
            if (relation != null) {
                isFriend = 1;//已经是好友
                map.put("note",relation.getNote());
                map.put("description",relation.getDescription());
            }
            if (friend.getUserId().equals(userId)){
                isFriend=2;//是自己
            }
            map.put("friend", friend);
            map.put("friendDetail", friendDetail);
            map.put("isFriend", isFriend);
            return map;
        }catch (Exception e){
            return map;
        }
    }

    public List<Relation> findAll() {
        return relationMapper.findAll();
    }


    /**
     * 将好友添加进入亲情榜
     * @param friendId
     * @param userId
     * @return
     */
    public String addAffection(String friendId, String userId) {
        Map map=new HashMap();
        map.put("note",null);
        map.put("permission",null);
        map.put("role", 1);
        map.put("friendId",friendId);
        map.put("userId",userId);
        try{
            relationMapper.update(map);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 将好友移出亲情榜
     * @param friendId
     * @param userId
     * @return
     */
    public String removeAffection(String friendId, String userId){
        Map map=new HashMap();
        map.put("note",null);
        map.put("permission",null);
        map.put("role", 0);
        map.put("friendId",friendId);
        map.put("userId",userId);
        try{
            relationMapper.update(map);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 将自己加入亲情榜
     * @param userId
     */
    public String addMeToAffection(String userId) {
        Relation relation=new Relation();
        String relationId= UUID.randomUUID().toString().replace("-","");
        relation.setRelationId(relationId);
        relation.setFriendId(userId);
        relation.setUserId(userId);
        relation.setPermission(1);
        relation.setRole(1);
        relation.setNote("自己");
        try{
            relationMapper.addMeToAffection(relation);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 设置权限不可见
     * @param friendId
     * @param userId
     * @return
     */
    public String removeFriend(String friendId, String userId) {
        Map map=new HashMap();
        map.put("note",null);
        map.put("permission",0);
        map.put("role", 0);
        map.put("friendId",userId);
        map.put("userId",friendId);
        try{
            relationMapper.update(map);
            System.out.println("设置"+map+"权限不可见");
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 设置权限可见
     * @param friendId
     * @param userId
     * @return
     */
    public String openFriend(String friendId, String userId) {
        Map map=new HashMap();
        map.put("note",null);
        map.put("permission",1);
        map.put("role", 0);
        map.put("friendId",userId);
        map.put("userId",friendId);
        try{
             relationMapper.update(map);
            System.out.println("设置"+map+"权限可见");
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 开放权限列表
     * @param userId
     * @return
     */
    public List<Relation> friendPermission(String userId) {
        List<Relation> list = relationMapper.findByFrirndId(userId);  //用户好友集合
        List<Relation> returnList = new ArrayList<>();
        for (Relation relation : list) {
            relation.setFriend(userDetailService.findById(relation.getUserId()));  //好友的详细资料
            if (relation.getPermission()==0){
                returnList.add(relation);
            }
            if (relation.getFriendId().equals(relation.getUserId())){
                returnList.remove(relation);
            }
        }
        try {
            return returnList;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Relation> ICare2(String userId) {
        List<Relation> list=relationMapper.ICare2(userId);
        List<Relation> returnList=new ArrayList<>();
        for (Relation r:list) {
            r.setFriend(userDetailService.findById(r.getFriendId()));
            if (deviceRelationService.findIsExist(r.getFriendId())!=null){
                r.setIsExist(1);
            }
            RelationGroup relationGroup=relationGroupService.findById(r.getGroupId());
            if (relationGroup!=null){
                r.setGroupName(relationGroup.getGroupName());
            }
            if (!r.getFriendId().equals(r.getUserId())){
                returnList.add(r);
            }
        }
        try{
            return returnList;
        }catch (Exception e){
            return null;
        }
    }
}

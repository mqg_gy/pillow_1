package com.ocs.pillow_1.service.relation.ordinary;


import com.ocs.pillow_1.entity.relation.ordinary.Relation;
import com.ocs.pillow_1.entity.relation.ordinary.RelationRequest;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationMapper;
import com.ocs.pillow_1.mapper.relation.ordinary.RelationRequestMapper;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  关系请求服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-11
 */
@Service
public class RelationRequestService {
    @Resource
    RelationRequestMapper relationRequestMapper=null;
    @Resource
    RelationService relationService=null;
    @Resource
    RelationResponseService relationResponseService=null;
    @Resource
    UserDetailService userDetailService=null;
    @Resource
    RelationMapper relationMapper=null;

    /**
     * 创建请求
     * @param friendId
     * @param userId
     * @param message
     * @param type
     */
    public String create(String friendId,String userId,String message,Integer type){
        //判断请求是否需要
        if (type==1){
            if (relationService.findByFandU(friendId, userId)!=null){
                return "206";//该用户已经是你的好友
            }
        }else if (type==2){
            if (relationService.findByFandU(friendId, userId).getPermission()==1){
                return "207";//你已经具有该用户报告的查看权限
            }
        }else if (type==3){
            if (relationService.findByFandU(friendId, userId).getRole()==1){
                return "208";//你已经是该用户的亲情好友
            }
        }

        RelationRequest relationRequest=findByFandU(friendId, userId, type);
        if (null==relationRequest) {//该类型请求记录不存在
            relationRequest = new RelationRequest();
            relationRequest.setRequestId(UUID.randomUUID().toString().replace("-", ""));
            relationRequest.setFriendId(friendId);
            relationRequest.setUserId(userId);
            relationRequest.setMessage("###1:" + message);  //用户申请的留言备注
            relationRequest.setCreateTime(LocalDateTime.now());
            relationRequest.setUpdateTime(LocalDateTime.now());
            relationRequest.setType(type);    //type表示请求类型，1：好友请求；2：查看权限请求；3：亲情榜请求。
            relationRequest.setReadState(0L);

//            if (type==2){
//                relationRequestMapper.save(relationRequest);
//                return "210";
//            }

            try{
                relationRequestMapper.save(relationRequest);
                return "200";//创建请求成功
            }catch (Exception e){
                return "201";//创建请求失败
            }
        }else {
            long count=relationRequest.getCount()%5;
            if (count==0){//申请次数已经达到五次
                DateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                LocalDateTime dt1=relationRequest.getUpdateTime();//最新的更新时间
                LocalDateTime dt2=LocalDateTime.now();//现在的时间
                if (dt1.getDayOfYear()==dt2.getDayOfYear()) {
                    return "209";//今日请求次数已达上限
                }
            }
            String oldMessage=relationRequest.getMessage();
            String mes ="###"+(Integer.parseInt(oldMessage.substring(3, 4)) + 1) + ":" + message+relationRequest.getMessage();
            relationRequest.setMessage(mes);
            relationRequest.setUpdateTime(LocalDateTime.now());
            relationRequest.setCount(relationRequest.getCount()+1);
            relationRequest.setAgreeState(0);
            try {
                relationRequestMapper.updateRequest(relationRequest);
                return "2100";//更新请求成功
            }catch (Exception e){
                return "2101";//更新请求失败
            }
        }
    }

    /**
     * 查找请求记录
     * @param friendId
     * @param userId
     * @return
     */
    public RelationRequest findByFandU(String friendId,String userId,Integer type){
        Map param=new HashMap();
        param.put("friendId",friendId);
        param.put("userId",userId);
        param.put("type",type);
        try {
            return relationRequestMapper.findByFandU(param);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找用户中的请求列表
     * @param userId
     * @return
     */
    public List<RelationRequest> findByUserId(String userId){
        List<RelationRequest> list=relationRequestMapper.findByUserId(userId);
        List<Relation> all = relationMapper.findAll();
        for (RelationRequest r:list) {

            Relation relation1 =new Relation();
            int a1=0;
            for(Relation aa :all){
                if(aa.getFriendId().equals(r.getUserId())&&(aa.getUserId().equals(r.getFriendId())) )
                {
                    relation1=aa;
                    a1=1;
                    break;
                }
            }
            if(a1==1)
            {
                r.setNote(relation1.getNote());
                a1=0;
            }
            else if(a1==0)
            {
                r.setNote("好友");
            }



            String[] message=r.getMessage().split("###");
            List<String> list1=new ArrayList<>();
            for (int i=1;i<message.length;i++){
                list1.add(message[i].substring(2));
            }
            r.setMessage(list1.get(0));
            r.setFriend(userDetailService.findById(r.getUserId()));
        }
        return list;
    }

    /**
     * 修改回应状态
     * @param requestId
     * @param agreeState
     * @return
     */
    public String updateAgree(String requestId,Integer agreeState){
        RelationRequest relationRequest=findById(requestId);

        if (agreeState==1){//同意请求
            relationResponseService.create(relationRequest.getFriendId(),relationRequest.getUserId(),relationRequest.getCreateTime(),relationRequest.getType(),1,relationRequest.getReadState());//创建回应消息
            if (relationRequest.getType()==1){//好友请求
                relationService.create(relationRequest.getFriendId(),relationRequest.getUserId()); //"同意好友请求，创建联系";
            } else if (relationRequest.getType() == 2) {//权限请求
                relationService.update(relationRequest.getUserId(),relationRequest.getFriendId(),null,null,1,null,null); //"同意权限请求，修改联系人权限";
            }
            return delete(requestId);
        }else {//agreeState==-1 不同意
            relationResponseService.create(relationRequest.getFriendId(),relationRequest.getUserId(),relationRequest.getCreateTime(),relationRequest.getType(),-1,relationRequest.getReadState());//创建回应消息
            Map param = new HashMap();
            param.put("requestId", requestId);
            param.put("agreeState", agreeState);
            try {
                relationRequestMapper.updateAgree(param);
                return "200";
            } catch (Exception e) {
                return "201";
            }
        }
    }



    /**
     * 根据Id查找
     * @param requestId
     * @return
     */
    public RelationRequest findById(String requestId){
        try{
            return relationRequestMapper.findById(requestId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 删除请求记录
     * @param requestId
     * @return
     */
    public String delete(String requestId){
        try{
            relationRequestMapper.delete(requestId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 删除message中的信息
     * @param requestId
     * @param index
     * @return
     */
    public String deleteMessage(String requestId,Integer index){
        RelationRequest relationRequest= findById(requestId);
        String message="";
        String[] mes=relationRequest.getMessage().split("###");
        while ((index+1)<mes.length){
            mes[index]=mes[index+1];
            index=index+1;
        }
        for (int i=1;i<mes.length-1;i++){
            message=message+"###"+mes[i];
        }
        Map param=new HashMap();
        param.put("requestId",requestId);
        param.put("message",message);
        try {
            relationRequestMapper.deleteMessage(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找别人发给我的请求中的未读消息
     * @param userId
     * @return
     */
    public Long findNotReadCount(String userId){
        try{
            Long data=relationRequestMapper.findNotReadCount(userId);
            return data;
        }catch (Exception e){
            return null;
        }
    }
    /**
     * 修改已读状态
     * @param requestId
     */
    public String updateReadState(String requestId){
        try {
            relationRequestMapper.updateReadState(requestId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }


}

package com.ocs.pillow_1.service.order.buyandback;

import com.ocs.pillow_1.entity.order.buyandback.OrderBackrelation;
import com.ocs.pillow_1.mapper.order.buyandback.OrderBackrelationMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * <p>
 * 返还用户关系服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
@Service
public class OrderBackrelationService {
    @Resource
    OrderBackrelationMapper orderBackrelationMapper = null;

    /**
     * 创建关系
     *
     * @param backOrderId
     * @param subUserId
     * @return
     */
    public String create(String backOrderId, String subUserId) {
        OrderBackrelation orderBackrelation = new OrderBackrelation();
        orderBackrelation.setBackOrderId(backOrderId);
        orderBackrelation.setSubUserId(subUserId);
        orderBackrelation.setAddTime(LocalDateTime.now());
        try {
            orderBackrelationMapper.save(orderBackrelation);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

}

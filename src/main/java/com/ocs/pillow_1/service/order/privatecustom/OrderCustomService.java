package com.ocs.pillow_1.service.order.privatecustom;


import com.ocs.pillow_1.entity.goods.product.Product;
import com.ocs.pillow_1.entity.order.privatecustom.OrderCustom;
import com.ocs.pillow_1.entity.order.privatecustom.OrderCustombase;
import com.ocs.pillow_1.mapper.order.privatecustom.OrderCustomMapper;
import com.ocs.pillow_1.service.goods.product.ProductService;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  私人定制订单服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderCustomService  {
    @Resource
    OrderCustomMapper orderCustomMapper=null;
    @Resource
    ProductService productService=null;
    @Resource
    OrderCustombaseService orderCustombaseService=null;
    @Resource
    ProductSkuService productSkuService=null;
    @Resource
    UserWalletService userWalletService=null;

    /**
     * 创建订单
     * @param orderId
     * @param payMoney
     * @param count
     * @param noteItem
     * @param productId
     * @param note 定制留言
     * @param userId
     * @param speValues
     * @return
     */
    public String create(String orderId,Double payMoney,Long count,String noteItem,String productId,String note,String userId,String speValues){
        OrderCustom orderCustom=new OrderCustom();
        orderCustom.setCustomOrderId(UUID.randomUUID().toString().replace("-",""));
        orderCustom.setCreateTime(LocalDateTime.now());
        orderCustom.setOrderId(orderId);
        orderCustom.setPayMoney(payMoney);

        OrderCustombase orderCustombase=orderCustombaseService.getCustomId(noteItem, productId);
        if (note!=null) {
            orderCustom.setPrice(productService.findById(productId).getPrice() + orderCustombase.getDistancePrice());
        }else{
            orderCustom.setPrice(productService.findById(productId).getPrice());
        }
        orderCustom.setCount(count);
        orderCustom.setCustomId(orderCustombase.getCustomId());
        orderCustom.setNote(note);
        orderCustom.setUserId(userId);
        orderCustom.setSkuId(productSkuService.getSkuId(productId, speValues).getSkuId());
        try{
            orderCustomMapper.save(orderCustom);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找同一个物流订单
     * @param orderId
     * @return
     */
    public List<OrderCustom> findByOrderId(String orderId){
        try{
            return orderCustomMapper.findByOrderId(orderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找一个订单
     * @param customOrderId
     * @return
     */
    public OrderCustom findById(String customOrderId){
        try{
            return orderCustomMapper.findById(customOrderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改订单状态
     * @param orderId
     * @param state
     * @return
     */
    public String updateState(String orderId,Integer state){
        List<OrderCustom> orderCustoms=new ArrayList<>();
        orderCustoms=findByOrderId(orderId);
        for (OrderCustom order:orderCustoms) {
            String customOrderId=order.getCustomOrderId();

            Map param=new HashMap();
            param.put("customOrderId",customOrderId);
            param.put("state",state);
            if (state==2){//已发货
                param.put("deliveryTime",LocalDateTime.now());
                param.put("completeTime",null);

                //库存减
                OrderCustom orderCustom=findById(customOrderId);
                String mes=productSkuService.updateInventory(orderCustom.getSkuId(),-orderCustom.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==4){//已退货
                param.put("deliveryTime",null);
                param.put("completeTime",LocalDateTime.now());

                //库存加
                OrderCustom orderCustom=findById(customOrderId);
                String mes=productSkuService.updateInventory(orderCustom.getSkuId(),orderCustom.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==6) {//已退款
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());
            }else if(state==7){//已收货
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());

                //返回积分
                OrderCustom orderCustom=findById(customOrderId);
                userWalletService.reduceIntegral(orderCustom.getUserId(),orderCustom.getPayMoney());
            }else{//退货中或者退款中
                param.put("deliveryTime",null);
                param.put("completeTime",null);
            }
            orderCustomMapper.updateState(param);
        }
        return "修改成功";
    }

}

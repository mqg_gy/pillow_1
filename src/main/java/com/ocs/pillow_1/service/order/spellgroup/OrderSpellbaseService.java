package com.ocs.pillow_1.service.order.spellgroup;

import com.ocs.pillow_1.entity.goods.product.Product;
import com.ocs.pillow_1.entity.order.spellgroup.OrderSpellbase;
import com.ocs.pillow_1.mapper.order.spellgroup.OrderSpellbaseMapper;
import com.ocs.pillow_1.service.goods.product.ProductService;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  已经存在的团表服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderSpellbaseService {
    @Resource
    OrderSpellbaseMapper orderSpellbaseMapper=null;
    @Resource
    ProductService productService=null;
    @Resource
    OrderSpellService orderSpellService=null;
    @Resource
    ProductSkuService productSkuService=null;

    /**
     * 开团
     * @param productId
     * @param payMoney
     * @param count
     * @param speValues
     * @param userId
     * @return
     */
    public String create(String productId,Double payMoney,Long count,String speValues,String userId){
        OrderSpellbase orderSpellbase=new OrderSpellbase();
        String groupId=UUID.randomUUID().toString().replace("-","");
        orderSpellbase.setGroupId(groupId);
        orderSpellbase.setCreateTime(LocalDateTime.now());
        orderSpellbase.setUpdateTime(LocalDateTime.now());
        orderSpellbase.setRealTimePrice(productService.findById(productId).getPrice());
        orderSpellbase.setProductId(productId);
        try{
            orderSpellbaseMapper.save(orderSpellbase);
            orderSpellService.create(productId,payMoney,count,groupId,speValues,userId);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找团
     * @param groupId
     * @return
     */
    public OrderSpellbase findById(String groupId){
        try{
            return orderSpellbaseMapper.findById(groupId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找某个商品的团
     * @param productId
     * @return
     */
    public List<OrderSpellbase> findSpelling(String productId){
        try{
            return orderSpellbaseMapper.findSpelling(productId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 拼团人数增加
     * @param groupId
     * @return
     */
    public String addCount(String groupId){
        Map param=new HashMap();
        param.put("groupId",groupId);

        OrderSpellbase orderSpellbase=findById(groupId);
        Product product=productService.findById(orderSpellbase.getProductId());
        if ((orderSpellbase.getRealTimePrice()-product.getSpellAmount())>product.getFloorPrice()) {
            param.put("realTimePrice",orderSpellbase.getRealTimePrice()-product.getSpellAmount());
        }else{
            param.put("realTimePrice",product.getFloorPrice());
        }

        param.put("updateTime",LocalDateTime.now());
        param.put("count",orderSpellbase.getCount()+1);
        try{
            orderSpellbaseMapper.addCount(param);
            return "添加成功";
        }catch (Exception e){
            return "添加失败"+e;
        }
    }

    /**
     * 团成立
     * @param groupId
     */
    public String endGroup(String groupId){
        Map param=new HashMap();
        param.put("groupId",groupId);
        param.put("updateTime",LocalDateTime.now());
        param.put("dealPrice",findById(groupId).getRealTimePrice());
        try{
            orderSpellbaseMapper.endGroup(param);
            return "关闭团成功";
        }catch (Exception e){
            return "关闭团失败"+e;
        }
    }
}

package com.ocs.pillow_1.service.order.buyandback;


import com.ocs.pillow_1.entity.order.buyandback.OrderBack;
import com.ocs.pillow_1.entity.order.buyandback.OrderBackdetail;
import com.ocs.pillow_1.mapper.order.buyandback.OrderBackdetailMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>
 * 返还详情服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
@Service
public class OrderBackdetailService {
    @Resource
    OrderBackdetailMapper orderBackdetailMapper = null;
    @Resource
    OrderBackService orderBackService = null;

    /**
     * 创建返还详情表
     *
     * @param backOrderId
     * @return
     */
    public String create(String backOrderId) {
        OrderBack orderBack = orderBackService.findById(backOrderId);
        OrderBackdetail orderBackdetail = new OrderBackdetail();
        orderBackdetail.setBackOrderId(backOrderId);

        //订单收货之后的第二天才开始返还
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, +1);
        orderBackdetail.setStartTime(calendar.getTime());

        orderBackdetail.setRemainDays(orderBack.getPaymoney().longValue());
        try {
            orderBackdetailMapper.save(orderBackdetail);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

}

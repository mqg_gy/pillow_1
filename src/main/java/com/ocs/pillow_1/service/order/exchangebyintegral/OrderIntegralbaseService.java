package com.ocs.pillow_1.service.order.exchangebyintegral;

import com.ocs.pillow_1.entity.order.exchangebyintegral.OrderIntegralbase;
import com.ocs.pillow_1.mapper.order.exchangebyintegral.OrderIntegralbaseMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderIntegralbaseService {
    @Resource
    OrderIntegralbaseMapper orderIntegralbaseMapper=null;

    /**
     * 创建基本
     * @param productId
     * @param distance
     * @param needIntegral
     * @return
     */
    public String create(String productId,Double distance,Double needIntegral){
        OrderIntegralbase orderIntegralbase=new OrderIntegralbase();
        orderIntegralbase.setProductId(productId);
        orderIntegralbase.setCreateTime(LocalDateTime.now());
        orderIntegralbase.setUpdateTime(LocalDateTime.now());
        orderIntegralbase.setDistance(distance);
        orderIntegralbase.setNeedIntegral(needIntegral);
        try{
            orderIntegralbaseMapper.save(orderIntegralbase);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 修改额外的差价
     * @param productId
     * @param distance
     * @return
     */
    public String updateDistance(String productId,Double distance){
        Map param=new HashMap();
        param.put("productId",productId);
        param.put("distance",distance);
        param.put("updateTime",LocalDateTime.now());
        try{
            orderIntegralbaseMapper.updateDistance(param);
            return "修改成功";
        }catch (Exception e){
            return "修改失败";
        }
    }

    /**
     * 修改所需的积分
     * @param productId
     * @param needIntegral
     * @return
     */
    public String updateIntegral(String productId,Double needIntegral){
        Map param=new HashMap();
        param.put("productId",productId);
        param.put("needIntegral",needIntegral);
        param.put("updateTime",LocalDateTime.now());
        try{
            orderIntegralbaseMapper.updateIntegral(param);
            return "修改成功";
        }catch (Exception e){
            return "修改失败";
        }
    }

    /**
     * 删除
     * @param productId
     */
    public String delete(String productId){
        try{
            orderIntegralbaseMapper.delete(productId);
            return "删除成功";
        }catch (Exception e){
            return "删除失败"+e;
        }
    }

    /**
     * 查找
     * @param productId
     * @return
     */
    public OrderIntegralbase findById(String productId){
        try{
            return orderIntegralbaseMapper.findById(productId);
        }catch (Exception e){
            return null;
        }
    }

}

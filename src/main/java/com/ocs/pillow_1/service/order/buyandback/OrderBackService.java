package com.ocs.pillow_1.service.order.buyandback;

import com.ocs.pillow_1.entity.order.buyandback.OrderBack;
import com.ocs.pillow_1.mapper.order.buyandback.OrderBackMapper;
import com.ocs.pillow_1.service.goods.product.ProductService;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import com.ocs.pillow_1.service.util.UtilService;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 先购后返订单服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-12
 */
@Service
public class OrderBackService {
    @Resource
    OrderBackMapper orderBackMapper = null;
    @Resource
    ProductService productService = null;
    @Resource
    ProductSkuService productSkuService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserWalletService userWalletService = null;
    @Resource
    OrderBackdetailService orderBackdetailService = null;

    /**
     * 创建购物订单
     *
     * @param orderId   前端生成编号，一个购物记录中有几个商品，公用一个orderId
     * @param paymoney
     * @param count
     * @param productId
     * @param userId
     * @param speValues
     * @param parentId
     * @return
     */
    public String create(String orderId, Double paymoney, Long count, String productId, String userId, String speValues, String parentId) {
        OrderBack orderBack = new OrderBack();
        String backOrderId = UUID.randomUUID().toString().replace("-", "");
        orderBack.setBackOrderId(backOrderId);
        orderBack.setCreateTime(LocalDateTime.now());
        orderBack.setOrderId(orderId);
        orderBack.setPaymoney(paymoney);
        orderBack.setPrice(productService.findById(productId).getPrice());
        orderBack.setCount(count);
        orderBack.setUserId(userId);
        orderBack.setSkuId(productSkuService.getSkuId(productId, speValues).getSkuId());
        orderBack.setParentId(parentId);
        try {
            orderBackMapper.save(orderBack);
            orderBackdetailService.create(backOrderId);//创建返还详情表
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 查找订单
     *
     * @param backOrderId
     * @return
     */
    public OrderBack findById(String backOrderId) {
        try {
            return orderBackMapper.findById(backOrderId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 修改订单状态
     *
     * @param orderId
     * @param state
     * @return
     */
    public String updateState(String orderId, Integer state) {
        List<OrderBack> orderBacks = new ArrayList<>();
        orderBacks = findByOrderId(orderId);
        for (OrderBack order : orderBacks) {
            String backOrderId = order.getBackOrderId();

            Map param = new HashMap();
            param.put("backOrderId", backOrderId);
            param.put("state", state);
            if (state == 2) {//已发货
                param.put("deliveryTime", LocalDateTime.now());
                param.put("completeTime", null);

                //库存减
                OrderBack orderBack = findById(backOrderId);
                String mes = productSkuService.updateInventory(orderBack.getSkuId(), -orderBack.getCount());
                if (!mes.equals("200")) {
                    return mes;
                }
            } else if (state == 4) {//已退货
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());

                //库存加
                OrderBack orderBack = findById(backOrderId);
                String mes = productSkuService.updateInventory(orderBack.getSkuId(), orderBack.getCount());
                if (!mes.equals("200")) {
                    return mes;
                }
            } else if (state == 6) {//已退款
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());
            } else if (state == 7) {//已收货
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());

                //返回积分
                OrderBack orderBack = findById(backOrderId);
                userWalletService.reduceIntegral(orderBack.getUserId(), orderBack.getPaymoney());
            } else {//退货中或者退款中
                param.put("deliveryTime", null);
                param.put("completeTime", null);
            }
            orderBackMapper.updateState(param);
        }
        return "修改成功";
    }

    /**
     * 查找同一个物流订单
     *
     * @param orderId
     * @return
     */
    public List<OrderBack> findByOrderId(String orderId) {
        try {
            return orderBackMapper.findByOrderId(orderId);
        } catch (Exception e) {
            return null;
        }
    }
}

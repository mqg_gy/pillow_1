package com.ocs.pillow_1.service.order.distribute;


import com.ocs.pillow_1.entity.order.distribute.OrderDistribute;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.mapper.order.distribute.OrderDistributeMapper;
import com.ocs.pillow_1.service.goods.product.ProductService;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserDetailService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  分销订单服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderDistributeService {
    @Resource
    OrderDistributeMapper orderDistributeMapper=null;
    @Resource
    ProductService productService=null;
    @Resource
    ProductSkuService productSkuService=null;
    @Resource
    UserWalletService userWalletService=null;
    @Resource
    UserDetailService userDetailService=null;

    /**
     * 创建订单
     * @param orderId
     * @param payMoney
     * @param count
     * @param productId
     * @param userId
     * @param speValues
     * @return
     */
    public String create(String orderId,Double payMoney,Long count,String productId, String userId,String speValues){
        OrderDistribute orderDistribute=new OrderDistribute();
        orderDistribute.setDistributeOrderId(UUID.randomUUID().toString().replace("-",""));
        orderDistribute.setCreateTime(LocalDateTime.now());
        orderDistribute.setOrderId(orderId);
        orderDistribute.setPayMoney(payMoney);
        Double price=productService.findById(productId).getPrice();
        orderDistribute.setPrice(price);
        orderDistribute.setCount(count);
        orderDistribute.setProductId(productId);
        orderDistribute.setUserId(userId);
        orderDistribute.setSkuId(productSkuService.getSkuId(productId, speValues).getSkuId());
        try{
            orderDistributeMapper.save(orderDistribute);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找订单
     * @param distributeOrderId
     * @return
     */
    public OrderDistribute findById(String distributeOrderId){
        try{
            return orderDistributeMapper.findById(distributeOrderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找同一个物流中的订单
     * @param orderId
     * @return
     */
    public List<OrderDistribute> findByOrderId(String orderId){
        try{
            return orderDistributeMapper.findByOrderId(orderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改订单状态
     * @param orderId
     * @param state
     * @return
     */
    public String updateState(String orderId,Integer state){
        List<OrderDistribute> orderDistributes=new ArrayList<>();
        orderDistributes=findByOrderId(orderId);
        for (OrderDistribute order:orderDistributes) {
            String distributeOrderId=order.getDistributeOrderId();

            Map param=new HashMap();
            param.put("distributeOrderId",distributeOrderId);
            param.put("state",state);
            if (state==2){//已发货
                param.put("deliveryTime",LocalDateTime.now());
                param.put("completeTime",null);

                //库存减
                OrderDistribute orderDistribute=new OrderDistribute();
                String mes=productSkuService.updateInventory(orderDistribute.getSkuId(),-orderDistribute.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==4){//已退货
                param.put("deliveryTime",null);
                param.put("completeTime",LocalDateTime.now());

                //库存加
                OrderDistribute orderDistribute=new OrderDistribute();
                String mes=productSkuService.updateInventory(orderDistribute.getSkuId(),orderDistribute.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==6) {//已退款
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());
            }else if(state==7){//已收货,已经无法退货的状态下
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());

                //返回分销佣金
                UserDetail parent1=userDetailService.findById(userDetailService.findById(order.getUserId()).getParentId());
                if (parent1 != null) {
                    userWalletService.addBalance(parent1.getUserId(),order.getPrice()*0.15);
                    UserDetail parent2=userDetailService.findById(userDetailService.findById(parent1.getUserId()).getParentId());
                    if (parent2!=null){
                        userWalletService.addBalance(parent2.getUserId(),order.getPrice()*0.05);
                    }
                }
                //返回积分
                OrderDistribute orderDistribute=new OrderDistribute();
                userWalletService.reduceIntegral(orderDistribute.getUserId(),orderDistribute.getPayMoney());
            }else{//退货中或者退款中
                param.put("deliveryTime",null);
                param.put("completeTime",null);
            }
            orderDistributeMapper.updateState(param);
        }
        return "修改成功";
    }

}

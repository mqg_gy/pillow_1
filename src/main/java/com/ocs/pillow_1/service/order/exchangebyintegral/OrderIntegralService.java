package com.ocs.pillow_1.service.order.exchangebyintegral;

import com.ocs.pillow_1.entity.order.exchangebyintegral.OrderIntegral;
import com.ocs.pillow_1.mapper.order.exchangebyintegral.OrderIntegralMapper;
import com.ocs.pillow_1.service.goods.product.ProductService;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderIntegralService {
    @Resource
    OrderIntegralMapper orderIntegralMapper=null;
    @Resource
    ProductService productService=null;
    @Resource
    OrderIntegralbaseService orderIntegralbaseService=null;
    @Resource
    ProductSkuService productSkuService=null;
    @Resource
    UserWalletService userWalletService=null;

    /**
     * 创建订单
     * @param orderId
     * @param payMoney
     * @param count
     * @param productId
     * @param userId
     * @param speValues
     * @return
     */
    public String create(String orderId,Double payMoney,Long count,String productId,String userId,String speValues){
        if (userWalletService.findById(userId).getIntegral()>=orderIntegralbaseService.findById(productId).getNeedIntegral()){
            userWalletService.reduceIntegral(userId,orderIntegralbaseService.findById(productId).getNeedIntegral());
        }else {
            return "积分不足";
        }
        OrderIntegral orderIntegral=new OrderIntegral();
        orderIntegral.setIntegralOrderId(UUID.randomUUID().toString().replace("-",""));
        orderIntegral.setCreateTime(LocalDateTime.now());
        orderIntegral.setOrderId(orderId);

        //支付价格，是否会有优惠卷   orderIntegral.setPayMoney(orderIntegralbaseService.findById(productId).getDistance());
        orderIntegral.setPayMoney(payMoney);

        orderIntegral.setPrice(productService.findById(productId).getPrice());
        orderIntegral.setCount(count);
        orderIntegral.setProductId(productId);
        orderIntegral.setUserId(userId);
        orderIntegral.setSkuId(productSkuService.getSkuId(productId,speValues).getSkuId());
        try{
            orderIntegralMapper.save(orderIntegral);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找同一个物流订单
     * @param orderId
     * @return
     */
    public List<OrderIntegral> findByOrderId(String orderId){
        try{
            return orderIntegralMapper.findByOrderId(orderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找一个订单
     * @param integralOrderId
     * @return
     */
    public OrderIntegral findById(String integralOrderId){
        try{
            return orderIntegralMapper.findById(integralOrderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改订单状态
     * @param orderId
     * @param state
     * @return
     */
    public String updateState(String orderId,Integer state){
        List<OrderIntegral> orderIntegrals=new ArrayList<>();
        orderIntegrals=findByOrderId(orderId);
        for (OrderIntegral order:orderIntegrals) {
            String integralOrderId=order.getIntegralOrderId();

            Map param=new HashMap();
            param.put("integralOrderId",integralOrderId);
            param.put("state",state);
            if (state==2){//已发货
                param.put("deliveryTime",LocalDateTime.now());
                param.put("completeTime",null);

                //库存减
                OrderIntegral orderIntegral=findById(integralOrderId);
                String mes=productSkuService.updateInventory(orderIntegral.getSkuId(),-orderIntegral.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==4){//已退货
                param.put("deliveryTime",null);
                param.put("completeTime",LocalDateTime.now());

                //库存加
                OrderIntegral orderIntegral=findById(integralOrderId);
                String mes=productSkuService.updateInventory(orderIntegral.getSkuId(),orderIntegral.getCount());
                if (!mes.equals("200")){
                    return mes;
                }
            }else if (state==6) {//已退款
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());
            }else if(state==7){//已收货
                param.put("deliveryTime", null);
                param.put("completeTime", LocalDateTime.now());

                //返回积分
                OrderIntegral orderIntegral=findById(integralOrderId);
                userWalletService.reduceIntegral(orderIntegral.getUserId(),orderIntegral.getPayMoney());
            }else{//退货中或者退款中
                param.put("deliveryTime",null);
                param.put("completeTime",null);
            }
            orderIntegralMapper.updateState(param);
        }
        return "修改成功";
    }

}

package com.ocs.pillow_1.service.order.privatecustom;

import com.ocs.pillow_1.entity.order.privatecustom.OrderCustombase;
import com.ocs.pillow_1.mapper.order.privatecustom.OrderCustombaseMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  私人定制商品列表实体（提前写下的）服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderCustombaseService {
    @Resource
    OrderCustombaseMapper orderCustombaseMapper=null;

    /**
     * 创建项
     * @param distancePrice
     * @param noteItem
     * @param productId
     * @return
     */
    public String create(Double distancePrice,String noteItem,String productId){
        OrderCustombase orderCustombase=new OrderCustombase();
        orderCustombase.setCustomId(UUID.randomUUID().toString().replace("-",""));
        orderCustombase.setCreateTime(LocalDateTime.now());
        orderCustombase.setUpdateTime(LocalDateTime.now());
        orderCustombase.setDistancePrice(distancePrice);
        orderCustombase.setNoteItem(noteItem);
        orderCustombase.setProductId(productId);
        try{
            orderCustombaseMapper.save(orderCustombase);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找同一商品
     * @param productId
     * @return
     */
    public List<OrderCustombase> findByProductId(String productId){
        try{
            return orderCustombaseMapper.findByProductId(productId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找指定项
     * @param customId
     * @return
     */
    public OrderCustombase findById(String customId){
        try{
            return orderCustombaseMapper.findById(customId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改项目差价
     * @param customId
     * @param distancePrice
     * @return
     */
    public String updateDistancePrice(String customId,Double distancePrice){
        Map param=new HashMap();
        param.put("customId",customId);
        param.put("distancePrice",distancePrice);
        param.put("updateTime",LocalDateTime.now());
        try{
            orderCustombaseMapper.updateDistancePrice(param);
            return "修改成功";
        }catch (Exception e){
            return "修改失败"+e;
        }
    }

    /**
     * 修改定制类目
     * @param customId
     * @param noteItem
     * @return
     */
    public String updateNoteItem(String customId,String noteItem){
        Map param=new HashMap();
        param.put("customId",customId);
        param.put("noteItem",noteItem);
        param.put("updateTime",LocalDateTime.now());
        try{
            orderCustombaseMapper.updateNoteItem(param);
            return "修改成功";
        }catch (Exception e){
            return "修改失败"+e;
        }
    }

    /**
     * 根据定制类目、商品id查找
     * @param noteItem
     * @param productId
     * @return
     */
    public OrderCustombase getCustomId(String noteItem,String productId){
        try{
            return orderCustombaseMapper.getCustomId(noteItem, productId);
        }catch (Exception e){
            return null;
        }
    }
}

package com.ocs.pillow_1.service.order.spellgroup;

import com.ocs.pillow_1.entity.order.spellgroup.OrderSpell;
import com.ocs.pillow_1.mapper.order.spellgroup.OrderSpellMapper;
import com.ocs.pillow_1.service.goods.product.ProductSkuService;
import com.ocs.pillow_1.service.user.ordinary.UserWalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  拼团订单服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-15
 */
@Service
public class OrderSpellService {
    @Resource
    OrderSpellMapper orderSpellMapper=null;
    @Resource
    OrderSpellbaseService orderSpellbaseService=null;
    @Resource
    ProductSkuService productSkuService=null;
    @Resource
    UserWalletService userWalletService=null;

    /**
     * 创建订单
     * @param productId
     * @param payMoney
     * @param count
     * @param groupId
     * @param speValues
     * @param userId
     * @return
     */
    public String create(String productId,Double payMoney,Long count,String groupId,String speValues,String userId){
        OrderSpell orderSpell=new OrderSpell();
        orderSpell.setOrderId(UUID.randomUUID().toString().replace("-",""));
        orderSpell.setCreateTime(LocalDateTime.now());
        orderSpell.setPayMoney(payMoney);
        orderSpell.setCount(count);
        orderSpell.setGroupId(groupId);
        orderSpell.setSkuId(productSkuService.getSkuId(productId,speValues).getSkuId());
        orderSpell.setUserId(userId);
        try{
            orderSpellMapper.save(orderSpell);
            if (groupId!=null) {//不为全款
                orderSpellbaseService.addCount(groupId);
            }
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找订单
     * @param orderId
     * @return
     */
    public OrderSpell findById(String orderId){
        try{
            return orderSpellMapper.findById(orderId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找一个团的订单
     * @param groupId
     * @return
     */
    public List<OrderSpell> findByGroupId(String groupId){
        try{
            return orderSpellMapper.findByGroupId(groupId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 修改订单状态
     * @param orderId
     * @param state
     * @return
     */
    public String updateState(String orderId,Integer state){
        Map param=new HashMap();
        param.put("orderId",orderId);
        param.put("state",state);
        if (state==2){//已发货
            param.put("deliveryTime",LocalDateTime.now());
            param.put("completeTime",null);

            //库存减
            OrderSpell orderSpell=findById(orderId);
            String mes=productSkuService.updateInventory(orderSpell.getSkuId(),-orderSpell.getCount());
            if (!mes.equals("200")){
                return mes;
            }
        }else if (state==4){//已退货
            param.put("deliveryTime",null);
            param.put("completeTime",LocalDateTime.now());

            //库存加
            OrderSpell orderSpell=findById(orderId);
            String mes=productSkuService.updateInventory(orderSpell.getSkuId(),orderSpell.getCount());
            if (!mes.equals("200")){
                return mes;
            }
        }else if (state==6) {//已退款
            param.put("deliveryTime", null);
            param.put("completeTime", LocalDateTime.now());
        }else if(state==7){//已收货
            param.put("deliveryTime", null);
            param.put("completeTime", LocalDateTime.now());

            //返回积分
            OrderSpell orderSpell=findById(orderId);
            userWalletService.reduceIntegral(orderSpell.getUserId(),orderSpell.getPayMoney());
        }else{//退货中或者退款中
            param.put("deliveryTime",null);
            param.put("completeTime",null);
        }
        try{
            orderSpellMapper.updateState(param);
            return "状态修改成功";
        }catch (Exception e){
            return "状态修改失败"+e;
        }
    }
}

package com.ocs.pillow_1.service.system.base;


import com.ocs.pillow_1.entity.system.base.SysRhythm;
import com.ocs.pillow_1.mapper.system.base.SysRhythmMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
@Service
public class SysRhythmService {
    @Resource
    SysRhythmMapper sysRhythmMapper=null;

    /**
     * 查找睡眠的起止时间
     * @param age
     * @return
     */
    public SysRhythm findTime(Long age){
        try{
            return sysRhythmMapper.findByAge(age);
        }catch (Exception e){
            return null;
        }
    }

}

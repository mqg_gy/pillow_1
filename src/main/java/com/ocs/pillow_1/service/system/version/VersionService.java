package com.ocs.pillow_1.service.system.version;

import com.ocs.pillow_1.config.FileBean;
import com.ocs.pillow_1.entity.system.version.Version;
import com.ocs.pillow_1.mapper.system.version.VersionMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-16
 */
@Service
public class VersionService {
    @Resource
    VersionMapper versionMapper=null;
    @Autowired
    FileBean fileBean = null;

    /**
     * 上传版本
     * @param versionId
     * @param file
     * @return
     */
    public String create(String versionId, MultipartFile file){
        Long apkSize=file.getSize();
        String apk;
        String apkDownloadUrl;
        try {
            String fileRealName=file.getOriginalFilename();//获取原始文件名
           /* int pointIndex=fileRealName.lastIndexOf(".");//点号的位置
            String fileSuffix=fileRealName.substring(pointIndex);//截取文件后缀
            String fileNewName=userId;
            String saveFileName=fileNewName.concat(fileSuffix);//文件存取名*/

            String readPath = fileBean.getReadPath();
            String filepath=fileBean.getSavePath();
            File path=new File(filepath);//判断文件路径下的文件夹是否存在，不存在则创建
            if (!path.exists()){
                path.mkdirs();
            }
            apk=filepath+"/"+fileRealName;
            apkDownloadUrl=readPath + fileRealName;
            File oldFile=new File(apk);
            if (oldFile.exists()){
                oldFile.delete();
            }
            File savedFile=new File(filepath);
            savedFile=new File(filepath,fileRealName);
            FileUtils.copyInputStreamToFile(file.getInputStream(),savedFile);
        }catch (Exception e){
            return "202";
        }
        Version version=new Version();
        version.setApk(apkDownloadUrl);
        version.setApkSize(apkSize);
        version.setVersionId(versionId);
        try{
            versionMapper.save(version);
            return "200";
        }catch (Exception e){
            return "201";
        }
        /*try{
            apk="C:\\upload1\\"+ file.getName();
            File newfile=new File(apk);

            InputStream inputStream=new FileInputStream(file);
            FileOutputStream fs=new FileOutputStream(newfile);

            byte[] buf = new byte[apkSize.intValue()];
            fs.write(buf, 0, inputStream.read(buf));

            inputStream.close();
            fs.close();
            return "上传成功";
        }catch (Exception e){
            System.out.println(e);
            return "错误"+e;
        }*/
    }

    /**
     * 查找最新的的版本记录
     * @return
     */
    public Version findNew(){
        List<Version> list=versionMapper.findAll();
        if (list.size()>0){
            return list.get(0);
        }else {
            return null;
        }
    }

    /**
     * 查找全部版本信息
     * 按照code排序
     * @return
     */
    public List<Version> findAll() {
        List<Version> versions=versionMapper.findAll();
        return versions;
    }
}

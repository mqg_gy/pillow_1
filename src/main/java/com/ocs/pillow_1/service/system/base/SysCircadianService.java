package com.ocs.pillow_1.service.system.base;


import com.ocs.pillow_1.entity.system.base.SysCircadian;
import com.ocs.pillow_1.mapper.system.base.SysCircadianMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-22
 */
@Service
public class SysCircadianService  {
    @Resource
    SysCircadianMapper sysCircadianMapper=null;

    /**
     * 根据此时的时间查找对应信息
     * @return
     */
    public String findByTimeDu(){
        Double timeDu;
        LocalTime localTime= LocalTime.now();
        String[] strings=localTime.toString().split(":");
        Double[] doubles = new Double[3];
        for (int i=0;i<strings.length;i++){
            doubles[i]=Double.parseDouble(strings[i]);
        }
        if (doubles[0]==6.0){
            if (doubles[1]<30){
                timeDu=6.0;
            }else {
                timeDu=6.5;
            }
        }else {
            timeDu=doubles[0];
        }
        try {
            SysCircadian sysCircadian = sysCircadianMapper.findByTimeDu(timeDu);
            return sysCircadian.getContent();
        }catch (Exception e){
            return "201";
        }

    }
}

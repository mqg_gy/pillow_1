package com.ocs.pillow_1.service.system.feedback;

import com.ocs.pillow_1.entity.system.feedback.SysFeedback;
import com.ocs.pillow_1.mapper.system.feedback.SysFeedbackMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-05-20
 */
@Service
public class SysFeedbackService {
    @Resource
    SysFeedbackMapper sysFeedbackMapper=null;

    /**
     * 用户创建反馈
     * @param content
     * @param contact
     * @return
     */
    public String create(String content,String contact){
        SysFeedback sysFeedback=new SysFeedback();
        sysFeedback.setContent(content);
        sysFeedback.setContact(contact);
        sysFeedback.setCreateTime(LocalDateTime.now());
        try{
            sysFeedbackMapper.save(sysFeedback);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找全部的反馈申诉问题
     * @return
     */
    public List<SysFeedback> findAll() {
        return sysFeedbackMapper.findAll();
    }

    public SysFeedback findById(Integer id) {
        return sysFeedbackMapper.findById(id);
    }

    public void saveResult(SysFeedback feedback) {
        sysFeedbackMapper.saveResult(feedback);
    }
}

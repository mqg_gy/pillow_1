package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleType;
import com.ocs.pillow_1.mapper.article.article.ArticleTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 文章类型服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleTypeService {
    @Resource
    ArticleTypeMapper articleTypeMapper = null;

    /**
     * 查找类型
     *
     * @param code
     * @return
     */
    public ArticleType findByCode(Integer code) {
        try {
            return articleTypeMapper.findByCode(code);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 查询所有的栏目类型
     */
    public List<ArticleType> findAll() {
        try {
            return articleTypeMapper.findAll();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 修改栏目名称
     *
     * @param articleTypeId
     * @param typeName
     */
    public String updateName(String articleTypeId, String typeName) {
        try {
            articleTypeMapper.updateName(articleTypeId, typeName);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }
}

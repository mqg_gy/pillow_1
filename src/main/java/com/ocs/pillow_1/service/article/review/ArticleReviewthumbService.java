package com.ocs.pillow_1.service.article.review;


import com.ocs.pillow_1.entity.article.review.ArticleReviewthumb;
import com.ocs.pillow_1.mapper.article.review.ArticleReviewthumbMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleReviewthumbService {
    @Resource
    ArticleReviewthumbMapper articleReviewthumbMapper = null;
    @Resource
    ArticleReviewService articleReviewService = null;

    /**
     * 创建点评点赞记录
     *
     * @param reviewId
     * @param userId
     * @return
     */
    public String create(String reviewId, String userId) {
        if (articleReviewthumbMapper.findByRandU(reviewId, userId) == 0) {
            ArticleReviewthumb reviewthumb = new ArticleReviewthumb();
            reviewthumb.setReviewThumbId(UUID.randomUUID().toString().replace("-", ""));
            reviewthumb.setReviewId(reviewId);
            reviewthumb.setThumbTime(LocalDateTime.now());
            reviewthumb.setUserId(userId);
            try {
                articleReviewthumbMapper.save(reviewthumb);
                articleReviewService.ThumbCount(reviewId);
                return "创建点评点赞成功";
            } catch (Exception e) {
                return "创建点评点赞失败" + e;
            }
        } else {
            return "点赞已经存在";
        }
    }

    /**
     * 删除点赞记录
     *
     * @param reviewThumbId
     * @return
     */
    public String delete(String reviewThumbId) {
        try {
            articleReviewService.ThumbCountDown(articleReviewthumbMapper.findById(reviewThumbId).getReviewId());
            articleReviewthumbMapper.delete(reviewThumbId);
            return "删除点评点赞成功";
        } catch (Exception e) {
            return "删除点评点赞失败" + e;
        }

    }

}

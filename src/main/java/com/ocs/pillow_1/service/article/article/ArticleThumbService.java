package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleThumb;
import com.ocs.pillow_1.mapper.article.article.ArticleThumbMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleThumbService {
    @Resource
    ArticleThumbMapper articleThumbMapper = null;
    @Resource
    ArticleService articleService = null;

    /**
     * 创建文章点赞记录
     *
     * @param articleId
     * @param userId
     * @return
     */
    public String create(String articleId, String userId) {
        if (articleThumbMapper.findByAandU(articleId, userId) == 0) {
            ArticleThumb articlethumb = new ArticleThumb();
            String articleThumbId = UUID.randomUUID().toString().replace("-", "");
            articlethumb.setArticleThumbId(articleThumbId);
            articlethumb.setArticleId(articleId);
            articlethumb.setThumbTime(LocalDateTime.now());
            articlethumb.setUserId(userId);
            try {
                articleThumbMapper.save(articlethumb);
                articleService.ThumbCount(articleId);
                return "200";
            } catch (Exception e) {
                return "201";
            }
        } else {
            return "204";//点赞记录已经存在
        }
    }

    /**
     * 删除文章点赞记录
     *
     * @param articleThumbId
     * @return
     */
    public String delete(String articleThumbId) {
        try {
            articleService.ThumbCountDown(articleThumbMapper.findById(articleThumbId).getArticleId());
            articleThumbMapper.delete(articleThumbId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

}

package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleCommentthumb;
import com.ocs.pillow_1.mapper.article.article.ArticleCommentMapper;
import com.ocs.pillow_1.mapper.article.article.ArticleCommentthumbMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 * 评论点赞服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleCommentthumbService {
    @Resource
    ArticleCommentService articleCommentService = null;
    @Resource
    ArticleCommentthumbMapper articleCommentthumbMapper = null;

    /**
     * 创建评论的点赞记录
     *
     * @param commentId
     * @param userId
     * @return
     */
    public String create(String commentId, String userId) {
        if (articleCommentthumbMapper.findByCandU(commentId, userId) == 0) {
            ArticleCommentthumb commentthumb = new ArticleCommentthumb();
            commentthumb.setCommentThumbId(UUID.randomUUID().toString());
            commentthumb.setCommentId(commentId);
            commentthumb.setThumbTime(LocalDateTime.now());
            commentthumb.setUserId(userId);
            try {
                articleCommentthumbMapper.save(commentthumb);
                articleCommentService.ThumbCount(commentId);
                return "200";
            } catch (Exception e) {
                return "201";
            }
        } else {
            return "204";//点赞记录已经存在
        }
    }

    /**
     * 删除评论的点赞记录
     *
     * @param commentThumbId
     * @return
     */
    public String delete(String commentThumbId) {
        try {
            articleCommentService.ThumbCountDown(articleCommentthumbMapper.findById(commentThumbId).getCommentId());
            articleCommentthumbMapper.delete(commentThumbId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

}

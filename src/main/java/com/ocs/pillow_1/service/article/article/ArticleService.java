package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.Article;
import com.ocs.pillow_1.mapper.article.article.ArticleMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.*;
import sun.misc.UUDecoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 文章服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleService {
    @Resource
    ArticleMapper articleMapper = null;
    @Resource
    UtilService utilService = null;

    /**
     * 查找
     *
     * @param articleId
     * @return
     */
    public Article findById(String articleId) {
        try {
            return articleMapper.findById(articleId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 文章阅读数量+1
     *
     * @param articleId
     */
    public String ReadCount(String articleId) {
        Article article = findById(articleId);
        long readCount = article.getReadCount() + 1;
        try {
            articleMapper.updateReadCount(articleId, readCount);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 文章点赞数+1
     *
     * @param articleId
     */
    public String ThumbCount(String articleId) {
        Article article = findById(articleId);
        long thumbCount = article.getThumbCount() + 1;
        try {
            articleMapper.updateThumbCount(articleId, thumbCount);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 文章点赞数-1
     *
     * @param articleId
     */
    public String ThumbCountDown(String articleId) {
        Article article = findById(articleId);
        long thumbCount = article.getThumbCount() - 1;
        try {
            articleMapper.updateThumbCount(articleId, thumbCount);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 文章评论数+1
     *
     * @param articleId
     */
    public String ReplyCount(String articleId) {
        Article article = findById(articleId);
        long replyCount = article.getReplyCount() + 1;
        try {
            articleMapper.updateReplyCount(articleId, replyCount);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    /**
     * 文章评论数-1
     *
     * @param articleId
     */
    public String ReplyCountDown(String articleId) {
        Article article = findById(articleId);
        long replyCount = article.getReplyCount() - 1;
        try {
            articleMapper.updateReplyCount(articleId, replyCount);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    public List<Article> findByTypeId(String articleTypeId) {
        try {
            return articleMapper.findByTypeId(articleTypeId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 创建文章
     *
     * @param articleContent
     * @param articleTypeId
     * @param author
     * @param downTime
     * @param publishersName
     * @param title
     * @param userId
     * @param file
     * @param request
     * @return
     */
    public String create(byte[] articleContent, String articleTypeId, String author, Date downTime, String publishersName, String title, String userId, MultipartFile file, HttpServletRequest request) {
        String fileName = file.getOriginalFilename();
        Date date = new Date();
        String filePath = request.getSession().getServletContext().getRealPath("/imgupload/") + date.toString() + "/";
        try {
            uploadFile(file.getBytes(), filePath, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Article article = new Article();
        article.setArticleId(UUID.randomUUID().toString().replace("-", ""));
        article.setArticleContent(articleContent);
        article.setArticleTypeId(articleTypeId);
        article.setAuthor(author);
        article.setDownTime(downTime);
        article.setOnTime(new Date());
        article.setPublishersName(publishersName);
        article.setImage("/imgupload/" + date.toString() + "/" + fileName);
        article.setTitle(title);
        article.setUserId(userId);
        try {
            articleMapper.save(article);
            return "成功";
        } catch (Exception e) {
            return "失败" + e;
        }
    }

    public void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(filePath + fileName);
        out.write(file);
        out.flush();
        out.close();
    }
}

package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.Article;
import com.ocs.pillow_1.entity.article.article.ArticleReader;
import com.ocs.pillow_1.mapper.article.article.ArticleReaderMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleReaderService {
    @Resource
    ArticleReaderMapper articleReaderMapper = null;
    @Resource
    ArticleService articleService = null;

    /**
     * 添加阅读记录
     *
     * @param articleId
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String create(String articleId, String userId) {
        if (articleReaderMapper.findByAandU(articleId, userId) == 0) {
            ArticleReader reader = new ArticleReader();
            String articleReaderId = UUID.randomUUID().toString().replace("-", "");
            reader.setArticleReaderId(articleReaderId);
            reader.setUserId(userId);
            reader.setArticleId(articleId);
            reader.setReadTime(LocalDateTime.now());
            try {
                articleReaderMapper.save(reader);
                articleService.ReadCount(articleId);
                return "200";
            } catch (Exception e) {
                return "201";
            }
        } else {
            return "204";//阅读记录已存在
        }
    }

    /**
     * 查询一篇文章的所有阅读记录
     *
     * @param articleId
     * @return
     */
    public List<ArticleReader> readerList(String articleId) {
        try {
            return articleReaderMapper.readerList(articleId);
        } catch (Exception e) {
            return null;
        }
    }

}

package com.ocs.pillow_1.service.article.article;


import com.ocs.pillow_1.entity.article.article.ArticleComment;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.mapper.article.article.ArticleCommentMapper;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleCommentService {
    @Resource
    ArticleCommentMapper articleCommentMapper = null;
    @Resource
    ArticleService articleService = null;
    @Resource
    UserService userService = null;

    /**
     * 增加评论记录
     *
     * @param articleId
     * @param commentContent
     * @param userId
     * @return
     */
    public String create(String articleId, String commentContent, String userId) {
        ArticleComment articlecomment = new ArticleComment();
        String commentId = UUID.randomUUID().toString().replace("-", "");
        articlecomment.setCommentId(commentId);
        articlecomment.setArticleId(articleId);
        articlecomment.setCommentContent(commentContent);
        User user = userService.findById(userId);
        articlecomment.setNickname(user.getNickname());
        articlecomment.setUserId(userId);
        articlecomment.setCommentTime(LocalDateTime.now());
        articlecomment.setReplyCount(0);
        articlecomment.setThumbCount(0);
        try {
            articleCommentMapper.save(articlecomment);
            articleService.ReplyCount(articleId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 查找评论
     *
     * @param commentId
     * @return
     */
    public ArticleComment findById(String commentId) {
        try {
            return articleCommentMapper.findById(commentId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 评论点赞数+1
     *
     * @param commentId
     * @return
     */
    public String ThumbCount(String commentId) {
        ArticleComment articlecomment = findById(commentId);
        long thumbCount = articlecomment.getThumbCount() + 1;
        try {
            articleCommentMapper.updateThumbCount(commentId, thumbCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 评论点赞数-1
     *
     * @param commentId
     * @return
     */
    public String ThumbCountDown(String commentId) {
        ArticleComment articlecomment = findById(commentId);
        long thumbCount = articlecomment.getThumbCount() - 1;
        try {
            articleCommentMapper.updateThumbCount(commentId, thumbCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 评论的点评数+1
     *
     * @param commentId
     * @return
     */
    public String ReplyCount(String commentId) {
        ArticleComment articlecomment = findById(commentId);
        long replyCount = articlecomment.getReplyCount() + 1;
        try {
            articleCommentMapper.updateReplyCount(commentId, replyCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 评论的点评数-1
     *
     * @param commentId
     * @return
     */
    public String ReplyCountDown(String commentId) {
        ArticleComment articlecomment = findById(commentId);
        long replyCount = articlecomment.getReplyCount() - 1;
        try {
            articleCommentMapper.updateReplyCount(commentId, replyCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }

    }

    /**
     * 删除评论记录
     *
     * @param commentId
     * @return
     */
    public String delete(String commentId) {
        try {
            articleService.ReplyCountDown(findById(commentId).getArticleId());
            articleCommentMapper.delete(commentId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 查找同一篇文章的评论记录
     *
     * @param articleId
     * @return
     */
    public List<ArticleComment> commentList(String articleId) {
        try {
            return articleCommentMapper.commentList(articleId);
        } catch (Exception e) {
            return null;
        }
    }

}

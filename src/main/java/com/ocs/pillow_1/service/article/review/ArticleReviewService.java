package com.ocs.pillow_1.service.article.review;

import com.ocs.pillow_1.entity.article.review.ArticleReview;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.mapper.article.review.ArticleReviewMapper;
import com.ocs.pillow_1.service.article.article.ArticleCommentService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleReviewService {
    @Resource
    ArticleReviewMapper articleReviewMapper = null;
    @Resource
    ArticleCommentService articleCommentService = null;
    @Resource
    UserService userService = null;

    /**
     * 创建评论的点评记录
     *
     * @param userId
     * @param reviewContent
     * @param commentId
     * @return
     */
    public String create(String userId, String reviewContent, String commentId) {
        ArticleReview articlereview = new ArticleReview();
        String reviewId = UUID.randomUUID().toString().replace("-", "");
        articlereview.setReviewId(reviewId);
        articlereview.setUserId(userId);
        User user = userService.findById(userId);
        articlereview.setNickname(user.getNickname());
        articlereview.setReviewTime(LocalDateTime.now());
        articlereview.setReviewContent(reviewContent);
        articlereview.setCommentId(commentId);
        articlereview.setThumbCount(0l);
        articlereview.setReplyCount(0l);
        try {
            articleReviewMapper.save(articlereview);
            articleCommentService.ReplyCount(commentId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 查找点评记录
     *
     * @param reviewId
     * @return
     */
    public ArticleReview findById(String reviewId) {
        try {
            return articleReviewMapper.findById(reviewId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 点评的点赞数+1
     *
     * @param reviewId
     * @return
     */
    public String ThumbCount(String reviewId) {
        ArticleReview articlereview = findById(reviewId);
        long thumbCount = articlereview.getThumbCount() + 1;
        try {
            articleReviewMapper.updateThumbCount(reviewId, thumbCount);
            return "点赞成功";
        } catch (Exception e) {
            return "点赞失败" + e;
        }
    }

    /**
     * 点评的点赞数-1
     *
     * @param reviewId
     * @return
     */
    public String ThumbCountDown(String reviewId) {
        ArticleReview articlereview = findById(reviewId);
        long thumbCount = articlereview.getThumbCount() - 1;
        try {
            articleReviewMapper.updateThumbCount(reviewId, thumbCount);
            return "取消点赞成功";
        } catch (Exception e) {
            return "取消点赞失败" + e;
        }
    }

    /**
     * 点评的回复数+1
     *
     * @param reviewId
     * @return
     */
    public String ReplyCount(String reviewId) {
        ArticleReview articlereview = findById(reviewId);
        long reply = articlereview.getReplyCount() + 1;
        try {
            articleReviewMapper.updateReplyCount(reviewId, reply);
            return "回复成功";
        } catch (Exception e) {
            return "回复失败" + e;
        }
    }

    /**
     * 点评的回复数-1
     *
     * @param reviewId
     * @return
     */
    public String ReplyCountDown(String reviewId) {
        ArticleReview articlereview = findById(reviewId);
        long reply = articlereview.getReplyCount() - 1;
        try {
            articleReviewMapper.updateReplyCount(reviewId, reply);
            return "点评成功";
        } catch (Exception e) {
            return "点评失败" + e;
        }
    }

    /**
     * 删除点评记录
     *
     * @param reviewId
     * @return
     */
    public String delete(String reviewId) {
        try {
            articleCommentService.ReplyCountDown(findById(reviewId).getCommentId());
            articleReviewMapper.delete(reviewId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 查找评论下的所有点评
     *
     * @param commentId
     * @return
     */
    public List<ArticleReview> reviewList(String commentId) {
        try {
            return articleReviewMapper.reviewList(commentId);
        } catch (Exception e) {
            return null;
        }
    }
}

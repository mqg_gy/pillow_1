package com.ocs.pillow_1.service.article.reply;


import com.ocs.pillow_1.entity.article.reply.ArticleReplythumb;
import com.ocs.pillow_1.mapper.article.reply.ArticleReplythumbMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 * 回复点赞服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleReplythumbService {
    @Resource
    ArticleReplythumbMapper articleReplythumbMapper = null;
    @Resource
    ArticleReplyService articleReplyService = null;

    /**
     * 创建点赞记录
     *
     * @param replyId
     * @param userId
     * @return
     */
    public String create(String replyId, String userId) {
        if (articleReplythumbMapper.findByRandU(replyId, userId) == 0) {
            ArticleReplythumb replythumb = new ArticleReplythumb();
            replythumb.setReplyThumbId(UUID.randomUUID().toString().replace("-", ""));
            replythumb.setReplyId(replyId);
            replythumb.setThumbTime(LocalDateTime.now());
            replythumb.setUserId(userId);
            try {
                articleReplythumbMapper.save(replythumb);
                articleReplyService.ThumbCount(replyId);
                return "200";
            } catch (Exception e) {
                return "201";
            }
        } else {
            return "204";//点赞记录已存在
        }
    }

    /**
     * 取消点赞记录
     *
     * @param replyThumbId
     * @return
     */
    public String delete(String replyThumbId) {
        try {
            articleReplyService.ThumbCountDown(articleReplythumbMapper.findById(replyThumbId).getReplyId());
            articleReplythumbMapper.delete(replyThumbId);
            return "200";
        } catch (Exception e) {
            return "201";
        }

    }

}

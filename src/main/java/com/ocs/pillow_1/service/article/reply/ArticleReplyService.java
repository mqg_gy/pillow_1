package com.ocs.pillow_1.service.article.reply;


import com.ocs.pillow_1.entity.article.reply.ArticleReply;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.mapper.article.reply.ArticleReplyMapper;
import com.ocs.pillow_1.service.article.review.ArticleReviewService;
import com.ocs.pillow_1.service.user.ordinary.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class ArticleReplyService {
    @Resource
    ArticleReplyMapper articleReplyMapper = null;
    @Resource
    ArticleReviewService articleReviewService = null;
    @Resource
    UserService sysUserService = null;

    /**
     * 查找回复
     *
     * @param replyId
     * @return
     */
    public ArticleReply findById(String replyId) {
        try {
            return articleReplyMapper.findById(replyId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 创建回复
     *
     * @param replyContent
     * @param reviewId
     * @param userId
     * @param atReplyId
     * @return
     */
    public String create(String replyContent, String reviewId, String userId, String atReplyId) {
        ArticleReply articlereply = new ArticleReply();
        String replyId = UUID.randomUUID().toString().replace("-", "");
        articlereply.setReplyId(replyId);
        articlereply.setReplyContent(replyContent);
        articlereply.setReviewId(reviewId);
        articlereply.setUserId(userId);
        User user = sysUserService.findById(userId);
        articlereply.setNickname(user.getNickname());
        articlereply.setAtReplyId(atReplyId);
        articlereply.setReplyCount(0);
        articlereply.setThumbCount(0);
        articlereply.setReplyTime(LocalDateTime.now());
        if (atReplyId != null) {
            ReplyCount(atReplyId);
        }
        try {
            articleReviewService.ReplyCount(reviewId);
            articleReplyMapper.save(articlereply);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 回复点赞数+1
     *
     * @param replyId
     * @return
     */
    public String ThumbCount(String replyId) {
        ArticleReply articlereply = findById(replyId);
        long thumbCount = articlereply.getThumbCount() + 1;
        try {
            articleReplyMapper.updateThumbCount(replyId, thumbCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 回复点赞数-1
     *
     * @param replyId
     * @return
     */
    public String ThumbCountDown(String replyId) {
        ArticleReply articlereply = findById(replyId);
        long thumbCount = articlereply.getThumbCount() - 1;
        try {
            articleReplyMapper.updateThumbCount(replyId, thumbCount);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 回复数+1
     *
     * @param replyId
     * @return
     */
    public String ReplyCount(String replyId) {
        ArticleReply articlereply = findById(replyId);
        long replyCount = articlereply.getReplyCount() + 1;
        try {
            articleReplyMapper.updateReplyCount(replyId, replyCount);
            return "回复成功";
        } catch (Exception e) {
            return "回复失败" + e;
        }
    }

    /**
     * 回复数-1
     *
     * @param replyId
     * @return
     */
    public String ReplyCountDown(String replyId) {
        ArticleReply articlereply = findById(replyId);
        long replyCount = articlereply.getReplyCount() - 1;
        try {
            articleReplyMapper.updateReplyCount(replyId, replyCount);
            return "取消回复成功";
        } catch (Exception e) {
            return "取消回复失败" + e;
        }
    }

    /**
     * 删除回复记录
     *
     * @param replyId
     * @return
     */
    public String delete(String replyId) {
        ArticleReply articlereply = findById(replyId);
        if (articlereply.getAtReplyId() != null) {
            ReplyCountDown(articlereply.getAtReplyId());
        }
        try {
            articleReviewService.ReplyCountDown(articlereply.getReviewId());
            articleReplyMapper.delete(replyId);
            return "200";
        } catch (Exception e) {
            return "201";
        }
    }

    /**
     * 查询同一点评下的所有回复
     *
     * @param reviewId
     * @return
     */
    public List<ArticleReply> replyList(String reviewId) {
        try {
            return articleReplyMapper.replyList(reviewId);
        } catch (Exception e) {
            return null;
        }
    }

}

package com.ocs.pillow_1.service.user.ordinary;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.mapper.user.ordinary.UserDetailMapper;
import com.ocs.pillow_1.mapper.user.ordinary.UserInfodataMapper;
import com.ocs.pillow_1.mapper.user.ordinary.UserMapper;
import com.ocs.pillow_1.service.relation.ordinary.RelationGroupService;
import com.ocs.pillow_1.service.util.UtilService;
/*import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;*/
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *  普通用户服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-04
 */
@Service
public class UserService {
    @Resource
    UserMapper userMapper = null;
    @Resource
    UserWalletService userWalletService = null;
    @Resource
    UserDetailService userDetailService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserTokenService userTokenService = null;
    @Resource
    UserIntegrallogService userIntegrallogService = null;
    @Resource
    UserService userService=null;
    @Resource
    UserDetailMapper userDetailMapper=null;
    @Resource
    UserInfodataMapper userInfodataMapper=null;


//    public Object create(String parentId,String username,String password,String nickname,String birthday, String sex,Double weight,String wakeupTime,String idealWakeupTime,String loadWay,String openId) throws ParseException {
//        User user=new User();
//        String userId=UUID.randomUUID().toString().replace("-","");
//        user.setUserId(userId);
//        if (findByUsername(username)!=null){
//            return "202";//该用户名已经存在
//        }
//        user.setUsername(username);
//        user.setPassword(utilService.string2MD5(userId,password));
//        user.setNickname(nickname);
//        user.setRegisterTime(LocalDateTime.now());
//        Integer age=utilService.calculateBirth(birthday);
//        try {
//            userMapper.save(user);
//            userDetailService.create(userId,parentId,age,birthday,sex,username,weight,wakeupTime,idealWakeupTime,loadWay,openId);
//            userWalletService.create(userId);
//            String token=userTokenService.login(username,password,0).getToken();
//            user.setToken(token);
//            user.setPassword(password);
//            return user;
//        }catch (Exception e){
//            return "201";//注册失败
//        }
//    }

    /**
     * 注册第一步
     * @param username
     * @param password
     * @return
     */
    public Object createOne(String username,String password){
        User user=new User();
        String userId=UUID.randomUUID().toString().replace("-","");
        user.setUserId(userId);
        user.setUsername(username);
        user.setPassword(utilService.string2MD5(userId,password));
        user.setNickname(username);
        user.setRegisterTime(LocalDateTime.now());
        if (findByUsername(username)!=null){
            return "202";//该用户名已经存在
        }
        try {
            userMapper.save(user);

            return user;
        }catch (Exception e){
            return "201";//注册失败
        }
    }

    /**
     * 注册第二步 资料填写
     * @param username
     * @param birthday
     * @param sex
     * @param weight
     * @param wakeupTime
     * @param idealWakeupTime
     * @param loadWay
     * @param openId
     * @return
     * @throws ParseException
     */
    public Object create(String username, String birthday, String sex, Double weight, String wakeupTime, String idealWakeupTime, String loadWay, String openId) throws ParseException {
        User user = userMapper.findByUsername(username);
        String password = user.getPassword();
        String userId = user.getUserId();
        Integer age = utilService.calculateBirth(birthday);
        try {
            String info = userDetailService.create(userId, age, birthday, sex, username, weight, wakeupTime, idealWakeupTime, loadWay, openId);
//            String token = userTokenService.login(username, password, 0).getToken();
            String token = userTokenService.getUserToken(username).getToken();
            user.setToken(token);
            user.setPassword(password);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return "201";//注册失败
        }
    }

    /**
     * 修改昵称或者用户名
     * @param token
     * @param username
     * @param nickname
     * @return
     */
    public String updateNickandName(String token,String username,String nickname){
        if (utilService.loginReturn(token)!=null) {
            String userId = utilService.loginReturn(token);
            User user=findById(userId);
            Map param = new HashMap();
            param.put("userId", userId);
            param.put("registerTime", LocalDateTime.now());
            param.put("username", username);
            param.put("nickname", nickname);
            if (nickname!=null&&user.getNickname()==null){
                userIntegrallogService.create(userId,10.0,null,"个人昵称修改奖励");
            }
            try {
                userMapper.updateNickandName(param);
                if (username!=null) {//修改用户名之后，同时修改token中的用户名
                    userTokenService.updateUsername(userId, username);
                    userDetailService.updateDetail(userId,null,null,null,username,null,null,null,null,null,null,null,null);
                }
                return "200";
            } catch (Exception e) {
                return "201";
            }
        }else {
            return "token不存在";
        }
    }

    /**
     * 根据Id查找用户
     * @param userId
     * @return
     */
    public User findById(String userId){
        return userMapper.findById(userId);
    }

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findByUsername(String username){
        return userMapper.findByUsername(username);
    }

    /**
     * 密码修改或找回
     * @param username
     * @param password
     * @return
     */
    public String updatePassword(String username,String password){
        User user=findByUsername(username);
        user.setRegisterTime(LocalDateTime.now());
        user.setPassword(utilService.string2MD5(user.getUserId(),password));
        try {
            userMapper.updatePassword(user);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 短信发送接口
     * @param phone
     * @return
     * @throws IOException
     */
   public String sendNote(String phone) throws IOException {
       int checkCode=0;
       while (checkCode<100000){
           checkCode=(int) (Math.random()*1000000);
       }
       String url ="http://218.244.141.161:8888/sms.aspx?action=send&userid=707&account=bl1422&password=xiaoyuezhineng.com&mobile="+phone+"&content=帐号验证码"+checkCode+"(60秒内有效，请勿将验证码透露给他人)&sendTime=&extno=";
       HttpGet httpGet =new HttpGet(url);
       CloseableHttpClient httpclient = HttpClients.createDefault();
       CloseableHttpResponse httpResponse = httpclient.execute(httpGet);
       String data = EntityUtils.toString(httpResponse.getEntity());
       httpResponse.close();
       /*def ret = new XmlParser().parseText(data)*/
       return String.valueOf(checkCode);
   }

    public List<User> findAll() {
       try{
           return userMapper.findAll();
       }catch (Exception e){
           return null;
       }

    }

    /**
     * 前端页面的根据用户名模糊查询
     * @param user
     * @return
     */
    public List<User> findByUsernameForWeb(User user) {
        return userMapper.findByUsernameForWeb(user);
    }


    public String findUserInfoByOpenId(String openId){
       return String.valueOf(userInfodataMapper.selectUserById(openId));
    }

    public User findByOpenId(String openId){
        return userMapper.findByOpenId(openId);
    }
}

package com.ocs.pillow_1.service.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserDraw;
import com.ocs.pillow_1.mapper.user.ordinary.UserDrawMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-29
 */
@Service
public class UserDrawService {
    @Resource
    UserDrawMapper userDrawMapper=null;

    /**
     * 创建中奖记录
     * @param userId
     * @param drawMessage
     * @return
     */
    public String create(String userId,String drawMessage){
        UserDraw userDraw=new UserDraw();
        userDraw.setDrawId(UUID.randomUUID().toString().replace("-",""));
        userDraw.setUserId(userId);
        userDraw.setDrawMessage(drawMessage);
        userDraw.setTime(LocalDateTime.now());
        try{
            userDrawMapper.save(userDraw);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找指定用户的所有中奖记录
     * @param userId
     * @return
     */
    public List<UserDraw> findByUserId(String userId){
        try{
            return userDrawMapper.findByUserId(userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找中奖记录详情
     * @param drawId
     * @return
     */
    public UserDraw findById(String drawId){
        try{
            return userDrawMapper.findById(drawId);
        }catch (Exception e){
            return null;
        }
    }

}

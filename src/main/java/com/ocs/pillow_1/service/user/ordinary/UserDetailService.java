package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.config.FileBean;
import com.ocs.pillow_1.entity.user.ordinary.UserDetail;
import com.ocs.pillow_1.mapper.user.ordinary.UserDetailMapper;
import com.ocs.pillow_1.service.goods.device.DeviceRelationService;
import com.ocs.pillow_1.service.util.FileServlet;
import com.ocs.pillow_1.service.util.UpDataService;
import com.ocs.pillow_1.service.util.UtilService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageTranscoder;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  用户详情服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Service
public class UserDetailService{
    @Resource
    UserDetailMapper userDetailMapper=null;
    @Resource
    UserService userService=null;
    @Resource
    UserIntegrallogService userIntegrallogService=null;
    @Resource
    DeviceRelationService deviceRelationService=null;
    @Resource
    UpDataService upDataService=null;
    @Resource
    UtilService utilService=null;
    @Autowired
    FileBean fileBean = null;

    /**
     * 创建用户详情，参数通过创建用户时，同时传入
     * @param userId
     * @param
     * @return
     */
    public String create(String userId, Integer age, String birthday, String sex, String phone, Double weight, String wakeupTime, String idealWakeupTime, String loadWay,String openId){
        Map param=new HashMap();
        param.put("userId",userId);
//        param.put("parentId",parentId);
        param.put("age",age);
        param.put("birthday",birthday);
        param.put("sex",sex);
        param.put("phone",phone);
        param.put("weight",weight);
        param.put("wakeupTime",wakeupTime);
        param.put("idealWakeupTime",idealWakeupTime);
        param.put("loadWay",loadWay);
        param.put("openId",openId);
        try {
            userDetailMapper.save(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }


    public String createOne(String userId){
        Map param=new HashMap();
        param.put("userId",userId);
        try {
            userDetailMapper.save(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 修改用户的手机系统,登录时触发操作
     * @param userId
     * @param systemType
     * @return
     */
    public void updateSystemType(String userId,Integer systemType){
        if(findById(userId)!=null) {
            UserDetail userDetail=findById(userId);
            String str;
            if (userDetail.getSystemType()==3){
                str="用户已经是全类型";
            }else if (userDetail.getSystemType()==systemType){
                str= "类型相同";
            }else {
                Integer type = 0;
                if (userDetail.getSystemType() == 0) {
                    type = systemType;
                } else if (systemType==1&&userDetail.getSystemType()==2) {
                    type=3;
                }else if (systemType==2&&userDetail.getSystemType()==1){
                    type=3;
                }
                Map param = new HashMap();
                param.put("userId", userId);
                param.put("systemType", type);
                try {
                    userDetailMapper.updateSystemType(param);
                    str="成功";
                } catch (Exception e) {
                    str= "失败" + e;
                }
            }
        }
    }

    /**
     * 根据用户id查找该用户详情
     * @param userId
     * @return
     */
    public UserDetail findById(String userId){
        if(userDetailMapper.findById(userId)==null) {
            return null;
        }
        UserDetail userDetail=userDetailMapper.findById(userId);
        userDetail.setNickname(userService.findById(userId).getNickname());
        return userDetail;
    }

    /**
     * 修改用户用户的详情
     * @param userId
     * @param birthday
     * @param file
     * @param sex
     * @param phone
     * @param industry
     * @param unit
     * @param position
     * @param address
     * @return
     */
    public String updateDetail(String userId, String birthday, MultipartFile file, String sex, String phone, String industry, String unit, String position, String address, String signature,Double weight,String wakeupTime,String idealWakeupTime){
        String str=null;
        UserDetail userDetail=findById(userId);
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("birthday",birthday);
        param.put("sex",sex);
        param.put("phone",phone);
        param.put("industry",industry);
        param.put("unit",unit);
        param.put("position",position);
        param.put("address",address);
        param.put("signature",signature);
        param.put("weight",weight);
        param.put("wakeupTime",wakeupTime);
        param.put("idealWakeupTime",idealWakeupTime);
        try {
            if (birthday!=null){
                Integer age=utilService.calculateBirth(birthday);
                param.put("age",age);
                str=age.toString();
                if (deviceRelationService.findByUserId(userId)!=null){
                    upDataService.changeAge(userId,birthday);
                }
            }

            if (file!=null){ //修改用户头像
                try {
                    String savePath =fileBean.getSavePath();
                    String readPath = fileBean.getReadPath();

                    String fileRealName=file.getOriginalFilename();//获取原始文件名
                    int pointIndex=fileRealName.lastIndexOf(".");//点号的位置
                    String fileSuffix=fileRealName.substring(pointIndex);//截取文件后缀
                    String fileNewName=userId+(int)(Math.random()*10000);
                    String fileOldName=userId;
                    String saveFileName=fileNewName.concat(fileSuffix);//文件存取名
                    //String saveOldName=fileOldName.concat(fileSuffix);//旧文件存取名
                    //String filepath="C:/upload1";
                    File path=new File(savePath);//判断文件路径下的文件夹是否存在，不存在则创建
                    if (!path.exists()){
                        path.mkdirs();
                    }
                    str=savePath+"/"+saveFileName;
                    //String strOld1=savePath+"/"+saveOldName;
                    //System.out.println(strOld1);
                    UserDetail info = userDetailMapper.findInfo(userId);
                    String headIcon = info.getHeadIcon();
                    if(headIcon!=null) {
                        String strOld = "upload/" + headIcon.substring(29, headIcon.length());
                        System.out.println(strOld);
                        File oldFile=new File(strOld);
                        if (oldFile.exists()){
                            oldFile.delete();
                        }
                    }


                    File savedFile=new File(savePath);
                    savedFile=new File(savePath,saveFileName);
                    FileUtils.copyInputStreamToFile(file.getInputStream(),savedFile);
                    param.put("headIcon",readPath+saveFileName);
                }catch (Exception e){
                    return "201";
                }

            }
            if (sex!=null){
                str=sex;
                if (deviceRelationService.findByUserId(userId)!=null){
                    upDataService.changeSex(userId,sex);
                }
                if (userDetail.getSex()==null){
                    userIntegrallogService.create(userId,10.0,null,"个人性别填写奖励");
                }
            }
            if (phone!=null){
                str=phone;
            }
            if (industry!=null){
                str=industry;
            }
            if (unit!=null){
                str=unit;
            }
            if (position!=null){
                str=position;
            }
            if (address!=null){
                str=address;
            }
            if (signature!=null){
                str=signature;
            }
            if (weight!=null){
                str=weight.toString();
            }
            if (wakeupTime!=null){
                str=wakeupTime;
            }
            if (idealWakeupTime!=null){
                str=idealWakeupTime;
            }
            userDetailMapper.updateDetail(param);
            return str;
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 修改用户的实名认证状态
     * @param userId
     * @return
     */
    public String updateAutonym(String userId,int state){
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("isAutonym",state);
        try{
            userDetailMapper.updateAutonym(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 修改医生的实名认证状态
     * @param userId
     * @param state
     * @return
     */
    public String updateDoctor(String userId,int state){
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("isDoctor",state);
        try{
            userDetailMapper.updateDoctor(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 根据用户名保存openId
     * @return
     */
    public String saveOpenIdByUsername(String username,String openId){
        Map param=new HashMap();
        param.put("username",username);
        param.put("openId",openId);
        try{
            userDetailMapper.saveOpenIdByUsername(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }


    /**
     * 删除文件 无用
     * @param fileName
     * @return
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

}

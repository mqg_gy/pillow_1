package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserAddress;
import com.ocs.pillow_1.mapper.user.ordinary.UserAddressMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  用户购物车地址服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Service
public class UserAddressService {
    @Resource
    UserAddressMapper userAddressMapper=null;
    @Resource
    UtilService utilService=null;

    /**
     * 创建用户的收货地址
     * @param province
     * @param city
     * @param area
     * @param street
     * @param address
     * @param consignee
     * @param consigneePhone
     * @param isDefault
     * @param type
     * @param userId
     * @return
     */
    public String create(String province,String city,String area,String street,String address,String consignee,String consigneePhone,Integer isDefault,String type,String userId){
        UserAddress userAddress=new UserAddress();
        userAddress.setAddressId(UUID.randomUUID().toString().replace("-",""));
        userAddress.setProvince(province);
        userAddress.setCity(city);
        userAddress.setArea(area);
        userAddress.setStreet(street);
        userAddress.setAddress(address);
        userAddress.setConsignee(consignee);
        userAddress.setConsigneePhone(consigneePhone);
        if (isDefault==null){
            isDefault=0;
        }
        userAddress.setIsDefault(isDefault);
        userAddress.setUserId(userId);
        if (type==null){
            type="0";
        }
        userAddress.setType(type);
        try {
            userAddressMapper.save(userAddress);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找一个用户的所有地址
     * @param userId
     * @return
     */
    public List<UserAddress> findByUserId(String userId){
        return userAddressMapper.findByUserId(userId);
    }

    /**
     * 修改默认地址，默认地址唯一
     * @param addressId
     * @param isDefault
     * @param userId
     * @return
     */
    public String updateDefault(String addressId,int isDefault,String userId){
        if (isDefault==1){
            List<UserAddress> list=findByUserId(userId);
            for (UserAddress useraddress:list){
                if (useraddress.getIsDefault()==1) {
                    Map param1 = new HashMap();
                    param1.put("addressId", useraddress.getAddressId());
                    param1.put("isDefault", 0);
                    userAddressMapper.updateDefault(param1);
                }
            }
        }
        Map param=new HashMap();
        param.put("addressId",addressId);
        param.put("isDefault",isDefault);
        try {
            userAddressMapper.updateDefault(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找指定地址
     * @param addressId
     * @return
     */
    public UserAddress findById(String addressId){
        return userAddressMapper.findById(addressId);
    }

    /**
     * 修改收货地址
     * @param addressId
     * @param province
     * @param city
     * @param area
     * @param street
     * @param address
     * @param consignee
     * @param consigneePhone
     * @param isDefault
     * @param type
     * @param userId
     * @return
     */
    public String updateAddress(String addressId,String province,String city,String area,String street,String address,String consignee,String consigneePhone,Integer isDefault,String type,String userId){
        UserAddress userAddress=findById(addressId);
        userAddress.setProvince(province);
        userAddress.setCity(city);
        userAddress.setArea(area);
        userAddress.setStreet(street);
        userAddress.setArea(address);
        userAddress.setConsignee(consignee);
        userAddress.setConsigneePhone(consigneePhone);
        if (type==null){
            type="0";
        }
        userAddress.setType(type);
        try {
            userAddressMapper.updateAddress(userAddress);
            updateDefault(addressId,isDefault,userId);
            return "200";
        }catch (Exception e){
            return "201";
        }

    }

    /**
     * 删除用户地址
     * @param addressId
     * @return
     */
    public String delete(String addressId){
        try{
            userAddressMapper.delete(addressId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }
}

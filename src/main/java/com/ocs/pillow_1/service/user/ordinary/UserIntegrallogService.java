package com.ocs.pillow_1.service.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserIntegrallog;
import com.ocs.pillow_1.mapper.user.ordinary.UserIntegrallogMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-28
 */
@Service
public class UserIntegrallogService {
    @Resource
    UserIntegrallogMapper userIntegrallogMapper=null;

    /**
     * 创建积分记录
     * @param userId
     * @param amount
     * @param orderId
     * @param message
     * @return
     */
    public String create(String userId,Double amount,String orderId,String message){
        UserIntegrallog userIntegrallog=new UserIntegrallog();
        userIntegrallog.setLogId(UUID.randomUUID().toString().replace("-",""));
        userIntegrallog.setUserId(userId);
        userIntegrallog.setAmount(amount);
        userIntegrallog.setOrderId(orderId);
        userIntegrallog.setMessage(message);
        userIntegrallog.setLogTime(LocalDateTime.now());
        try{
            userIntegrallogMapper.save(userIntegrallog);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找指定用户的所有积分记录
     * @param userId
     * @return
     */
    public List<UserIntegrallog> findByUserId(String userId){
        try{
            return userIntegrallogMapper.findByUserId(userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找指定记录的详情
     * @param logId
     * @return
     */
    public UserIntegrallog findById(String logId){
        try{
            return userIntegrallogMapper.findById(logId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找奖励积分表
     * @param userId
     * @return
     */
    public List<UserIntegrallog> findAwarding(String userId){
        try{
            return userIntegrallogMapper.findAwarding(userId);
        }catch (Exception e){
            return null;
        }
    }

}

package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserAutonym;
import com.ocs.pillow_1.mapper.user.ordinary.UserAutonymMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  用户实名认证服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Service
public class UserAutonymService{
    @Resource
    UserAutonymMapper userAutonymMapper=null;
    @Resource
    UtilService utilService=null;
    @Resource
    UserDetailService userDetailService=null;

    /**
     * 创建实名认证信息，认证状态默认是0：审核中
     * @param userId
     * @param positivePhoto
     * @param backPhoto
     * @param name
     * @param national
     * @param birthday
     * @param address
     * @param idNumber
     * @param deadline
     * @param organ
     * @return
     */
    public String create(String userId, String positivePhoto, String backPhoto, String name, String national, Date birthday,String address,String idNumber,Date deadline,String organ){
        UserAutonym userAutonym=new UserAutonym();
        userAutonym.setUserId(userId);
        userAutonym.setPositivePhoto(positivePhoto);
        userAutonym.setBackPhoto(backPhoto);
        userAutonym.setName(name);
        userAutonym.setNational(national);
        userAutonym.setBirthday(birthday);
        userAutonym.setAddress(address);
        userAutonym.setIdNumber(idNumber);
        userAutonym.setDeadline(deadline);
        userAutonym.setOrgan(organ);
        try {
            userAutonymMapper.save(userAutonym);
            userDetailService.updateAutonym(userId,1);//创建实名信息之后，表示正在审核
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找实名认证信息
     * @param userId
     * @return
     */
    public UserAutonym findById(String userId){
        return userAutonymMapper.findById(userId);
    }

    /**
     * 删除审核不通过的实名认证
     * @param userId
     * @return
     */
    public String delete(String userId){
        try {
            userAutonymMapper.delete(userId);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 修改用户的审核状态
     * @param userId
     * @param state
     * @return
     */
    public String updateState(String userId,int state){
        if (state==1){
            Map param=new HashMap();
            param.put("userId",userId);
            param.put("verifyState",String.valueOf(state));
            userAutonymMapper.updateState(param);
            userDetailService.updateAutonym(userId,2);//isAutonym=2,表示审核通过
            return "已审核通过";
        }else if (state==-1){
            userDetailService.updateAutonym(userId,0);//未实名
            return delete(userId);
        }
        return "审核未成功";
    }
}

package com.ocs.pillow_1.service.user.system;

import com.ocs.pillow_1.entity.user.system.SysRole;
import com.ocs.pillow_1.mapper.user.system.SysRoleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-06-25
 */
@Service
public class SysRoleService{
    @Resource
    SysRoleMapper sysRoleMapper=null;

    public List<SysRole> findAll() {
        return sysRoleMapper.findAll();
    }
}

package com.ocs.pillow_1.service.user.ordinary;


import com.ocs.pillow_1.entity.user.ordinary.UserVouchers;
import com.ocs.pillow_1.mapper.user.ordinary.UserVouchersMapper;
import com.ocs.pillow_1.service.user.system.SysVouchersService;
import com.ocs.pillow_1.service.util.UtilService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Service
public class UserVouchersService  {
    @Resource
    UserVouchersMapper userVouchersMapper=null;
    @Resource
    SysVouchersService sysVouchersService=null;
    @Resource
    UtilService utilService=null;

    /**
     * 添加用户的优惠卷
     * @param voucherId
     * @param userId
     * @param source
     * @return
     */
    public String create(String voucherId,String userId,String source){
        UserVouchers userVouchers=new UserVouchers();
        userVouchers.setId(UUID.randomUUID().toString().replace("-",""));
        userVouchers.setVoucherId(voucherId);
        userVouchers.setUserId(userId);
        userVouchers.setSource(source);
        if (!utilService.compareDate(sysVouchersService.findById(voucherId).getEndTime())){
            userVouchers.setState("-1");//优惠卷还未到时间
        }else {
            userVouchers.setState("0");//优惠卷还未使用
        }
        try{
            userVouchersMapper.save(userVouchers);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 删除优惠卷
     * @param id
     */
    public String delete(String id){
        try{
            userVouchersMapper.delete(id);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 修改优惠卷的状态
     * @param id
     * @param state
     * @return
     */
    public String updateState(String id,String state){
        Map param=new HashMap();
        param.put("id",id);
        param.put("state",state);
        try{
            userVouchersMapper.updateState(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找用户所有的优惠券,每次进入优惠卷页面最先执行
     * @param userId
     * @return
     */
    public List<UserVouchers> findByUserId(String userId){
        List<UserVouchers> list=userVouchersMapper.findByUserId(userId);
        for (UserVouchers v:list) {
            if (utilService.compareDate(sysVouchersService.findById(v.getVoucherId()).getEndTime())){
                updateState(v.getId(),"2");//优惠卷已经过期了
            }
        }
        return list;
    }

    /**
     * 未使用的优惠卷
     * @param userId
     * @return
     */
    public List<UserVouchers> validVouchers(String userId){
        List<UserVouchers> list=findByUserId(userId);
        List<UserVouchers> retuenlist=new ArrayList<>();
        for (UserVouchers v:list) {
            if (v.getState().equals("0")){//未使用的优惠卷
                retuenlist.add(v);
            }
        }
        return retuenlist;
    }

    /**
     * 已使用的优惠卷
     * @param userId
     * @return
     */
    public List<UserVouchers> usedVouchers(String userId){
        List<UserVouchers> list=findByUserId(userId);
        List<UserVouchers> retuenlist=new ArrayList<>();
        for (UserVouchers v:list) {
            if (v.getState().equals("1")){//已使用的优惠卷
                retuenlist.add(v);
            }
        }
        return retuenlist;
    }

    /**
     * 已过期的优惠卷
     * @param userId
     * @return
     */
    public List<UserVouchers> overVouchers(String userId){
        List<UserVouchers> list=findByUserId(userId);
        List<UserVouchers> retuenlist=new ArrayList<>();
        for (UserVouchers v:list) {
            if (v.getState().equals("2")){//已过期的优惠卷
                retuenlist.add(v);
            }
        }
        return retuenlist;
    }

}

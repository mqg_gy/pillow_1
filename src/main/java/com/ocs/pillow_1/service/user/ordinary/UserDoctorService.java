package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserDoctor;
import com.ocs.pillow_1.mapper.user.ordinary.UserDoctorMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Service
public class UserDoctorService  {
    @Resource
    UserDoctorMapper userDoctorMapper=null;
    @Resource
    UtilService utilService=null;
    @Resource
    UserDetailService userDetailService=null;

    /**
     * 创建医生实名认证信息，认证状态默认是0：审核中
     * @param userId
     * @param licPositivePhoto
     * @param licBackPhoto
     * @param workPhoto
     * @param introduce
     * @return
     */
    public String create(String userId,String licPositivePhoto,String licBackPhoto,String workPhoto,String introduce){
        UserDoctor userDoctor=new UserDoctor();
        userDoctor.setUserId(userId);
        userDoctor.setLicPositivePhoto(licPositivePhoto);
        userDoctor.setLicBackPhoto(licBackPhoto);
        userDoctor.setWorkPhoto(workPhoto);
        userDoctor.setIntroduce(introduce);
        try{
            userDoctorMapper.save(userDoctor);
            userDetailService.updateDoctor(userId,1);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找实名认证信息
     * @param userId
     * @return
     */
    public UserDoctor findById(String userId){
        return userDoctorMapper.findById(userId);
    }

    /**
     * 删除审核不通过的实名认证
     * @param userId
     * @return
     */
    public String delete(String userId){
        try{
            userDoctorMapper.delete(userId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 修改医生的审核状态
     * @param userId
     * @param state
     * @return
     */
    public String updateState(String userId,int state){
        if (state==1){
            Map param=new HashMap();
            param.put("userId",userId);
            param.put("verifyState",String.valueOf(state));
            userDoctorMapper.updateState(param);
            userDetailService.updateDoctor(userId,2);//isAutonym=2,表示审核通过
            return "已审核通过";
        }else if (state==-1){
            userDetailService.updateDoctor(userId,0);//未实名
            return delete(userId);
        }
        return "审核未成功";
    }
}

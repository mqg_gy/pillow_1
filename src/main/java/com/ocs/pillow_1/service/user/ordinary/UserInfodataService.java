package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserInfodata;
import com.ocs.pillow_1.mapper.user.ordinary.UserInfodataMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-06-11
 */
@Service
public class UserInfodataService {
    @Resource
    UserInfodataMapper userInfodataMapper=null;


    /**
     * 保存用户的微信信息
     * @param openId
     * @param nickname
     * @param userImg
     * @param sex
     * @param unionId
     * @return
     */
    public String create(String openId,String nickname,String userImg,String sex,String unionId){
        UserInfodata userInfodata=new UserInfodata();
        userInfodata.setOpenId(openId);
        userInfodata.setNickname(nickname);
        userInfodata.setUserImg(userImg);
        userInfodata.setSex(sex);
        userInfodata.setUnionId(unionId);
        try {
            userInfodataMapper.save(userInfodata);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

}

package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserWallet;
import com.ocs.pillow_1.mapper.user.ordinary.UserWalletMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  用户钱包服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Service
public class UserWalletService {
    @Resource
    UserWalletMapper userWalletMapper=null;


    /**
     * 注册时，创建用户的钱包
     * @param userId
     */
    public String create(String userId){
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("updateTime",LocalDateTime.now());
        try{
            userWalletMapper.save(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 查找用户钱包
     * @param userId
     * @return
     */
    public UserWallet findById(String userId){
        return userWalletMapper.findById(userId);
    }

    /**
     * 增加用户余额
     * @param userId
     * @param amount
     * @return
     */
    public String addBalance(String userId,Double amount){
        UserWallet userWallet=findById(userId);
        Map param=new HashMap();
        param.put("userId",userWallet.getUserId());
        param.put("updateTime",LocalDateTime.now());
        param.put("balance",userWallet.getBalance()+amount);
        try {
            userWalletMapper.updateBalance(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 减少用户余额
     * @param userId
     * @param amount
     * @return
     */
    public String reduceBalance(String userId,Double amount){
            UserWallet userWallet = findById(userId);
            Map param = new HashMap();
            param.put("userId", userWallet.getUserId());
            param.put("updateTime", LocalDateTime.now());
            param.put("balance", userWallet.getBalance() - amount);
            try {
                userWalletMapper.updateBalance(param);
                return "200";
            } catch (Exception e) {
                return "201";
            }
    }

    /**
     * 增加用户积分
     * @param userId
     * @param amount
     * @return
     */
    public String addIntegral(String userId,Double amount){
        UserWallet userWallet=findById(userId);
        Map param=new HashMap();
        param.put("userId",userWallet.getUserId());
        param.put("updateTime",LocalDateTime.now());
        param.put("integral",userWallet.getIntegral()+amount);
        try {
            userWalletMapper.updateIntegral(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 减少用户积分
     * @param userId
     * @param amount
     * @return
     */
    public String reduceIntegral(String userId,Double amount){
            UserWallet userWallet = findById(userId);
            Map param = new HashMap();
            param.put("userId", userWallet.getUserId());
            param.put("updateTime", LocalDateTime.now());
            param.put("balance", userWallet.getIntegral() - amount);
            try {
                userWalletMapper.updateIntegral(param);
                return "200";
            } catch (Exception e) {
                return "201";
            }
    }

    /**
     * 密码校对
     * @param userId
     * @param payNumber
     * @return
     */
    public String chenkPay(String userId,String payNumber){
        UserWallet userWallet=findById(userId);
        if (userWallet.getPayNumber()==null){
            return "204";//密码未设置
        }
        if (payNumber.equals(userWallet.getPayNumber())){
            return "200";//密码正确
        }else{
            return "201";//密码错误
        }
    }

    /**
     * 设置或修改密码
     * @param userId
     * @param payNumber
     * @return
     */
    public String updatePayNumber(String userId,String payNumber){
        Map param=new HashMap();
        param.put("userId",userId);
        param.put("updateTime",LocalDateTime.now());
        param.put("payNumber",payNumber);
        try{
            userWalletMapper.updatePayNumber(param);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }
}

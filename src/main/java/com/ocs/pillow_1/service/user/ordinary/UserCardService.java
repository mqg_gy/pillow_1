package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserCard;
import com.ocs.pillow_1.mapper.user.ordinary.UserCardMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  用户银行卡服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Service
public class UserCardService  {
    @Resource
    UserCardMapper userCardMapper=null;
    @Resource
    UtilService utilService=null;
    @Resource
    UserDetailService userDetailService=null;

    /**
     * 绑定银行卡
     * @param userId
     * @param type
     * @param number
     * @param name
     * @param phone
     * @return
     */
    public String create(String userId, String type, String number, String name, String phone){
        if (userDetailService.findById(userId).getIsAutonym()==2){
            UserCard userCard=new UserCard();
            userCard.setCardId(UUID.randomUUID().toString().replace("-",""));
            userCard.setType(type);
            userCard.setNumber(number);
            userCard.setName(name);
            userCard.setPhone(phone);
            userCard.setRelevanTime(LocalDateTime.now());
            userCard.setUserId(userId);
            try {
                userCardMapper.save(userCard);
                return "200";
            }catch (Exception e){
                return "201";
            }
        }else {
            return "204";//未完成实名认证
        }
    }

    /**
     * 查找用户的银行卡
     * @param userId
     * @return
     */
    public List<UserCard> findByUserId(String userId){
        return userCardMapper.findByUserId(userId);
    }

    /**
     * 解绑银行卡
     * @param cardId
     * @return
     */
    public String delete(String cardId){
        try{
            userCardMapper.delete(cardId);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }
}

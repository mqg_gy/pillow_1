package com.ocs.pillow_1.service.user.ordinary;

import ch.qos.logback.core.encoder.EchoEncoder;
import com.ocs.pillow_1.entity.user.ordinary.CustomerHelp;
import com.ocs.pillow_1.entity.user.ordinary.User;
import com.ocs.pillow_1.entity.user.ordinary.UserToken;
import com.ocs.pillow_1.mapper.user.ordinary.UserTokenMapper;
import com.ocs.pillow_1.service.util.UtilService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 * 用户Token服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-08
 */
@Service
public class UserTokenService {
    @Resource
    UserTokenMapper userTokenMapper = null;
    @Resource
    UserDetailService userDetailService = null;
    @Resource
    UtilService utilService = null;
    @Resource
    UserService userService = null;

    /**
     * 根据登录的账号查找用户，并判断用户是否存在
     *
     * @param username
     * @param password
     * @return
     */
    public User findToLogin(String username, String password) {
        if (userTokenMapper.findToLogin(username, password) != null) {
            return userTokenMapper.findToLogin(username, password);
        } else {
            return null;
        }
    }

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    public UserToken login(String username, String password, Integer systemType) {
        UserToken userToken = new UserToken();
        User user = userService.findByUsername(username);   //根据用户名查询用户
        String mdPassword = null;
        if (user != null) {//该用户存在
            mdPassword = utilService.string2MD5(user.getUserId(), password);
        }
        if (findToLogin(username, mdPassword) != null) {//如果输入的账号正确
            String userId = user.getUserId();
            if (findById(userId) != null) {//如果用户的token存在
                userToken = reLogin(userId);
            } else {
                userToken = create(userId, username);
            }
            userDetailService.updateSystemType(userId, systemType);
            return userToken;
        } else {//如果输入的账号错误
            return null;
        }
    }


    /**
     * @param username
     * @return
     */
    public UserToken getUserToken(String username) {
        UserToken userToken = new UserToken();
        User user = userService.findByUsername(username);   //根据用户名查询用户
        String userId = user.getUserId();
        if (findById(userId) != null) {//如果用户的token存在
            userToken = reLogin(userId);
        } else {
            userToken = create(userId, username);
        }
        return userToken;
    }

    /**
     * 刚注册完还没有登陆过的用户,即第一次登录
     *
     * @param userId
     * @param username
     * @return
     */
    public UserToken create(String userId, String username) {
        UserToken userToken = new UserToken();
        userToken.setUserId(userId);
        userToken.setUsername(username);
        userToken.setLastUsedTime(LocalDateTime.now());
        userToken.setToken(UUID.randomUUID().toString().replace("-", ""));
        try {
            userTokenMapper.save(userToken);
            return userToken;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据userId查找
     *
     * @param userId
     * @return
     */
    public UserToken findById(String userId) {
        return userTokenMapper.findById(userId);
    }

    /**
     * 已经登陆过了，需要修改token
     *
     * @param userId
     * @return
     */
    public UserToken reLogin(String userId) {
        UserToken userToken = findById(userId);
        userToken.setToken(UUID.randomUUID().toString().replace("-", ""));
        userToken.setLastUsedTime(LocalDateTime.now());
        userTokenMapper.reLogin(userToken);
        return userToken;
    }

    /**
     * 根据每一次前端传回的token，获得需要的userId等信息
     *
     * @param token
     * @return
     */
    public UserToken findByToken(String token) {
        return userTokenMapper.findByToken(token);
    }

    /**
     * 当用户修改了用户名的时候，token中也需要修改
     *
     * @param userId
     * @param username
     * @return
     */
    public String updateUsername(String userId, String username) {
        try {
            userTokenMapper.updateUsername(userId, username);
            return "成功";
        } catch (Exception e) {
            return "失败";
        }
    }

    public UserToken wechatLogin(String openId, int systemType) {
        UserToken userToken = new UserToken();
        User user = userService.findByOpenId(openId);   //根据用户名查询用户
        if(user==null)
        {
            return null;
        }
        String userId = user.getUserId();
        if (findById(userId) != null) {//如果用户的token存在
            userToken = reLogin(userId);
        } else {
            userToken = create(userId, user.getUsername());
        }
        userDetailService.updateSystemType(userId, systemType);
        return userToken;
    }

}

package com.ocs.pillow_1.service.user.system;


import com.ocs.pillow_1.entity.user.system.SysVouchers;
import com.ocs.pillow_1.mapper.user.system.SysVouchersMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linj123
 * @since 2019-04-26
 */
@Service
public class SysVouchersService {
    @Resource
    SysVouchersMapper sysVouchersMapper=null;

    /**
     * 添加系统的优惠卷
     * @param faceValue
     * @param range
     * @param products
     * @param startTime
     * @param endTime
     * @return
     */
    public String create(String faceValue, String range, String products, Date startTime,Date endTime){
        SysVouchers sysVouchers=new SysVouchers();
        sysVouchers.setVoucherId(UUID.randomUUID().toString().replace("-",""));
        sysVouchers.setFaceValue(faceValue);
        sysVouchers.setRange(range);
        sysVouchers.setProducts(products);
        sysVouchers.setStartTime(startTime);
        sysVouchers.setEndTime(endTime);
        try{
            sysVouchersMapper.save(sysVouchers);
            return "成功";
        }catch (Exception e){
            return "失败";
        }
    }

    /**
     * 修改优惠卷的基本信息
     * @param voucherId
     * @param faceValue
     * @param range
     * @param products
     * @return
     */
    public String update(String voucherId,String faceValue, String range, String products){
        Map param=new HashMap();
        param.put("voucherId",voucherId);
        param.put("faceValue",faceValue);
        param.put("range",range);
        param.put("products",products);
        try{
            sysVouchersMapper.update(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 更新开始时间
     * @param voucherId
     * @param startTime
     * @return
     */
    public String updateStartTime(String voucherId,Date startTime){
        Map param=new HashMap();
        param.put("voucherId",voucherId);
        param.put("startTime",startTime);
        try {
            sysVouchersMapper.updateStartTime(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 更新结束时间
     * @param voucherId
     * @param endTime
     * @return
     */
    public String updateEndTime(String voucherId,Date endTime){
        Map param=new HashMap();
        param.put("voucherId",voucherId);
        param.put("endTime",endTime);
        try {
            sysVouchersMapper.updateStartTime(param);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 删除优惠卷
     * @param voucherId
     */
    public String delete(String voucherId){
        try{
            sysVouchersMapper.delete(voucherId);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }
    }

    /**
     * 查找所有优惠卷，并按照时间的顺序
     * @return
     */
    public List<SysVouchers> findAll(){
        try{
            return sysVouchersMapper.findAll();
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 查找指定优惠卷
     * @param voucherId
     * @return
     */
    public SysVouchers findById(String voucherId){
        try{
            return sysVouchersMapper.findById(voucherId);
        }catch (Exception e){
            return null;
        }
    }

}

package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserTradingrecord;
import com.ocs.pillow_1.mapper.user.ordinary.UserTradingrecordMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>
 *  用户交易记录服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-09
 */
@Service
public class UserTradingrecordService {
    @Resource
    UserTradingrecordMapper userTradingrecordMapper=null;

    /**
     * 创建交易记录
     * @param amount
     * @param method
     * @param type
     * @param userId
     * @param orderId
     * @return
     */
    public String create(Double amount,String method,String type,String userId,String orderId){
        UserTradingrecord userTradingrecord=new UserTradingrecord();
        userTradingrecord.setRecordId(UUID.randomUUID().toString().replace("-",""));
        userTradingrecord.setTime(LocalDateTime.now());
        userTradingrecord.setAmount(amount);
        userTradingrecord.setMethod(method);
        userTradingrecord.setType(type);
        userTradingrecord.setUserId(userId);
        userTradingrecord.setOrderId(orderId);
        try {
            userTradingrecordMapper.save(userTradingrecord);
            return "成功";
        }catch (Exception e){
            return "失败"+e;
        }

    }

}

package com.ocs.pillow_1.service.user.system;

import com.ocs.pillow_1.entity.user.system.SysUser;
import com.ocs.pillow_1.mapper.user.system.SysUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysManagerService {
    @Resource
    SysUserMapper sysUserMapper=null;

    /**
     * 校对登录信息
     * @param usercode
     * @param password
     * @return 布尔值
     */
    public SysUser checkLogin(String usercode,String password){
        Map param=new HashMap();
        param.put("usercode",usercode);
        param.put("password",password);
        SysUser sysUser=sysUserMapper.findByCodeAndPass(param);
        if (sysUser!=null){
            return sysUser;
        }else {
            return null;
        }
    }

    public List<SysUser> findAll() {
        return sysUserMapper.findAll();
    }

    public String save(SysUser manager) {
        try {
            sysUserMapper.save(manager);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }
}

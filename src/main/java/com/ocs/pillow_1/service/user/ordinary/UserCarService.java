package com.ocs.pillow_1.service.user.ordinary;

import com.ocs.pillow_1.entity.user.ordinary.UserCar;
import com.ocs.pillow_1.mapper.user.ordinary.UserCarMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  购物车服务类
 * </p>
 *
 * @author linj123
 * @since 2019-04-10
 */
@Service
public class UserCarService {
    @Resource
    UserCarMapper userCarMapper=null;

    /**
     * 向购物车添加商品
     * @param userId
     * @param productId
     * @param type
     * @param count
     * @param specValues
     * @return
     */
    public String create(String userId,String productId,Integer type,Integer count,String specValues){
        UserCar userCar=new UserCar();
        userCar.setUserId(userId);
        userCar.setProductId(productId);
        userCar.setType(type);
        userCar.setSpecValues(specValues);
        if (userCarMapper.checkSave(userCar)==null){
            userCar.setShopId(UUID.randomUUID().toString().replace("-",""));
            userCar.setCount(count);
            userCar.setAddTime(LocalDateTime.now());
            try{
                userCarMapper.save(userCar);
                return "200";
            }catch (Exception e){
                return "201";
            }
        }else{
            userCar=userCarMapper.checkSave(userCar);
            userCar.setCount(userCar.getCount()+count);
            userCar.setAddTime(LocalDateTime.now());
            try{
                userCarMapper.updateCount(userCar);
                return "成功";
            }catch (Exception e){
                return "失败"+e;
            }
        }
    }

    /**
     * 查找指定用户的购物车
     * （需要根据商品Id返回商品信息）
     * @param userId
     * @return
     */
    public List<UserCar> findByUserId(String userId){
        try {
            return userCarMapper.findByUserId(userId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 根据id查找购物车中的商品
     * @param shopId
     * @return
     */
    public UserCar findById(String shopId){
        return userCarMapper.findById(shopId);
    }

    /**
     * 删除购物车中的商品
     * @param shopId
     * @return
     */
    public String delete(String shopId){
        try {
            userCarMapper.delete(shopId);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 点击+按钮，商品数量+1
     * @param shopId
     * @return
     */
    public String upCount(String shopId){
        UserCar userCar=findById(shopId);
        userCar.setCount(userCar.getCount()+1);
        userCar.setAddTime(LocalDateTime.now());
        try{
            userCarMapper.updateCount(userCar);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }

    /**
     * 点击-按钮，商品数量-1
     * @param shopId
     * @return
     */
    public String downCount(String shopId){
        UserCar userCar=findById(shopId);
        userCar.setCount(userCar.getCount()-1);
        userCar.setAddTime(LocalDateTime.now());
        try {
            userCarMapper.updateCount(userCar);
            return "200";
        }catch (Exception e){
            return "201";
        }
    }
}

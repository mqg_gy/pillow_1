/**
 *auto:Readchar
 *date:20140123
 *discription:麦开官方网站特效
 */
 

$(function(){																				//文档加载完成调用
	$(".mecare-nav-btn").click(function(){
		MMecare.indexEffec.toggleNav();
	});
	
	$(".notich-wx >a").click(function(){
		MMecare.toggleWx();															//关注微信
	});
});
 
																							//声明一个私有空间，防止变量名冲突
var MMecare={																				//创建一个公共对象，一被调用
		indexEffec:{																		//创建首页特效对象
			navElm:[$(".mh-line-pa"),$(".mh-line-pb"),$("nav")],							//导航元素
			
			toggleNav:function(){															//导航特效
				if(this.navElm[0].hasClass("mh-line-pa-tra")){								//如果显示导航，就隐藏
					this.navElm[0].removeClass("mh-line-pa-tra");
					this.navElm[1].removeClass("mh-line-pb-tra");
					this.navElm[2].addClass("mecare-hidden-nav");
				}else{																		//如果隐藏，则显示
					this.navElm[0].addClass("mh-line-pa-tra");
					this.navElm[1].addClass("mh-line-pb-tra");
					this.navElm[2].removeClass("mecare-hidden-nav");
				}
			},
			hiddenNav:function(){
				if(this.navElm[0].hasClass("mh-line-pa-tra")){								//如果显示导航，就隐藏
					this.navElm[0].removeClass("mh-line-pa-tra");
					this.navElm[1].removeClass("mh-line-pb-tra");
					this.navElm[2].addClass("mecare-hidden-nav");
				}
			}
		},
		toggleWx:function(){
			//$(".notich-wx >a").click(function(){												//关注微信
				//$(".show-wx-tip").animate({"height":300},300);
				//alert();
				if($(".show-wx-tip").hasClass("hidden-wx-tip")){
					$(".show-wx-tip").removeClass("hidden-wx-tip");
				}else{
					$(".show-wx-tip").addClass("hidden-wx-tip");
				}
				return false;
			//})
		}
	};


/**
 * Created by jien on 2016/6/27.
 */
//对某对象增加错误提示.使用方法如：addError($("#user_name"), "User name not null!");
function addError(obj, msg){
    var parentObj = obj.closest("div.form-group");
    var msgObj = parentObj.find("span.help-block");
    msgObj.text(msg);
    parentObj.addClass("has-error");
}

//对某对象删除错误提示.使用方法如：removeError($("#user_name"));
function removeError(obj) {
    var parentObj = obj.closest("div.form-group");
    parentObj.removeClass("has-error");

    var msgObj = parentObj.find("span.help-block");
    if (msgObj) {
        msgObj.text("");
    }
}
